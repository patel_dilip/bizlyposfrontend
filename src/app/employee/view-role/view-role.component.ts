import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'kt-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.scss']
})
export class ViewRoleComponent implements OnInit {

  roleForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    // form defination

    this.roleForm = this.fb.group({
      id: [''],
      name: [''],
      access:[''],
      desc: ['']
    })
  }

}
