import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopCreateRoleComponent } from './pop-create-role.component';

describe('PopCreateRoleComponent', () => {
  let component: PopCreateRoleComponent;
  let fixture: ComponentFixture<PopCreateRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopCreateRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopCreateRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
