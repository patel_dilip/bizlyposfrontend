import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'kt-pop-create-role',
  templateUrl: './pop-create-role.component.html',
  styleUrls: ['./pop-create-role.component.scss']
})
export class PopCreateRoleComponent implements OnInit {

  roleForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    // form defination

    this.roleForm = this.fb.group({
      id: [''],
      name: [''],
      access:[''],
      desc: ['']
    })
  }

}
