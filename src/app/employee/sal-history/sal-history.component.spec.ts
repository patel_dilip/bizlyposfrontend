import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalHistoryComponent } from './sal-history.component';

describe('SalHistoryComponent', () => {
  let component: SalHistoryComponent;
  let fixture: ComponentFixture<SalHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
