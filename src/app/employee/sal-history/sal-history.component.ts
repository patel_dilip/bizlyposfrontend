import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'kt-sal-history',
  templateUrl: './sal-history.component.html',
  styleUrls: ['./sal-history.component.scss']
})
export class SalHistoryComponent implements OnInit {

 //colums
 cols=[] 

 // rows
 rows=[
   {
     'srno':'1',
     'staffName':'Mr. Sanket',
     'role': 'Manager',
     'lastsalon': '45,000 - 05/12/19',
     'salary': '50000',
     'emi': '',
     'allowance': '',
     'deduction': '1500',
     'pending': '3500',
     'total_payable':'52000',
     'amt_paid': '52000',
     'status': 'Paid'
   },
   {
     'srno':'2',
     'staffName':'Mr. Shivraj',
     'role': 'CEO',
     'lastsalon': '45,000 - 05/12/19',
     'salary': '50000',
     'emi': '',
     'allowance': '',
     'deduction': '1500',
     'pending': '1500',
     'total_payable':'37000',
     'amt_paid': '52000',
     'status': 'Pending'
   }
 ]

 constructor(private router: Router, private empServ: EmployeeService) { }

 ngOnInit() {

   this.cols =[
     {field:'date', header:'Date'},
     {field:'employee', header:'Employee'},
     {field:'loan', header:'Loan Amount'},
     {field:'pending', header:'Pending Amount'},
     {field:'emi', header:'EMI/Month'},
     {field:'status', header:'Status'}
   ]
 


 }

 // view Loan History
 onViewSalary(name){
   this.empServ.empSalName = name
   this.router.navigate(['employee-management/view-salary-history'])
 }

 // Receive emi
 onReceiveEmi(){
   this.router.navigate(['employee-management/receive-emi'])
 }
}
