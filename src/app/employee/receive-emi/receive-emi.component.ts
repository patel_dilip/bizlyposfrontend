import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-receive-emi',
  templateUrl: './receive-emi.component.html',
  styleUrls: ['./receive-emi.component.scss']
})
export class ReceiveEmiComponent implements OnInit {

  receiveForm: FormGroup

  constructor(private fb: FormBuilder, private router: Router) {
    
  }

  ngOnInit() {
    this.receiveForm = this.fb.group({
      emi_amt: [''],
      amt_pen: [''],
      amount: ['']
    })
  }

  // Receive EMI amount
  onReceive(){
    this.router.navigate(['/employee-management/loan-history'])
  }

}
