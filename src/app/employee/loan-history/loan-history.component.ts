import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'kt-loan-history',
  templateUrl: './loan-history.component.html',
  styleUrls: ['./loan-history.component.scss']
})
export class LoanHistoryComponent implements OnInit {

  
  //colums
  cols=[]

  // rows
  rows=[ {'date':'17/08/2019','employee':'Mr. Sanket','loan':'50000','pending':'45000', 'emi':'3000', 'status':'Complete'},
  {'date':'17/08/2018','employee':'Mr. Prasad','loan':'880000','pending':'50000', 'emi':'11000', 'status':'on Going'}
]

  constructor(private router: Router, private empServ: EmployeeService) { }

  ngOnInit() {

    this.cols =[
      {field:'date', header:'Date'},
      {field:'employee', header:'Employee'},
      {field:'loan', header:'Loan Amount'},
      {field:'pending', header:'Pending Amount'},
      {field:'emi', header:'EMI/Month'},
      {field:'status', header:'Status'}
    ]
  


  }

  // view Loan History
  onViewLoan(name){
    this.empServ.emploanhistory = name
    this.router.navigate(['employee-management/view-loan-history'])
  }

  // Receive emi
  onReceiveEmi(){
    this.router.navigate(['employee-management/receive-emi'])
  }
}
