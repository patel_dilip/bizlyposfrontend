import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'kt-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  // Form Hidden
  formHide: boolean = true
  extNode: any = [];

  // Tree varialbe DEFAULT 1 DATA
  files: TreeNode[] = [
    {
      'label': 'CEO/Super Admin',
      'data': [
      {'access':'Restaurant','status': true},
      {'access':'Menu','status': true},
      {'access':'POS orders','status': true},
      {'access':'Charges','status': false},
      {'access':'User & Roles','status': true},
      {'access':'Integration','status': false},
      {'access':'Circular','status': false},
      {'access':'Feedback','status': true}],
      'key' : 'ROLE001',
      "expanded": true,
      "expandedIcon": "pi pi-folder-open",
      "collapsedIcon": "pi pi-folder",
      "leaf": false,
      'children': [  
        {
          'label': 'General Developer',
          'data': [
            {'access':'Restaurant','status': true},
            {'access':'Menu','status': true},
            {'access':'POS orders','status': true},
            {'access':'Charges','status': false},
            {'access':'User & Roles','status': false},
            {'access':'Integration','status': false},
            {'access':'Circular','status': false},
            {'access':'Feedback','status': false}],
          'key' : 'ROLE002',
          "expanded": true,
          "expandedIcon": "pi pi-folder-open",
          "collapsedIcon": "pi pi-folder",
          "leaf": false,
          'children': [    
            {
              'label': 'Developer',
              'data':  [
                {'access':'Restaurant','status': true},
                {'access':'Menu','status': true},
                {'access':'POS orders','status': false},
                {'access':'Charges','status': false},
                {'access':'User & Roles','status': false},
                {'access':'Integration','status': false},
                {'access':'Circular','status': false},
                {'access':'Feedback','status': false}],
              'key' : 'ROLE002',
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              'children': [      
              ]
            } ,
            {
              'label': 'CA',
              'data': [
                {'access':'Restaurant','status': true},
                {'access':'Menu','status': false},
                {'access':'POS orders','status': false},
                {'access':'Charges','status': false},
                {'access':'User & Roles','status': false},
                {'access':'Integration','status': false},
                {'access':'Circular','status': false},
                {'access':'Feedback','status': false}],
              'key' : 'ROLE002',
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              'children': [      
              ]
            } ,
            {
              'label': 'CS',
              'data': [
                {'access':'Restaurant','status': true},
                {'access':'Menu','status': true},
                {'access':'POS orders','status': true},
                {'access':'Charges','status': true},
                {'access':'User & Roles','status': false},
                {'access':'Integration','status': true},
                {'access':'Circular','status': false},
                {'access':'Feedback','status': false}],
              'key' : 'ROLE002',
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              'children': [      
              ]
            }   
          ]
        } ,
        {
          'label': 'Operations Manager',
          'data': [
            {'access':'Restaurant','status': true},
            {'access':'Menu','status': true},
            {'access':'POS orders','status': true},
            {'access':'Charges','status': false},
            {'access':'User & Roles','status': false},
            {'access':'Integration','status': false},
            {'access':'Circular','status': false},
            {'access':'Feedback','status': false}],
          'key' : 'ROLE003',
          "expanded": true,
          "expandedIcon": "pi pi-folder-open",
          "collapsedIcon": "pi pi-folder",
          "leaf": false,
          'children': [   
            {
              'label': 'BDM',
              'data': [
                {'access':'Restaurant','status': true},
                {'access':'Menu','status': true},
                {'access':'POS orders','status': false},
                {'access':'Charges','status': false},
                {'access':'User & Roles','status': false},
                {'access':'Integration','status': false},
                {'access':'Circular','status': false},
                {'access':'Feedback','status': false}],
              'key' : 'ROLE002',
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              'children': [   
                {
                  'label': 'BDE',
                  'data': [
                    {'access':'Restaurant','status': true},
                    {'access':'Menu','status': true},
                    {'access':'POS orders','status': false},
                    {'access':'Charges','status': false},
                    {'access':'User & Roles','status': false},
                    {'access':'Integration','status': false},
                    {'access':'Circular','status': false},
                    {'access':'Feedback','status': false}],
                  'key' : 'ROLE002',
                  "expanded": true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  "leaf": false,
                  'children': [      
                  ]
                }    
              ]
            },
            {
              'label': 'Account Manager',
              'data': [
                {'access':'Restaurant','status': true},
                {'access':'Menu','status': true},
                {'access':'POS orders','status': true},
                {'access':'Charges','status': false},
                {'access':'User & Roles','status': false},
                {'access':'Integration','status': false},
                {'access':'Circular','status': false},
                {'access':'Feedback','status': false}],
              'key' : 'ROLE002',
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              'children': [ 
                {
                  'label': 'Accountant',
                  'data': [
                    {'access':'Restaurant','status': true},
                    {'access':'Menu','status': true},
                    {'access':'POS orders','status': true},
                    {'access':'Charges','status': false},
                    {'access':'User & Roles','status': false},
                    {'access':'Integration','status': false},
                    {'access':'Circular','status': false},
                    {'access':'Feedback','status': false}],
                  'key' : 'ROLE002',
                  "expanded": true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  "leaf": false,
                  'children': [      
                  ]
                }      
              ]
            }  

          ]
        }    
      ]
    }

  ]

  userForm: FormGroup
  

constructor(private fb: FormBuilder) {

   // Role create form
   this.userForm = this.fb.group({
    role: [''],
    access_points: [[]],
    firstName: ['',Validators.required],     
    middleName: [''],
    lastName: [''],
    contact: [''],
    emailId: [''],
    photo: [''],
    address: [''],
    comm_address: [''],
    gender: [''],
    dob: [''],
    blood_grp: [''],
    marital_staus: [''],
    father_husband_name: [''],
    mother_name: [''],
    emergency: [''],
    work_schedule: [''],
    emp_responsibility: [''],
    no_disclosure: [''],
    sanction_leave:[''],
    account_no: [''],
    account_type: [''],
    bank_name: [''],
    branch_name: [''],
    bank_city: [''],
    bank_address: [''],
    bank_ifsc_code: [''],
    salary: [''],
    aadhar: [''],
    driving_lic: [''],
    passport: [''],     
    legal_docName: [''],
    legal_doc: [''],
    doj: [''],
    assigned_city: [''],
    assigned_loc: [''],
    join_letter: [''],
    asset_provided: false,
    asset_name: [''],
    asset_desc: [''],
    asset_quantity: [''],
    gen_loginId: [''],
    gen_password: [''],     
    status: true,
    created_by: ['']
  });
 }

ngOnInit() {
}

 // TREE NODE SELECTION
 onTreeNodeSelection(event){
  console.log('selected node', event.node)

  this.formHide = false
  this.extNode = event.node

  // patching role and access point
  this.userForm.patchValue({
    role: this.extNode.label,
    access_points: this.extNode.data

  })
}



// Save User Function
saveUser(){

  console.log('Saved User', this.userForm.value)
}
}
