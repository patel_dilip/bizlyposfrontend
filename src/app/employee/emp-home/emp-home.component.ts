import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PopCreateRoleComponent } from '../pop-create-role/pop-create-role.component';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { PopAddSalComponent } from '../pop-add-sal/pop-add-sal.component';
import { PopPaySalComponent } from '../pop-pay-sal/pop-pay-sal.component';


interface salary{
  serial_No;
    staffName;
    role;
    lastPaid;
    salaryAmt;
    emiAmt;
    allowance;
    deduction;
    pendingAmt;
    payableAmt;
    paidAmt;
    status;
    action;
}

@Component({
  selector: 'kt-emp-home',
  templateUrl: './emp-home.component.html',
  styleUrls: ['./emp-home.component.scss']
})
export class EmpHomeComponent implements OnInit {


  // ROw variables
  rows = []
  cols = []
  _selectedColumns = []


  // USer variables
  user_rows=[]
  user_cols = []
  user_selectedColumns = []

    // for salary
    cols_sal: any[] = [];
    rows_sal: any[] = [];
    _selectedColSal: any[] = [];
    selectedSal1: salary;
    seletedReceiptant: any

  star =4

  constructor(private matdialog: MatDialog, private router: Router, private empServ: EmployeeService) {



   }

  ngOnInit() {

    // Role Rows Data
    this.rows = [
      {
        'roleid': 'Role001',
        'roleName': 'CEO',
        'total_users': '15',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role002',
        'roleName': 'Manager',
        'total_users': '65',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'false',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role003',
        'roleName': 'Cashier',
        'total_users': '37',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role001',
        'roleName': 'CEO',
        'total_users': '15',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role002',
        'roleName': 'Manager',
        'total_users': '65',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'false',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role003',
        'roleName': 'Cashier',
        'total_users': '37',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role001',
        'roleName': 'CEO',
        'total_users': '15',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role002',
        'roleName': 'Manager',
        'total_users': '65',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'false',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role003',
        'roleName': 'Cashier',
        'total_users': '37',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role001',
        'roleName': 'CEO',
        'total_users': '15',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role002',
        'roleName': 'Manager',
        'total_users': '65',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'false',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role003',
        'roleName': 'Cashier',
        'total_users': '37',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role001',
        'roleName': 'CEO',
        'total_users': '15',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role002',
        'roleName': 'Manager',
        'total_users': '65',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'false',
        'created_by_on': 'sanket on 12/01/2020'
      },
      {
        'roleid': 'Role003',
        'roleName': 'Cashier',
        'total_users': '37',
        'role_access_points': ['Restaurant Registration', 'Feedback', 'Notice', 'Campaign'],
        'status': 'true',
        'created_by_on': 'sanket on 12/01/2020'
      }
    ]

    // Role COlumns list
    this.cols = [
      {
        'field': 'roleid',
        'header': 'Role Id'
      },
      {
        'field': 'roleName',
        'header': 'Role'
      },
      {
        'field': 'total_users',
        'header': 'Total Users'
      },
      {
        'field': 'role_access_points',
        'header': 'Role Access Points'
      },
      {
        'field': 'status',
        'header': 'Status'
      },
      {
        'field': 'created_by_on',
        'header': 'Created By and On'
      }
    ]

    this._selectedColumns = this.cols


    // user Rows Data
    this.user_rows = [
      {
        'userId': 'MH123',
        'empName': 'Mike Acosta',
        'role': 'BDM',
        'rating': 5,
        'login':'Mike123',
        'email': 'admin@gmail.com',
        'contact': '9988774455',
        'joining': '21-09-2020',
        'status': 'true'
      },
      {
        'userId': 'QH147',
        'empName': 'Peter Hebrew',
        'role': 'CEO',
        'rating': 5,
        'login':'Pete123',
        'email': 'admin@gmail.com',
        'contact': '4714747475',
        'joining': '31-09-2020',
        'status': 'false'
      }
    ]

    // User Columns Data
    this.user_cols = [
      {'field':'userId', 'header':'User Id'},
      {'field':'empName', 'header':'Employee Name with Photo'},
      {'field':'role', 'header':'Role'},
      {'field':'rating', 'header':'Employee Rating'},
      {'field':'login', 'header':'Login'},
      {'field':'email', 'header':'Email Id'},
      {'field':'contact', 'header':'Contact'},
      {'field':'joining', 'header':'Joining Date'},
      {'field':'status', 'header':'Status'},
    ]

    this.user_selectedColumns = this.user_cols

        // *********************************************SALARY*********************************************
    // DATATABLE CODE

    // SLARY ROWS
    this.rows_sal = [
      {
        serial_No: '1.',
        staffName: 'Mr. Sanket',
        role: 'Manager',
        lastPaid:'45,000 - 05/02/19',
        salaryAmt: '50,000',
        emiAmt: '5000',
        allowance: 'NA',
        deduction: '1500',
        pendingAmt: '-3500',
        payableAmt: '52,000',
        paidAmt: '52,000',
        status: 'paid',
        action: ''

      },
      {
        serial_No: '2.',
        staffName: 'Mr. Prasad',
        role: 'Chef',
        lastPaid:'40,000 - 05/02/19',
        salaryAmt: '50,000',
        emiAmt: '10,000',
        allowance: 'NA',
        deduction: '1500',
        pendingAmt: '+1500',
        payableAmt: '37,000',
        paidAmt: '33,500',
        status: 'pending',
        action: ''

      }
    ]

    // sALARY COLUMNS
    
    this.cols_sal = [

      {field: 'serial_No', header: 'Sr No.'},
     
      {field: 'staffName', header: 'Name of Staff'},

      {field: 'role', header: 'Role'},
      
      {field: 'lastPaid', header: 'Last Salary - Paid On'},
      
      {field: 'salaryAmt', header: 'Salary'},
      
      {field: 'emiAmt', header: 'EMI'},
      
      {field: 'allowance', header: 'Allowances'},
      
      {field: 'deduction', header: 'Deductions'},
      
      {field: 'pendingAmt', header: 'Pending Amount'},
      
      {field: 'payableAmt', header: 'Total Amount of Payable'},
      
      {field: 'paidAmt', header: 'Amount Paid'},
      
      {field: 'status', header: 'Status'},
      
      {field: 'action', header: 'Action'},
      

    ]


     // Selected User Columnns
     this._selectedColSal = this.cols_sal;

  }

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);

  }


  // Show hide Columns for users
  @Input() get userSelectedColumns(): any[] {
    return this.user_selectedColumns;
  }

  set userSelectedColumns(val: any[]){

    this.user_selectedColumns = this.user_cols.filter((col)=>val.includes(col));

    console.log("Users value", this.user_selectedColumns)
  }


  // show hide SALARY
@Input() get salSeletedCols(): any[] {
  return this._selectedColSal
}
  
set salSeletedCols(val: any[]){
  this._selectedColSal = this.cols_sal.filter((col)=>val.includes(col));

  console.log("salary value", this._selectedColSal)
}

  //  Create Role Button

  createRole() {
    const dialogRef = this.matdialog.open(PopCreateRoleComponent, {
      width: '500px'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('res', res)
    })
  }


  // Add user
  onAddUser(){
    this.router.navigate(['/employee-management/add-user'])
  }


  // Navigate to view role
  navtoViewRole(){
    this.router.navigate(['/employee-management/view-role'])
  }


  // Navigate to view user
  navigatetoUserView(){
    this.router.navigate(['/employee-management/view-user'])
  }


  ////////////////////////////////////////SALARY/////////////////////////////////////

  
// *******************************************************
//  SALARY
// ******************************************************

onAddSalary(){
  this.router.navigate(['employee-management/add-salary'])
}

// dialog open add salary button
openAddDialog(value){
  const dialogRef = this.matdialog.open(PopAddSalComponent,{
    width: '800px',
    data: value
  })

// dialog closed
dialogRef.afterClosed().subscribe(res=>{
  console.log('Result', res)
})
  
}

// pay dialog 
openPayDialog(){
  const dialogRef = this.matdialog.open(PopPaySalComponent,{
    width: '750px',
    height: "600px"
  })

  dialogRef.afterClosed().subscribe(res=>{
    console.log("result",res)
  })
}

// PAY LOAN SCREEN
onPayLoan(){
  this.router.navigate(['employee-management/pay-loan'])
}

// on select receiptants
onSelectReceiptant(value){
 
  this.seletedReceiptant = value
  console.log("seleted staff: ", this.seletedReceiptant)
  this.empServ.receiptant = this.seletedReceiptant
}

// pay salary screen
onPaySalary(){
  this.router.navigate(['employee-management/pay-salary'])
}

// pay allownance screnn
onPayAllowance(){
  this.router.navigate(['employee-management/pay-salary'])
}

// pay deduction screen
onPayDeduction(){
  this.router.navigate(['employee-management/pay-salary'])
}

// pay incentives screen
onPayIncentive(){
  this.router.navigate(['employee-management/pay-salary'])
}

// on Loan History
onLoanHistory(){
  this.router.navigate(['employee-management/loan-history'])
}
// on salary History
onSalaryHistory(){
  this.router.navigate(['employee-management/salary-history'])
}

// Receive emi
onReceiveEmi(){
  this.router.navigate(['employee-management/receive-emi'])
}

// view salary
  // view Loan History
  onViewSalary(name){
    console.log('empname', name)
    this.empServ.empSalName = name
    this.router.navigate(['employee-management/view-salary-history'])
  }



}
