import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'kt-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  userForm: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    // form defination

    this.userForm = this.fb.group({
      id: [''],
      name: [''],
      access:[''],
      desc: ['']
    })
  }

}

 