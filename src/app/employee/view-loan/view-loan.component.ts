import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'kt-view-loan',
  templateUrl: './view-loan.component.html',
  styleUrls: ['./view-loan.component.scss']
})
export class ViewLoanComponent implements OnInit {

  empName
  Amtcols = []
  Emicols = []
  rows1 =[
  {'amt': '100000', 'emi':'5000','date':'17-08-2019','irregular':'2'}
  ]

  rows2=[
    {'emi':'1','amt_paid':'5000','due':'365000','emi_date':'22-04-20','paid_date':'30-04-20'},
    {'emi':'2','amt_paid':'5000','due':'325000','emi_date':'17-05-20','paid_date':'28-05-20'},
    {'emi':'3','amt_paid':'5000','due':'305000','emi_date':'20-06-20','paid_date':'25-06-20'}
  ]

  NoLoans = ['','']

  constructor(private empServ: EmployeeService) {
    this.empName = this.empServ.emploanhistory
   }

  ngOnInit() {
  }

}

