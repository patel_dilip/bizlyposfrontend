import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpHomeComponent } from './emp-home/emp-home.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '../views/pages/material/material.module';
import { PrimengModule } from '../views/pages/primeng/primeng.module';
import { PopCreateRoleComponent } from './pop-create-role/pop-create-role.component';
import {TableModule} from 'primeng/table';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms'
import {MultiSelectModule} from 'primeng/multiselect';
import {RatingModule} from 'primeng/rating';
import {TreeModule} from 'primeng/tree';
import { ViewRoleComponent } from './view-role/view-role.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { PopAddSalComponent } from './pop-add-sal/pop-add-sal.component';
import { PopPaySalComponent } from './pop-pay-sal/pop-pay-sal.component';
import { LoanHistoryComponent } from './loan-history/loan-history.component';
import { ReceiveEmiComponent } from './receive-emi/receive-emi.component';
import { SalHistoryComponent } from './sal-history/sal-history.component';
import { AddSalComponent } from './add-sal/add-sal.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { PaySalComponent } from './pay-sal/pay-sal.component';
import {CalendarModule} from 'primeng/calendar';
import { PayLoanComponent } from './pay-loan/pay-loan.component';
import { ViewSalaryComponent } from './view-salary/view-salary.component';
import { ViewLoanComponent } from './view-loan/view-loan.component';
import {TabViewModule} from 'primeng/tabview';
const routes: Routes = [
{
  path:'employee-management',
  component: EmpHomeComponent 
},
{
  path:'employee-management/view-role',
  component: ViewRoleComponent 
},
{
  path: 'employee-management/view-user',
  component: ViewUserComponent
},
{
  path: 'employee-management/add-user',
  component: AddUserComponent
},
// AddSalComponent
{
  path: 'employee-management/add-salary',
  component: AddSalComponent
},
{
  path: 'employee-management/loan-history',
  component: LoanHistoryComponent
},
// SalHistoryComponent
{
  path: 'employee-management/salary-history',
  component: SalHistoryComponent
},
// PaySalComponent
{
  path: 'employee-management/pay-salary',
  component: PaySalComponent
},
{
  path: 'employee-management/pay-loan',
  component: PayLoanComponent
},
{
  path: 'employee-management/receive-emi',
  component: ReceiveEmiComponent
},
// employee-management/view-loan-history
{
  path: 'employee-management/view-loan-history',
  component: ViewLoanComponent
},
//ViewSalaryComponent
{
  path: 'employee-management/view-salary-history',
  component: ViewSalaryComponent
}
]


@NgModule({
  declarations: [EmpHomeComponent, PayLoanComponent, PopCreateRoleComponent, ViewRoleComponent, ViewUserComponent, AddUserComponent, PopAddSalComponent, PopPaySalComponent, LoanHistoryComponent, ReceiveEmiComponent, SalHistoryComponent, AddSalComponent, PaySalComponent, ViewSalaryComponent, ViewLoanComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    PrimengModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    RatingModule,
    TreeModule,
    NgMultiSelectDropDownModule.forRoot(),
    CalendarModule,
    TabViewModule
    ],
    entryComponents: [PopCreateRoleComponent, PopAddSalComponent, PopPaySalComponent]
})
export class EmployeeModule { }
