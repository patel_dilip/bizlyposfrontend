import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PoService } from '../po.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'kt-generate-order',
  templateUrl: './generate-order.component.html',
  styleUrls: ['./generate-order.component.scss'],
  providers: [MessageService]
})
export class GenerateOrderComponent implements OnInit {


  poData: any[] = []
  supplierExclusive: any = [];

  supplierpoList: any = [
    {
       "supplier":"Big Bazaar",
       "date":"11/5/2020",
       "poNumber":"POXXX1",
       "data":[
          {
             "type":"Raw Material",
             "material":"Cheese",
             "product":"NA",
             "unit":"Kilogram",
             "supplier":"Big Bazaar",
             "orderQuantity":1,
             "unitPrice":440,
             "totalPurchase":440,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Kilogram",
                   "value":"Kilogram"
                },
                {
                   "label":"Gram",
                   "value":"Gram"
                }
             ]
          }
       ],
       "outlet":"NA",
       "status":"draft"
    },
    {
       "supplier":"Star Bazaar",
       "date":"11/5/2020",
       "poNumber":"POXXX2",
       "data":[
          {
             "type":"Retailed Food",
             "material":"Kissan",
             "product":"Kissan Jam 200 gram",
             "unit":"Pieces",
             "supplier":"Star Bazaar",
             "orderQuantity":2,
             "unitPrice":54,
             "totalPurchase":108,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          },
          {
             "type":"Retailed Food",
             "material":"Kissan",
             "product":"Kissan Ketchup 500 gram",
             "unit":"Dozens",
             "supplier":"Star Bazaar",
             "orderQuantity":3,
             "unitPrice":55,
             "totalPurchase":165,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          }
       ],
       "outlet":"NA",
       "status":"draft"
    },
    {
       "supplier":"D MART",
       "date":"11/5/2020",
       "poNumber":"POXXX3",
       "data":[
          {
             "type":"Retailed Beverages",
             "material":"Pepsi",
             "product":"Pepsi 200 ml",
             "unit":"Pieces",
             "supplier":"D MART",
             "orderQuantity":4,
             "unitPrice":44,
             "totalPurchase":176,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          },
          {
             "type":"Retailed Beverages",
             "material":"Pepsi",
             "product":"Pepsi 1 Litre",
             "unit":"Pieces",
             "supplier":"D MART",
             "orderQuantity":5,
             "unitPrice":85,
             "totalPurchase":425,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          },
          {
             "type":"Retailed Beverages",
             "material":"Pepsi",
             "product":"Pepsi 2 Ltr",
             "unit":"Dozens",
             "supplier":"D MART",
             "orderQuantity":6,
             "unitPrice":87,
             "totalPurchase":522,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          }
       ],
       "outlet":"NA",
       "status":"draft"
    },
    {
       "supplier":"Krossfire General Store",
       "date":"11/5/2020",
       "poNumber":"POXXX4",
       "data":[
          {
             "type":"Liquor",
             "material":"Breiser",
             "product":"Breiser 2 Ltr",
             "unit":"Pieces",
             "supplier":"Krossfire General Store",
             "orderQuantity":7,
             "unitPrice":64,
             "totalPurchase":448,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          },
          {
             "type":"Liquor",
             "material":"Breiser",
             "product":"Breiser 1 Litre",
             "unit":"Pieces",
             "supplier":"Krossfire General Store",
             "orderQuantity":8,
             "unitPrice":100,
             "totalPurchase":800,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          },
          {
             "type":"Liquor",
             "material":"Breiser",
             "product":"Breiser 200 ml",
             "unit":"Dozens",
             "supplier":"Krossfire General Store",
             "orderQuantity":9,
             "unitPrice":22,
             "totalPurchase":198,
             "editable":false,
             "custom_unit":[
                {
                   "label":"Select Unit",
                   "value":null
                },
                {
                   "label":"Pieces",
                   "value":"Pieces"
                },
                {
                   "label":"Dozens",
                   "value":"Dozens"
                }
             ]
          }
       ],
       "outlet":"NA",
       "status":"draft"
    }
 ]
  date= new Date()


  
  // Discount Accordion
  discountOpenState = false;

  // Free Items Accordion
  freeItemOpenState = false;

  // Extra Charges Accordion
  extraChargesOpenState = false;

  // Notification Accordion
  notificationOpenState = false;

  // rowsdata
  rowsdata = []
  AddFreeTable= [
    {
      material: '',
      product: '',
      unit: '',
      quantity: '',
      priceper: '',
      expect_date: ''
    }
  ]

  // autocomplete variables
  keyword = 'name';
  data = [
     {
       id: 1,
       name: 'Usa'
     },
     {
       id: 2,
       name: 'England'
     }
  ];
  constructor(
    private router: Router, 
    private poServ: PoService,
    private messageService: MessageService
    ) {

      console.log("date here", this.date.toLocaleDateString())
  // Get all Generate Po
  this.getAllGeneratedPo();
   }

  ngOnInit() {
    this.rowsdata = [
      {code: 's00', name: 'da', category: 'sldf', quantity: 'dlas'},
      {code: 's00', name: 'da', category: 'sldf', quantity: 'dlas'},
      {code: 's00', name: 'da', category: 'sldf', quantity: 'dlas'}
    ]
  }

  // GET ALL GENERATING PO
  getAllGeneratedPo(){
   
    new Promise((resolve, reject)=>{
      this.poData = this.poServ.generatePO
      resolve(this.poData)
    }).then(data=>{
     let i =0
      this.poData.forEach(element => {
        this.rowsdata.push(element)
        console.log('element',element.supplier)
        if(!this.supplierExclusive.includes(element.supplier)){
         
          this.supplierExclusive.push(element.supplier)
          i++
          let po = 'POXXX'+i
          this.supplierpoList.push(
            {
              supplier: element.supplier, 
              date:this.date.toLocaleDateString(),
              poNumber: po, 
              data: [element],
              outlet: 'NA',
              status: 'draft'
            }
            )

        }
        else{         
          this.supplierpoList.forEach(ele => {
            new Promise((resolve,reject)=>{
              resolve(ele)
            }).then(item=>{
              if(element.supplier == ele.supplier){
                ele.data.push(element)
              }


            })
          });

        }

        console.log('supplier exclusives', this.supplierExclusive)
        console.log('supplier po list', this.supplierpoList)
      });
    })
  }

  // SUBMIT PURCHASE ORDER
  onSubmitPo(obj){
   obj.status = 'approve'

   this.messageService.add({ severity: 'success', summary: 'Product Added to List' });
  }


  // ON CLICK PO LIST
  onPoList(){
    this.poServ.generatePO = this.supplierpoList
    console.log('generate po', this.poServ.generatePO)
    this.router.navigate(['/purchase-order'])
  }

  // AutoComplete

  selectEvent(item) {
    // do something with selected item
  }
 
  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }
  
  onFocused(e){
    // do something when input is focused
  }

}
