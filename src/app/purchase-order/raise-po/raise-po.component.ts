import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { PoService } from '../po.service';

import { MessageService } from 'primeng/api';



@Component({
  selector: 'kt-raise-po',
  templateUrl: './raise-po.component.html',
  styleUrls: ['./raise-po.component.scss'],
  providers: [MessageService]
})



export class RaisePoComponent implements OnInit {

  myInventdata: any = []
  rowsData: any = []
  materialSection: any = []
  productSection: any = []
  suppliers: any = []
  // editing: boolean = false;


  constructor(
    private poServ: PoService,
    private messageService: MessageService,
    private router: Router
  ) {

    //  GET ALL INVENTORY
    this.getAllInventory()
  }

  ngOnInit() {
    this.suppliers = [
      {label : 'Select Supplier', value: null},
      {label : 'Big Bazaar', value: 'Big Bazaar'},
      {label : 'Star Bazaar', value: 'Star Bazaar'},
      {label : 'D MART', value: 'D MART'},
      {label : 'Krossfire General Store', value: 'Krossfire General Store'},
    ]
  }

  // #1 GET ALL INVENTORY

  getAllInventory() {
    // GET ALL INVENTRORY
    this.poServ.getMyInventory().subscribe(data => {
      this.myInventdata = data
      console.log("My Inventory Data", this.myInventdata)
      this.myInventdata.forEach(element => {
        new Promise((resolve, reject) => {
          // Material Section
          if (element.materials.material_type == "Raw Material") {
            element.addStatus = false;
            this.materialSection.push(element)
          }
          else {

            this.productSection.push(element)
            element.materials.products.forEach(ele => {
              new Promise((resolve, reject) => {
                ele.addStatus = false
              }).then(status => {
                console.log('product obj', ele)
              })
            });
          }
        })
      });
    })


    // Material Section
    console.log("Material Section", this.materialSection)

    // Product Section
    console.log("Product Section", this.productSection)

    // ROWS DATA
    // this.rowsData.push(
    //   {date: '', outlet: 'ABC', poNumber: 'NA', supplier: 'BIG BAZAAR', material: 'PANEER', product: 'NA',amount: '200',expected:'NA',status: false},
    //   {date: '', outlet: 'DEF', poNumber: 'NA', supplier: 'STAR BAZAAR', material: 'COFFEE', product: 'NESCAFE',amount: '500',expected:'NA',status: false},
    //   {date: '', outlet: 'GHI', poNumber: 'NA', supplier: 'D MART', material: 'KISSAN', product: 'JAM',amount: '800',expected:'NA',status: false},
    //   {date: '', outlet: 'JKL', poNumber: 'NA', supplier: 'GROFERS', material: 'RICE', product: 'INDIA GATE',amount: '900',expected:'NA',status: false},
    //   {date: '', outlet: 'MNO', poNumber: 'NA', supplier: 'D MART', material: 'PEPSI', product: 'PEPSI 200 ML',amount: '400',expected:'NA',status: false}
    //   )

  }


  // ******************************ADD INVENTORY TO LIST***********************
  onAddList(element, product) {



    if (element.materials.material_type == "Raw Material") {


      // CUSTOM UNIT LOGIC FOR MATERIALS
      let custom = [{ label: 'Select Unit', value: null }]
      element.materials.materail_unit.forEach(item => {
        new Promise((resolve, reject) => {
          resolve(item)
        }).then(var1 => {
          let obj = { label: item.unit_name, value: item.unit_name }
          custom.push(obj)
        })
      });


      element.addStatus = true
      let tableObj = {
        type: element.materials.material_type,
        material: element.materials.material_name,
        product: 'NA',
        unit: null,
        supplier: element.supplier_details.supplier_name,
        orderQuantity: null,
        unitPrice: null,
        totalPurchase: 0,
        editable: false,
        custom_unit: custom
      }
      this.rowsData.push(tableObj)
      this.messageService.add({ severity: 'success', summary: 'Material Added to List' });
    } else {

      // >>>>>>>>>>>>>>>>>>>>>>>CUSTOM UNIT LOGIC FOR Producsts
      let custom = [{ label: 'Select Unit', value: null }]
      element.materials.products.forEach(item => {
        new Promise((resolve, reject) => {
          resolve(item)
        }).then(var1 => {
          // ******************************IF CONDITIONS
          if (item.product_name == product.product_name) {
            item.product_unit.forEach(element => {
              new Promise((resolve, reject) => {
                resolve(element)
              }).then(val => {
                let obj = { label: element.unit_name, value: element.unit_name }
                custom.push(obj)
              })
            });
          }   //>>>>>>>>>>>>>>>>>>>>> IF CONDITION IS CLOSED
        })

      });

      product.addStatus = true
      let tableObj = {
        type: element.materials.material_type,
        material: element.materials.material_name,
        product: product.product_name,
        unit: null,
        supplier: element.supplier_details.supplier_name,
        orderQuantity: null,
        unitPrice: null,
        totalPurchase: 0,
        editable: false,
        custom_unit: custom
      }
      this.rowsData.push(tableObj)
      this.messageService.add({ severity: 'success', summary: 'Product Added to List' });
    }






    console.log("Table Rows Data", this.rowsData)
  }


  // ON CLICK EDIT
  onRowEditInit(row) {

    row.editable = false

  }
  onRowEditSave(row) {
    row.editable = true
  }

  onRowEditCancel(row, index) {
    row.editable = true

    this.rowsData.splice(index, 1)
    this.messageService.add({ severity: 'warn', summary: 'Item Removed from List' });


    // ****************************** ADD STATUS CHANGE FOR INVENTORY OVERLAY ADD BUTTON******************

    if (row.type == 'Raw Material') {
      console.log("Material NAME", row.material)

      this.materialSection.forEach(element => {
        new Promise((resolve, reject) => {
          resolve(element)
        }).then(item => {
          if (element.materials.material_name == row.material) {
            element.addStatus = false
          }
        })
      });
    }
    else {
      console.log('Product', row.product)
      this.productSection.forEach(element => {
        new Promise((resolve, reject) => {
          resolve(element)
        }).then(item => {
          element.materials.products.forEach(ele => {
            new Promise((resolve, reject) => {
              resolve(ele)
            }).then(item1 => {
              if (ele.product_name == row.product) {
                ele.addStatus = false
              }
            })
          });
        })
      });
    }
    // ************************** CLOSING ADD STATUS CHANGE FOR INVENTORY OVERLAY ADD BUTTON******************





  }




  // *********************************CALCULATE ORDER AND PRICE******************
  calUnitPrice(row){
   

   let orderval = parseInt(row.orderQuantity)
   let priceval = parseInt(row.unitPrice)
  let result = orderval * priceval
   row.totalPurchase = result

    
  }


  // generate PO FUNCTIONS

  generatePo(){

    this.poServ.generatePO = this.rowsData
    console.log('generate po', this.poServ.generatePO)
  this.router.navigate(['purchase-order/generate-po'])

  }

}

