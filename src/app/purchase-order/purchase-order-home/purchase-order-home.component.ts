import { Component, OnInit } from '@angular/core';
import { reject } from 'lodash';
import {MenuItem} from 'primeng/api';
import Swal from 'sweetalert2';
import { PoService } from '../po.service';

@Component({
  selector: 'kt-purchase-order-home',
  templateUrl: './purchase-order-home.component.html',
  styleUrls: ['./purchase-order-home.component.scss']
})
export class PurchaseOrderHomeComponent implements OnInit {

  myAllInventory: any = []
  poListHome: any = []

  supplierExclusive = []
  purchaseOrderList: any = [
    {
      "supplier":"Big Bazaar",
      "date":"11/5/2020",
      "poNumber":"POXXX1",
      "expected_date": "",
      "data":[
         {
            "type":"Raw Material",
            "material":"Cheese",
            "product":"NA",
            "unit":"Kilogram",
            "supplier":"Big Bazaar",
            "orderQuantity":61,
            "unitPrice":440,
            "totalPurchase":440,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Kilogram",
                  "value":"Kilogram"
               },
               {
                  "label":"Gram",
                  "value":"Gram"
               }
            ] 
         }
      ],
      "outlet":"NA",
      "status":"draft"
   },
   {
      "supplier":"Star Bazaar",
      "date":"11/5/2020",
      "poNumber":"POXXX2",
      "expected_date": "",
      "data":[
         {
            "type":"Retailed Food",
            "material":"Kissan",
            "product":"Kissan Jam 200 gram",
            "unit":"Pieces",
            "supplier":"Star Bazaar",
            "orderQuantity":85,
            "unitPrice":54,
            "totalPurchase":108,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         },
         {
            "type":"Retailed Food",
            "material":"Kissan",
            "product":"Kissan Ketchup 500 gram",
            "unit":"Dozens",
            "supplier":"Star Bazaar",
            "orderQuantity":20,
            "unitPrice":55,
            "totalPurchase":165,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         }
      ],
      "outlet":"NA",
      "status":"draft"
   },
   {
      "supplier":"D MART",
      "date":"11/5/2020",
      "poNumber":"POXXX3",
      "expected_date": "",
      "data":[
         {
            "type":"Retailed Beverages",
            "material":"Pepsi",
            "product":"Pepsi 200 ml",
            "unit":"Pieces",
            "supplier":"D MART",
            "orderQuantity":70,
            "unitPrice":44,
            "totalPurchase":176,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         },
         {
            "type":"Retailed Beverages",
            "material":"Pepsi",
            "product":"Pepsi 1 Litre",
            "unit":"Pieces",
            "supplier":"D MART",
            "orderQuantity":33,
            "unitPrice":85,
            "totalPurchase":425,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         },
         {
            "type":"Retailed Beverages",
            "material":"Pepsi",
            "product":"Pepsi 2 Ltr",
            "unit":"Dozens",
            "supplier":"D MART",
            "orderQuantity":7,
            "unitPrice":87,
            "totalPurchase":522,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         }
      ],
      "outlet":"NA",
      "status":"draft"
   },
   {
      "supplier":"Krossfire General Store",
      "date":"11/5/2020",
      "poNumber":"POXXX4",
      "expected_date": "",
      "data":[
         {
            "type":"Liquor",
            "material":"Breiser",
            "product":"Breiser 2 Ltr",
            "unit":"Pieces",
            "supplier":"Krossfire General Store",
            "orderQuantity":12,
            "unitPrice":64,
            "totalPurchase":448,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         },
         {
            "type":"Liquor",
            "material":"Breiser",
            "product":"Breiser 1 Litre",
            "unit":"Pieces",
            "supplier":"Krossfire General Store",
            "orderQuantity":68,
            "unitPrice":100,
            "totalPurchase":800,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         },
         {
            "type":"Liquor",
            "material":"Breiser",
            "product":"Breiser 200 ml",
            "unit":"Dozens",
            "supplier":"Krossfire General Store",
            "orderQuantity":71,
            "unitPrice":22,
            "totalPurchase":198,
            "editable":false,
            "custom_unit":[
               {
                  "label":"Select Unit",
                  "value":null
               },
               {
                  "label":"Pieces",
                  "value":"Pieces"
               },
               {
                  "label":"Dozens",
                  "value":"Dozens"
               }
            ] 
         }
      ],
      "outlet":"NA",
      "status":"draft"
   }
  ]

  rowsData: any = []
   goodReceiveArr: any = [];
   itemsLen: any;
  totalValue: any;
constructor(private poServ: PoService){

  // GET ALL My Inventory Funciton
    this.getAllMyInventory();


    // GET PO LIST HOME
    // this.getAllPoListHome();
}

ngOnInit(){}

// GET ALL MY Inventory

getAllMyInventory(){
  this.poServ.getMyInventory().subscribe(data=>{

    this.myAllInventory = data;
    console.log("Get All MY Invenotory", this.myAllInventory)
    
  })
}


// GET ALL PO LIST HOME
getAllPoListHome(){
  this.purchaseOrderList = this.poServ.generatePO
  console.log('purchase order list', this.purchaseOrderList)
}

// GOOD RECEIVE FUNCTION DATA
goodReceive(data){

   new Promise((resolve,reject)=>{
     
      resolve(data)
   }).then(item=>{
      this.goodReceiveArr = data

      this.itemsLen = this.goodReceiveArr.data.length
      let value = 0
      this.goodReceiveArr.data.forEach(element => {
         new Promise((resolve, reject)=>{
            resolve(element)
           
         }).then(item1=>{
            value = element.totalPurchase+ value
            console.log('product: ', element.product)
            console.log('value:', element.totalPurchase)

            this.totalValue = value
         })
      });
      console.log("Good Receive array data", this.goodReceiveArr);
   })
  
   // console.log("Good Receive array data", this.goodReceiveArr);
   
}


testdate(value){
   console.log('date expected', value)
}


}