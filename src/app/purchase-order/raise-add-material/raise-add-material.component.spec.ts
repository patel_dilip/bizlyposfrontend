import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaiseAddMaterialComponent } from './raise-add-material.component';

describe('RaiseAddMaterialComponent', () => {
  let component: RaiseAddMaterialComponent;
  let fixture: ComponentFixture<RaiseAddMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaiseAddMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaiseAddMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
