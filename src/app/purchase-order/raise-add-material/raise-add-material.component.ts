import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'kt-raise-add-material',
  templateUrl: './raise-add-material.component.html',
  styleUrls: ['./raise-add-material.component.scss']
})
export class RaiseAddMaterialComponent implements OnInit {

  materialForm: FormGroup

  units = [
    { label: 'KG/Grams', value: 'KG/Grams' },
    { label: 'Ltr./ml', value: 'Ltr./ml' },
    { label: 'Pieces/Dozens', value: 'Pieces/Dozens' }
  ]

  categories = [
    { label: 'Select Category', value: null },
    { label: 'Raw Material', value: 'Raw Material' },
    { label: 'Retailed Food', value: 'Retailed Food' },
    { label: 'Retailed Beverages', value: 'Retailed Beverages' },
    { label: 'Liquor', value: 'Liquor' },
  ]
  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.materialForm = this.fb.group({
      material_name: [''],
      select_category: [''],
      unit: [''],
      current_stock:['']
    })
  }

}
