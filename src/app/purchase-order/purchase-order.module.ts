import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { FormsModule } from '@angular/forms';

// MATERIAL CARD
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {OverlayModule} from '@angular/cdk/overlay';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatExpansionModule} from '@angular/material/expansion';

// PRIME NG MODULES
import {TabViewModule} from 'primeng/tabview';
import {TooltipModule} from 'primeng/tooltip';
import {SelectButtonModule} from 'primeng/selectbutton';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputNumberModule} from 'primeng/inputnumber';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {ButtonModule} from 'primeng/button';
import {OverlayPanelModule} from 'primeng/overlaypanel';


// COMPONENTS
import { PurchaseOrderHomeComponent } from './purchase-order-home/purchase-order-home.component';
import { RaisePoComponent } from './raise-po/raise-po.component';
import {SidebarModule} from 'primeng/sidebar';
import {TableModule} from 'primeng/table';
import { RaiseAddMaterialComponent } from './raise-add-material/raise-add-material.component';
import { GenerateOrderComponent } from './generate-order/generate-order.component';
import {ToastModule} from 'primeng/toast';
// npmjs
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { GoodsReceiveComponent } from './goods-receive/goods-receive.component';

const routes: Routes = [
  {
    path: 'purchase-order',
    component: PurchaseOrderHomeComponent
  },
  {
    path: 'purchase-order/raising-po',
    component: RaisePoComponent
  },
  {
    path: 'purchase-order/generate-po',
    component: GenerateOrderComponent
  },
  {
    path: 'purchase-order/goods-receive',
    component: GoodsReceiveComponent
  }
]

@NgModule({
  declarations: [
    PurchaseOrderHomeComponent,
    RaisePoComponent,
    RaiseAddMaterialComponent,
    GenerateOrderComponent,
    GoodsReceiveComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    TabViewModule,
    SidebarModule,
    TableModule,
    MatProgressSpinnerModule,
    FormsModule,
    TooltipModule,
    OverlayModule,
    MatDialogModule,
    ReactiveFormsModule,
    SelectButtonModule,
    DropdownModule,
    MatSidenavModule,
    DialogModule,
    InputSwitchModule,
    InputNumberModule,
    AutoCompleteModule,
    NgMultiSelectDropDownModule.forRoot(),
    SelectDropDownModule,
    ButtonModule,
    ToastModule,
    MatExpansionModule,
    AutocompleteLibModule,
    OverlayPanelModule   
  ],
  entryComponents: [RaiseAddMaterialComponent]
})
export class PurchaseOrderModule { }
