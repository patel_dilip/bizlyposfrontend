import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PoService {


  // Generate po 
  generatePO: any[] = [];

  constructor(private http: HttpClient) { }

  ////////////////////////////////////// GET ALL MY INVENTORY FUNCTIONS////////////////////////////////

  getMyInventory(){
    return this.http.get('/assets/po/po_json_objects/new_inventory.json')
  }


///////////////////////////////////// GET ALL MY PO LIST HOME FUNCTIONS////////////////////////////////

getAllPoList(){
return this.http.get('/assets/po/po_json_objects/po_list_home.json')
}

}
