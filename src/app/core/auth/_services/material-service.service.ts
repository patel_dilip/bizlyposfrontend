import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { FormArray } from "@angular/forms";

@Injectable({
  providedIn: "root",
})
export class MaterialServiceService {
  SelectedItems: any = [];
  AddMaterialObj: any = [];
  formobj: any = [];
  POdetails: FormArray;
  editPO: any;
  editable = true;
  selectedProducts: any = [];
  selectedMaterialsAddMaterialPopUp: any = [];
  selectedProductsAddMaterialPopUp: any = [];
  selectedProductsRaisePopUp = [];
  patchPreviousData: boolean = false;
  previousData: any = [];
  addStockData;
  addStockDataArr:any = [];
  filteredPODetails: any = [];
  //Flag to do autocomplete feature in stock and live
  liqourData: boolean = false;
  allCategoriesData: boolean = false;

  // Selected Suppliers Data
  selectedSupplier: any = [];

  constructor(private http: HttpClient) {}
  //*********Get All beverages category******** */
  getAllCategories() {
    return this.http.get(
      environment.base_Url + "allcategoriestree/getallCategories"
    );
  }
  //get all categories of asset inventory
  getAllAssetInventoryCategories() {
    return this.http.get(
      environment.base_Url + "inventoryassets/getallrootinventoryassetscategory"
    );
  }
  //*****get all suppliers******/
  getAllSupliers() {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/supplier/allsuppliers"
    );
  }
  //*******add supplier ********/
  addSupplier(obj) {
    return this.http.post(
      "http://bizlypos.com:1400/bizlypos/supplier/addsupplier",
      obj
    );
  }
  //******post Material for live inventory********/
  postMaterial(obj) {
    return this.http.post(
      "http://bizlypos.com:1400/bizlypos/liveinventory/restaurantliveinventoryaddmaterial",
      obj
    );
  }
  //*******post product for live inventory*****/
  postProduct(obj) {
    return this.http.post(
      "http://bizlypos.com:1400/bizlypos/restaurantproduct/restaurantproductadd",
      obj
    );
  }
  //******get All Material Live Inventory*******/
  getAllLiveInevntory(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/liveinventory/restaurantliveinventorymaterials/" +
        id
    );
  }
  //******get all Product Inventory***** */
  getAllProductIventory(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/restaurantproduct/restaurantproducts/" +
        id
    );
  }
  //*****post add Purchase Orders******/
  postPurchaseOrder(obj) {
    return this.http.post(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/addpurchaseorder",
      obj
    );
  }
  //******get all Purchase Orders******/
  getAllPurchaseOrders(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/allpurchaseorders/" + id
    );
  }
  //********get all Submitted Review Purchase Order***** */
  getAllPurchaseOrdersReview(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/reviewedpurchaseorders/" +
        id
    );
  }
  //*********get all Aproved Purchase Oder********* */
  getAllPurchaseOrdersAproved(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/approvedpurchaseorders/" +
        id
    );
  }

  //*********post Draft Purchase Order****** */

  getAllPurchaseOrdersDrafts(id) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/draftpurchaseorders/" +
        id
    );
  }
  //************Get all current Stock for Porchase order add to cart*********/
  getCurrentStock() {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/liveinventory/getmaterialunitandstocks"
    );
  }

  deleteMaterial(restaurantId, materialId) {
    return this.http.put(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/deletematerial/" +
        restaurantId +
        "/" +
        materialId,
      {}
    );
  }

  updatePO(editPO) {
    return this.http.put(
      "http://bizlypos.com:1400/bizlypos/purchaseorder/updatepurchaseorder",
      editPO
    );
  }

  //*****food inventory add material to subsubcategorytype*****/
  putMaterialForFoodInventory(rootcategoryid, subsubcategoryid, obj) {
    return this.http.put(
      environment.base_Url +
        "fooodinventory/addsubsubcategorytype/" +
        rootcategoryid +
        "/" +
        subsubcategoryid,
      obj
    );
  }

  //*****retail add material to subsubcategorytype*****/
  putMaterialForRetail(rootcategoryid, subsubcategoryid, obj) {
    return this.http.put(
      environment.base_Url +
        "retailfood/addretailfoodsubsubcategorytype/" +
        rootcategoryid +
        "/" +
        subsubcategoryid,
      obj
    );
  }

  //*****beverages add material to subsubcategorytype*****/
  putMaterialForBeverages(rootcategoryid, subsubcategoryid, obj) {
    return this.http.put(
      environment.base_Url +
        "beveragescategory/addbeveragescategorysubsubcategorytype/" +
        rootcategoryid +
        "/" +
        subsubcategoryid,
      obj
    );
  }

  //*****liquor add material to subsubcategorytype*****/
  putMaterialForLiqour(rootcategoryid, subsubcategoryid, obj) {
    return this.http.put(
      environment.base_Url +
        "drinktype/addliquorsubsubvarient/" +
        rootcategoryid +
        "/" +
        subsubcategoryid,
      obj
    );
  }

  //***** get single material data ******/
  getMaterialData(materialname) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/liveinventory/restauranliveinventorymaterialbyname/" +
        materialname
    );
  }
  // getSingleSupplierPO
  getSingleSupplierPO(supplierId) {
    return this.http.get(
      "http://bizlypos.com:1400/bizlypos/supplier/singlesupplier/" + supplierId
    );
  }

  putSupplier(supplierId, supplierData) { 
    return this.http.put("http://bizlypos.com:1400/bizlypos/supplier/updtesupplier/" + supplierId, supplierData);
  }

  getCities(){
    return this.http.get(
      "https://indian-cities-api-nocbegfhqg.now.sh/cities"
    )
  }
}
