export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
				{
					title: 'Dashboards',
					root: true,
					alignment: 'left',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
				},
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'flaticon2-architecture-and-city',
					page: '/dashboard',
					translate: 'MENU.DASHBOARD',
					bullet: 'dot',
				},
				{
					title: '',
					root: true,
					icon: 'flaticon2-expand',
					page: '/builder'
				},
				{section: 'Components'},
				
				{
					title: 'POS',
					root: true,
					bullet: 'dot',
					icon: 'flaticon2-expand',
					submenu: [
						{
							title: 'Live Inventory',
							page: '/ngbootstrap/live-inventory'
						},
						{
							title: 'Live Inventory (new)',
							page: '/live-inventory'
						},
						{
							title: 'Stock View',
							page: '/ngbootstrap/stockview'
						},
						{
							title: 'Closing Stock Audit',
							page: '/ngbootstrap/closing-stock-audit'
						},
						{
							title: 'Good Received',
							page: '/ngbootstrap/good-received'
						},
						{
							title: 'Purchase Order',
							page: '/ngbootstrap/purchase-order'
						},
						{
							title: 'Purchase Order (new)',
							page: '/purchase-order' 
						},
						{
							title: 'Variance',
							page: '/ngbootstrap/variance'
						},
						{
							title: 'Bulk Order',
							page: '/ngbootstrap/bulk-order'
						},
						{
							title: 'Employee Management',
							page: '/employee-management'
						},
						{
							title: 'Menu Management',
							page: '/menu-management'							
						},
						{
							title: 'Cash Management',
							page: '/ngbootstrap/cash-management'
						},
						{
							title: 'Suppliers',
							page: '/ngbootstrap/suppliers-dashboard'
						}
					]
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
