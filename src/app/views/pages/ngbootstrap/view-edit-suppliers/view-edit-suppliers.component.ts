import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MaterialServiceService } from "../../../../core/auth/_services/material-service.service";
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from "@angular/material/snack-bar";
import { state } from "@angular/animations";

@Component({
  selector: "kt-view-edit-suppliers",
  templateUrl: "./view-edit-suppliers.component.html",
  styleUrls: ["./view-edit-suppliers.component.scss"],
})
export class ViewEditSuppliersComponent implements OnInit {
  supplierData: any;
  supplierPODetails: any;
  horizontalPosition: MatSnackBarHorizontalPosition = "start";
  verticalPosition: MatSnackBarVerticalPosition = "bottom";

  state_city_data : any;

  // Dealing Categories
  showBevCategories: boolean = false;
  showIntCategories: boolean = false;
  showLiqCategories: boolean = false;
  showRetCategories: boolean = false;

  //Beverage Category
  BeverageCategory: any = [];
  allCategories: any = [];
  categoryData: any = [];
  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];

  //Food Inventory
  InventoryFoodCategory: any = [];
  categoryInvData: any = [];
  InvfirstLevel: any = [];
  InvsecondLevel: any = [];
  InvthirdLevel: any = [];
  InvthirdLevel2: any = [];
  InvforthLevel: any = [];
  InvfiftLevel: any = [];

  //Retail Category
  retailFoodCategory: any = [];
  categoryRetData: any = [];
  RetfirstLevel: any = [];
  RetsecondLevel: any = [];
  RetthirdLevel: any = [];
  RetthirdLevel2: any = [];
  RetforthLevel: any = [];
  RetfiftLevel: any = [];

  //Liqour Category
  LiquorCategory: any = [];
  categoryLiqData: any = [];
  LiqfirstLevel: any = [];
  LigsecondLevel: any = [];
  LiqthirdLevel: any = [];
  LiqthirdLevel2: any = [];
  LiqforthLevel: any = [];
  LiqfiftLevel: any = [];
  CancatAllArray: any = [];

  // dealing product
  productArr = [];
  selected = -1;

  public editable = this.material_service.editable;
  constructor(
    private route: Router,
    private _snackBar: MatSnackBar,

    private material_service: MaterialServiceService
  ) {
    console.log(this.supplierData);
    this.supplierData = this.material_service.selectedSupplier;
    console.log(this.supplierData);
    this.material_service
      .getSingleSupplierPO(this.supplierData[0]._id)
      .subscribe((res) => {
        console.log(res);
        this.supplierPODetails = res["data"];
        console.log(this.supplierPODetails);

        this.supplierPODetails = this.supplierPODetails.orders;
        console.log(this.supplierPODetails);
      });

    // dealing categories
    this.getallCategories();
    this.getallInvCategories();
    this.getallRetCategories();
    this.getallLiqCategories();
    this.patchProduct();

    this.material_service.getCities().subscribe(res => {
      console.log(res);
      this.state_city_data = res;
    })

    console.log("State : ",this.state_city_data);
  }

  ngOnInit() {}

  navigateToDashboard() {
    this.material_service.selectedSupplier = [];
    this.route.navigateByUrl("/ngbootstrap/suppliers-dashboard");
  }

  editSupplier() {
    console.log("click edit");
    this.editable = false;
  }

  editSupplierName(supplier) {
    console.log(supplier);
    this.supplierData[0].supplierDetails.supplierName = supplier;
  }

  editSupplierState(state) {
    console.log(state);
    this.supplierData[0].supplierDetails.state = state;
  }

  editSupplierCity(city) {
    console.log(city);
    this.supplierData[0].supplierDetails.city = city;
  }

  editSupplierAddress1(line1) {
    console.log(line1);
    this.supplierData[0].supplierDetails.address.line1 = line1;
  }

  editSupplierAddress2(line2) {
    console.log(line2);
    this.supplierData[0].supplierDetails.address.line2 = line2;
  }

  editSupplierContact(suppliercontact, i) {
    console.log(suppliercontact);
    this.supplierData[0].supplierDetails.supplierContactNo[
      i
    ].contactNo = suppliercontact;
  }

  editSupplierWhatsappContact(supplierwhatsappcontact, i) {
    console.log(supplierwhatsappcontact);
    this.supplierData[0].supplierDetails.supplierWhatsappNo[
      i
    ].whatsappNo = supplierwhatsappcontact;
  }

  editSupplierEmail(supplieremail, i) {
    console.log(supplieremail);
    this.supplierData[0].supplierDetails.supplierEmail[i].email = supplieremail;
  }

  editSupplierWebsite(supplierwebsite) {
    console.log(supplierwebsite);
    this.supplierData[0].supplierDetails.supplierWebsite = supplierwebsite;
  }

  editGST(gst) {
    console.log(gst);
    this.supplierData[0].supplierDetails.gstNo = gst;
  }

  editContactPersonName(contactpersonname) {
    console.log(contactpersonname);
    this.supplierData[0].contactPersonDetails.contactPersonName = contactpersonname;
  }

  editContactPersonDesignation(contactpersondesignation) {
    console.log(contactpersondesignation);
    this.supplierData[0].contactPersonDetails.contactPersonDesignation = contactpersondesignation;
  }

  editContactPersonNumber(contactpersonnumber, id) {
    console.log(contactpersonnumber);
    // this.supplierData[0].contactPersonDetails.contactPersonNo[
    //   i
    // ].contactNo = contactpersonnumber;
    // var index = this.supplierData[0].contactPersonDetails.contactPersonNo.indexOf(
    //   contactpersonnumber
    // );
    // console.log(index);

    // if (index !== -1) {
    //   this.supplierData[0].contactPersonDetails.contactPersonNo[
    //     index
    //   ].contactNo = contactpersonnumber;
    // }
    // console.log(contactpersonnumber);

    this.supplierData[0].contactPersonDetails.contactPersonNo.forEach(
      (ele, i) => {
        if (ele._id == id) {
          this.supplierData[0].contactPersonDetails.contactPersonNo[
            i
          ].contactNo = contactpersonnumber;
        }
      }
    );
  }

  editContactPersonEmail(contactpersonemail, id) {
    console.log(contactpersonemail);
    // this.supplierData[0].contactPersonDetails.contactPersonEmail[
    //   i
    // ].email = contactpersonemail;
    // var index = this.supplierData[0].contactPersonDetails.contactPersonEmail.indexOf(
    //   contactpersonemail
    // );
    // console.log(index);

    // if (index !== -1) {
    //   this.supplierData[0].contactPersonDetails.contactPersonEmail[
    //     index
    //   ].email = contactpersonemail;
    // }
    // console.log(contactpersonemail);
    this.supplierData[0].contactPersonDetails.contactPersonEmail.forEach(
      (ele, i) => {
        if (ele._id == id) {
          this.supplierData[0].contactPersonDetails.contactPersonEmail[
            i
          ].email = contactpersonemail;
        }
      }
    );
  }

  getallCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe((data) => {
        //*****All Categories*****/
        this.allCategories = data;
        console.log("This is All Categories", this.allCategories);

        //*****All Bevrages*******/
        this.BeverageCategory = data["BeverageCategory"];
        console.log("all Bevrages categories", this.BeverageCategory);

        resolve(this.BeverageCategory);
      });
    }).then((id) => {
      console.log("this is id", id);
      this.categoryData = id;
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories);
      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel);
    });
  }

  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);

    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach((element) => {
        // console.log("element", element);
        element.forEach((ele) => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories);
        });
      });
      resolve(this.secondLevel);
    }).then((sid) => {
      this.thirdLevel = sid;

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach((element) => {
        element.forEach((ele) => {
          this.thirdLevel2.push(ele);
          this.forthLevel.push(ele.subSubCategoryType);
        });
      });
      this.forthLevel.forEach((element1) => {
        element1.forEach((ele) => {
          this.fiftLevel.push(ele);
        });
      });

      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);
    });
  }

  getallInvCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe((data) => {
        this.InventoryFoodCategory = data["InventoryFoodCategory"];
        console.log("this is Inventory Category", this.InventoryFoodCategory);

        resolve(this.InventoryFoodCategory);
      });
    }).then((id) => {
      console.log("this is inventory id", id);
      this.categoryInvData = id;
      console.log("Food Inventory category data", this.categoryInvData);

      this.categoryInvData.forEach((element) => {
        this.InvfirstLevel.push(element.subCategories);
      });
      console.log("sub category", this.InvfirstLevel);
      this.categoriesInvChild(this.InvfirstLevel);
    });
  }
  categoriesInvChild(data) {
    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach((element) => {
        element.forEach((ele) => {
          this.InvsecondLevel.push(ele.subSubCategories);
        });
      });
      resolve(this.InvsecondLevel);
    }).then((sid) => {
      this.InvthirdLevel = sid;

      console.log("third level", this.InvthirdLevel);

      this.InvthirdLevel.forEach((element) => {
        element.forEach((ele) => {
          this.InvthirdLevel2.push(ele);
          this.InvforthLevel.push(ele.subSubCategoryType);
        });
      });
      this.InvforthLevel.forEach((element1) => {
        element1.forEach((ele) => {
          this.InvfiftLevel.push(ele);
        });
      });

      console.log(
        "this is fourth level final of food inventory",
        this.InvforthLevel
      );
      console.log(
        "this is fifth level final of food inventory",
        this.InvfiftLevel
      );
    });
  }
  getallRetCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe((data) => {
        this.retailFoodCategory = data["retailFoodCategory"];
        console.log("all retail food ", this.retailFoodCategory);
        resolve(this.retailFoodCategory);
      });
    }).then((id) => {
      console.log("this is Reatil id", id);
      this.categoryRetData = id;
      console.log("Retail category data", this.categoryRetData);

      this.categoryRetData.forEach((element) => {
        this.RetfirstLevel.push(element.subCategories);
      });
      console.log("sub category", this.RetfirstLevel);
      this.categoriesRetChild(this.RetfirstLevel);
    });
  }
  categoriesRetChild(data) {
    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach((element) => {
        element.forEach((ele) => {
          this.RetsecondLevel.push(ele.subSubCategories);
        });
      });
      resolve(this.RetsecondLevel);
    }).then((sid) => {
      this.RetthirdLevel = sid;

      console.log("third level", this.RetthirdLevel);

      this.RetthirdLevel.forEach((element) => {
        element.forEach((ele) => {
          this.RetthirdLevel2.push(ele);
          this.RetforthLevel.push(ele.subSubCategoryType);
        });
      });
      this.RetforthLevel.forEach((element1) => {
        element1.forEach((ele) => {
          this.RetfiftLevel.push(ele);
        });
      });

      console.log("this is fourth level final of Retail", this.RetforthLevel);
      console.log("this is fifth level final of Retail", this.RetfiftLevel);
    });
  }
  getallLiqCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe((data) => {
        this.LiquorCategory = data["LiquorCategory"];
        console.log("all Liqoure Categories", this.LiquorCategory);
        resolve(this.LiquorCategory);
      });
    }).then((id) => {
      console.log("this is Reatil id", id);
      this.categoryLiqData = id;
      console.log("Liqour category data", this.categoryLiqData);

      this.categoryLiqData.forEach((element) => {
        this.LiqfirstLevel.push(element.liquorVarients);
      });
      console.log("sub category", this.LiqfirstLevel);
      this.categoriesLiqChild(this.LiqfirstLevel);
    });
  }
  categoriesLiqChild(data) {
    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach((element) => {
        element.forEach((ele) => {
          this.LigsecondLevel.push(ele.liquorSubVarients);
        });
      });
      resolve(this.LigsecondLevel);
    }).then((sid) => {
      this.LiqthirdLevel = sid;

      console.log("third level", this.LiqthirdLevel);

      this.LiqthirdLevel.forEach((element) => {
        element.forEach((ele) => {
          this.LiqthirdLevel2.push(ele);
          this.LiqforthLevel.push(ele.liquorSubSubVarientType);
        });
      });
      this.LiqforthLevel.forEach((element1) => {
        element1.forEach((ele) => {
          this.LiqfiftLevel.push(ele);
        });
      });

      console.log("this is fourth level final of Liqour", this.LiqforthLevel);
      console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

      this.cancatArray();
    });
  }
  cancatArray() {
    let Array1 = this.InvfiftLevel.concat(this.RetfiftLevel);
    this.CancatAllArray = this.fiftLevel.concat(Array1);
    console.log("this is the concatination of array", this.CancatAllArray);
  }

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  BevragesCategory() {
    this.showBevCategories = true;
    this.showLiqCategories = false;
    this.showRetCategories = false;
    this.showIntCategories = false;
  }

  InventoryCategory() {
    this.showBevCategories = false;
    this.showLiqCategories = false;
    this.showRetCategories = false;
    this.showIntCategories = true;
  }

  LiqourCategory() {
    this.showBevCategories = false;
    this.showLiqCategories = true;
    this.showRetCategories = false;
    this.showIntCategories = false;
  }

  retailCategory() {
    this.showBevCategories = false;
    this.showLiqCategories = false;
    this.showRetCategories = true;
    this.showIntCategories = false;
  }

  selectedChild(event, root, sub, subsub, child) {
    console.log("event value", event);
    console.log(root + " " + sub + " " + subsub + " " + child);

    if (event) {
      // const add = this.addSupplier.controls.supplierDetails.get('dealCategory') as FormArray;
      // add.push(this.fb.group({
      //   rootcategoryid: root,
      //   subcategoryid: sub,
      //   subsubcategoryid: subsub,
      //   subsubcategorytypename: child
      // }))

      this.supplierData[0].supplierDetails.dealCategory.push({
        rootcategoryid: root,
        subcategoryid: sub,
        subsubcategoryid: subsub,
        subsubcategorytypename: child,
      });
    } else {
      // this.addSupplier.controls.supplierDetails.get('dealCategory').value.forEach((element, index) => {
      //   if (element.subsubcategorytypename === child) {
      //     const deal = this.addSupplier.controls.supplierDetails.get('dealCategory') as FormArray;
      //     deal.removeAt(index);
      //     console.log("Removed from deal category");
      //   }
      // })

      this.supplierData[0].supplierDetails.dealCategory.forEach(
        (element, index) => {
          if (element.subsubcategorytypename === child) {
            this.supplierData[0].supplierDetails.dealCategory.removeAt(index);
          }
        }
      );
    }
  }
  selectedProChild(child) {
    console.log(child);
  }

  // dealing product
  patchProduct() {
    this.material_service.getAllProductIventory(123).subscribe((data) => {
      this.productArr = data["data"];
    });
  }
  selectedProduct(product, event) {
    console.log(product);
    console.log(event);
    if(event === true){
      this.supplierData[0].supplierDetails.dealingProducts.push(product);
    }
    else {
      this.supplierData[0].supplierDetails.dealingProducts.forEach(
        (element, index) => {
          if (element === product) {
            this.supplierData[0].supplierDetails.dealingProducts.splice(index, 1);
          }
        }
      );
    }
    console.log(this.supplierData);
  }

  removeProduct(i){
    this.supplierData[0].supplierDetails.dealingProducts.splice(i, 1);
  }

  removeCategory(i){
    this.supplierData[0].supplierDetails.dealCategory.splice(i, 1);    
  }

  saveSupplier() {
    console.log(JSON.stringify(this.supplierData));
    this._snackBar.open("Updated successfully...!", "End now", {
      duration: 1000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

    this.material_service.putSupplier(this.supplierData[0]._id, this.supplierData[0])
      .subscribe((res) => {
        console.log(res);
      });
  }
}
