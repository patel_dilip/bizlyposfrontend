import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashMgtAddExpensesComponent } from './cash-mgt-add-expenses.component';

describe('CashMgtAddExpensesComponent', () => {
  let component: CashMgtAddExpensesComponent;
  let fixture: ComponentFixture<CashMgtAddExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashMgtAddExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashMgtAddExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
