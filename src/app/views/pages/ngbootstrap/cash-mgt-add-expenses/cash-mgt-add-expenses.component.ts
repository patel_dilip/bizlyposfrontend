import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup }from '@angular/forms'
import { Router }from '@angular/router'
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service'
//import { MaterialServiceService } from 'src/app/core/auth/_services/material-service.service';
@Component({
  selector: 'kt-cash-mgt-add-expenses',
  templateUrl: './cash-mgt-add-expenses.component.html',
  styleUrls: ['./cash-mgt-add-expenses.component.scss']
})
export class CashMgtAddExpensesComponent implements OnInit {
  addRestoTechForm:FormGroup;
  maintenanceForm:FormGroup;
  addVendorForm:FormGroup;
  addRentForm:FormGroup;
  buyFromStoreForm:FormGroup;
  addUtilityForm:FormGroup;

  isShowAmt:boolean=false;
  isShowAmtToPaid:boolean=true;
  isPayMethod:boolean=true;

  showCategories:boolean=false;

  BeverageCategory:any=[];
  constructor(private fb:FormBuilder,private router:Router,private material_service: MaterialServiceService,) { }

  ngOnInit() {
    this.addRestoTechForm=this.fb.group({
      amount:[''],
      purpose:['']
    });
    this.maintenanceForm=this.fb.group({
      equipments:[''],
      recurring:[''],
      maintanence_period:[''],
      amount_toBe_paid:[''],
      amount:[''],
      maintanence_StartDate:[''],
      payment_method:['']
    });
    this.addVendorForm=this.fb.group({
      select_vendor:[''],

    });
    this.addRentForm=this.fb.group({
      rent_date:[''],
      rent_amount:[''],
      paymentDate_everyMonth:[''],
      agreeStart_Date:[''],
      numberOf_Months:[''],
      payment_method:['']
    });
    this.buyFromStoreForm=this.fb.group({
      material_name:[''],
      unit:[''],
      quantity:[''],
      total_price:['']
    });
    this.addUtilityForm=this.fb.group({
      select_utility:[''],
      amount_tobe_paid:[''],
      pay_ofMonth:[''],
      recurring:[''],
      payment_method:['']
    });

  }
  //********save utility**** */
  Save_Utility(){
    let obj=this.addUtilityForm.value;
    console.log("Saved Utility",obj);
    
  }
  //*****Add resto tech*****/
  Save_RestoTech(){
    let obj=this.addRestoTechForm.value
    console.log("Saved Restaurant Tech",obj);
    
  }
  //*****Add Expences function***** */
Save_Expences(){
  let obj=this.addRestoTechForm.value
  console.log("Saved Expences",obj);
  
}
//****Add Maintanace function**** */
Save_Maintanance(){
  let obj=this.maintenanceForm.value;
  console.log("saved Maintenance",obj);
  
}
//****Add equipment function***** */
route_to_AddEquipments(){
  this.router.navigate(['ngbootstrap/add-equipments']);
  
}
//*******Add rent function*** */
Save_Rent(){
  let obj=this.addRentForm.value;
  console.log("Saved Rent",obj);
  
}
//******save buy from store items*** */
Save_BuyItem(){
  let obj=this.buyFromStoreForm.value
  console.log("Saved Buy from store",obj);
  
}
//*****show hide div of amount when recurring*** */
Amount_Paid(){
  this.isShowAmtToPaid=false;
  this.isShowAmt=true
  console.log("hii");
  this.isPayMethod=false
}
//******tree function***** */
getallCategories() {

    this.material_service.getAllCategories().subscribe(data => { 
      this.BeverageCategory = data['BeverageCategory']
console.log('all Bevrages categories', this.BeverageCategory);
    });
  }

  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
    //*****bev function***** */
    BevragesCategory(){
   
   
    this.showCategories=true
   
    
  
    }
}
