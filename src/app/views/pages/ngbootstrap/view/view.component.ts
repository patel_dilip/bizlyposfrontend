import { Component, OnInit } from '@angular/core';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  poFinalObj = [];
  public editable = this.material_service.editable;
  public totalTax = 0;
  public totalPriceAfterDiscount = 0;
  itempriceAfterDiscount = 0;
  totalDiscount = 0;
  discountPerItem = 0;
  taxPerItem = 0;
  tempTotalPayAmt = 0;
  contributionOnTotalPerItem = 0;
  row = [];
  tempData = 0;
  displayExtraChargeArr = [];
  obj = [];

  constructor(private material_service: MaterialServiceService, private router: Router) {
   // this.material_service.editable = false;
    console.log('Row : ', this.material_service.editPO);
    this.poFinalObj.push(this.material_service.editPO);
    console.log(this.poFinalObj);
   }

  ngOnInit() {
  this.tempData =  this.poFinalObj[0].totalpayble[0].totalpayble;
  console.log('tempdata', this.tempData);
  }
  openEdit() {
    console.log('click edit');
    this.editable = true;
    //this.router.navigate(['ngbootstrap/edit']);
  }

  orderQty(t, i, c, totalpayble) {

  let priceUnit = this.poFinalObj[0].product[i].priceUnit;
  let tax = this.poFinalObj[0].product[i].tax;
  const totalPriceWithoutTax = c * (priceUnit);
  const totalWithTax = ((priceUnit * c) / 100) * tax;
  this.poFinalObj[0].product[i].orderQty = c;
  let discountPrice = totalpayble[0].discount;

  this.poFinalObj[0].product[i].totalpurchasepricewithouttax =  totalPriceWithoutTax + '';
  this.poFinalObj[0].product[i].totalpurchaseprice =  totalPriceWithoutTax + totalWithTax;

  let totalPurchasePrise = 0;
  this.poFinalObj[0].product.forEach((element, index) => {
    totalPurchasePrise = totalPurchasePrise + (+element.totalpurchasepricewithouttax);
    (document.getElementById('totalpurchaseprise') as HTMLInputElement).value = totalPurchasePrise + '';
    (document.getElementById('totalpurchaseprise') as HTMLInputElement).innerHTML = totalPurchasePrise + '';
    this.poFinalObj[0].totalpayble[0].totalpurchaseprise = totalPurchasePrise;

    if (totalpayble[0].discounttype === 'percentage') {
     this.totalPriceAfterDiscount = 0;
     this.totalTax = 0;
     this.itempriceAfterDiscount = 0;

     this.totalDiscount = 0;
     this.contributionOnTotalPerItem = 0;

     let tax = +(element.tax);
     this.totalPriceAfterDiscount = totalPurchasePrise - (totalPurchasePrise * (discountPrice / 100));
     this.totalDiscount =   this.totalDiscount + (totalPurchasePrise * (discountPrice / 100));
     this.contributionOnTotalPerItem = (element.totalpurchasepricewithouttax / totalPurchasePrise) * (100);
     this.discountPerItem = (this.totalDiscount * (this.contributionOnTotalPerItem / 100) );
     this.itempriceAfterDiscount = (element.totalpurchasepricewithouttax)  - (this.discountPerItem);
     this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
     this.totalTax = +(this.totalTax + this.taxPerItem).toFixed(2);

     /* (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).value = this.itempriceAfterDiscount + '';
     (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).innerHTML = this.itempriceAfterDiscount + ''; */

     this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

     (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
     (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

     this.poFinalObj[0].totalpayble[0].discount = discountPrice;

     (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
     (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

     this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

     /* (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
     (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + ''; */
     this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;
     this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;
     console.log('tottax', this.totalTax);
     (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
     (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

     this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
     this.tempData = this.tempTotalPayAmt;
     console.log('after json', this.poFinalObj);

   } else if (totalpayble[0].discounttype === 'price') {
    this.totalPriceAfterDiscount = 0;
    this.totalTax = 0;
    this.itempriceAfterDiscount = 0;
    this.totalDiscount = 0;

    let tax = +(element.tax);
    this.discountPerItem = ((element.totalpurchasepricewithouttax) / totalPurchasePrise) * (discountPrice);
    this.itempriceAfterDiscount = (element.totalpurchasepricewithouttax)  - (this.discountPerItem);
    this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
    this.totalTax = this.totalTax + this.taxPerItem;
    this.totalPriceAfterDiscount = this.totalPriceAfterDiscount + this.itempriceAfterDiscount;
    this.totalDiscount = this.totalDiscount + this.discountPerItem;
    this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

    this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;

    (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
    (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

    this.poFinalObj[0].totalpayble[0].discount = discountPrice;

    (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
    (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

    this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

    this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

    (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
    (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

    this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
    this.tempData = this.tempTotalPayAmt;
   }
});
}

  priceUnit(t, i, c, totalpayble) {

  let orderQty = this.poFinalObj[0].product[i].orderQty;
  let tax = this.poFinalObj[0].product[i].tax;
  const totalPriceWithoutTax = c * (orderQty);
  const totalPriceWithTax = ((orderQty * c) / 100) * tax;
  this.poFinalObj[0].product[i].priceUnit = c;
  let discountPrice = totalpayble[0].discount;

  this.poFinalObj[0].product[i].totalpurchasepricewithouttax =  totalPriceWithoutTax + '';
  this.poFinalObj[0].product[i].totalpurchaseprice =  totalPriceWithoutTax + totalPriceWithTax;

  let totalPurchasePrise = 0;
  this.poFinalObj[0].product.forEach((element, index) => {
    totalPurchasePrise = totalPurchasePrise + (+element.totalpurchasepricewithouttax);
    (document.getElementById('totalpurchaseprise') as HTMLInputElement).value = totalPurchasePrise + '';
    (document.getElementById('totalpurchaseprise') as HTMLInputElement).innerHTML = totalPurchasePrise + '';
    this.poFinalObj[0].totalpayble[0].totalpurchaseprise = totalPurchasePrise;

    if (totalpayble[0].discounttype === 'percentage') {
       this.totalPriceAfterDiscount = 0;
       this.totalTax = 0;
       this.itempriceAfterDiscount = 0;

       this.totalDiscount = 0;
       this.contributionOnTotalPerItem = 0;

       let tax = +(element.tax);
       this.totalPriceAfterDiscount = totalPurchasePrise - (totalPurchasePrise * (discountPrice / 100));
       this.totalDiscount =   this.totalDiscount + (totalPurchasePrise * (discountPrice / 100));
       this.contributionOnTotalPerItem = (element.totalpurchasepricewithouttax / totalPurchasePrise) * (100);
       this.discountPerItem = (this.totalDiscount * (this.contributionOnTotalPerItem / 100) );
       this.itempriceAfterDiscount = (element.totalpurchasepricewithouttax)  - (this.discountPerItem);
       this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
       this.totalTax = +(this.totalTax + this.taxPerItem).toFixed(2);

       this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

       this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;

       (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
       (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

       this.poFinalObj[0].totalpayble[0].discount = discountPrice;

       (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
       (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

       this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

       (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
       (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + '';

       this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

       (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
       (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

       this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
       this.tempData = this.tempTotalPayAmt;
       console.log('after json', this.poFinalObj);

     } else if (totalpayble[0].discounttype === 'price') {
      this.totalPriceAfterDiscount = 0;
      this.totalTax = 0;
      this.itempriceAfterDiscount = 0;
      this.totalDiscount = 0;

      let tax = +(element.tax);
      this.discountPerItem = ((element.totalpurchasepricewithouttax) / totalPurchasePrise) * (discountPrice);
      this.itempriceAfterDiscount = (element.totalpurchasepricewithouttax)  - (this.discountPerItem);
      this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
      this.totalTax = this.totalTax + this.taxPerItem;
      this.totalPriceAfterDiscount = this.totalPriceAfterDiscount + this.itempriceAfterDiscount;
      this.totalDiscount = this.totalDiscount + this.discountPerItem;
      this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

      this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;

      (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
      (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

      this.poFinalObj[0].totalpayble[0].discount = discountPrice;

      (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
      (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

      this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

      this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

      (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
      (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

      this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
      this.tempData = this.tempTotalPayAmt;
     }
  });
  }
  discountType(type, totalpayble) {
   console.log('type', type);
   this.poFinalObj[0].totalpayble[0].discounttype = type;
  }

 taxCalculation(t, i, c, totalpayble) {
   console.log('t,supplier,i', t, i, c);
   let taxCurrent = (+c);
   this.poFinalObj[0].product[i].tax = taxCurrent;
   let totalPriceWithTax = t.totalpurchaseprice;
   let discountType = totalpayble.discounttype;
   let discountPrice = totalpayble.discount;
   let totalPurchasePrise = totalpayble.totalpurchaseprise;
   this.totalTax = 0;
   let totalPayableAmt = 0;
   let totalWithTaxes = 0;
   let taxtotal = 0;
   var e = document.getElementById('discounttype') as HTMLSelectElement;
   var v = (e.options[e.selectedIndex]).value;

   const quantity = parseInt(t.orderQty)
   const price = parseInt(t.priceUnit);
   const tax = parseInt(c);
   const totalpurchaseprice = parseInt(t.totalpurchaseprice);

   const totalWithTax = ((price * quantity) / 100) * tax;
   const TaxtotalPrice = (price * quantity) + (totalWithTax);

   (document.getElementById('totalpurchaseprice') as HTMLInputElement).value = TaxtotalPrice + '';
   (document.getElementById('totalpurchaseprice') as HTMLInputElement).innerHTML = TaxtotalPrice + '';

   taxtotal = taxtotal + tax;

   totalWithTaxes = totalWithTaxes + (totalpurchaseprice);

   (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = totalPurchasePrise;
   (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = totalPurchasePrise;

   (document.getElementById('totaltax') as HTMLInputElement).value = (taxtotal).toFixed(2) + '';
   (document.getElementById('totaltax') as HTMLInputElement).innerHTML = taxtotal + '';

   (document.getElementById('totalpayble') as HTMLInputElement).value = totalWithTaxes + '';
   (document.getElementById('totalpayble') as HTMLInputElement).innerHTML = totalWithTaxes + '';

   console.log('po', this.poFinalObj);
   if (totalpayble.discounttype === 'percentage') {
    this.poFinalObj[0].product.forEach((ele, index) => {
      this.totalPriceAfterDiscount = 0;
     // this.totalTax = 0;
      this.itempriceAfterDiscount = 0;
  
      this.totalDiscount = 0;
      this.contributionOnTotalPerItem = 0;
  
      let tax = +(ele.tax);
      this.totalPriceAfterDiscount = totalPurchasePrise - (totalPurchasePrise * (discountPrice / 100));
      this.totalDiscount =   this.totalDiscount + (totalPurchasePrise * (discountPrice / 100));
      this.contributionOnTotalPerItem = (ele.totalpurchasepricewithouttax / totalPurchasePrise) * (100);
      this.discountPerItem = (this.totalDiscount * (this.contributionOnTotalPerItem / 100) );
      this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
      this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
      this.totalTax = +(this.totalTax + this.taxPerItem).toFixed(2);
  
   /*    (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).value = this.itempriceAfterDiscount + '';
      (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).innerHTML = this.itempriceAfterDiscount + ''; */
  
      this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;
  
      this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;
  
  
      (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
      (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;
  
      this.poFinalObj[0].totalpayble[0].discount = discountPrice;
  
      (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
      (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';
  
      this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;
  
      (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
      (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + '';
  
      this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;
  
      (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
      (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';
  
      this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
      this.tempData =  this.tempTotalPayAmt;
      console.log('after json', this.poFinalObj);
  
    });

    } else if (totalpayble.discounttype === 'price') {
      // this.poFinalObj.forEach((ele,index1) => {
      if (this.poFinalObj[0].product.length > 0) {
        this.totalPriceAfterDiscount = 0;
        this.poFinalObj[0].product.forEach((ele, index) => {
        //  this.totalTax = 0;
          this.itempriceAfterDiscount = 0;
          this.totalDiscount = 0;

          let tax = +(ele.tax);
          this.discountPerItem = ((ele.totalpurchasepricewithouttax) / totalPurchasePrise) * (discountPrice);
          this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
          this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
          this.totalTax = this.totalTax + this.taxPerItem;
          this.totalPriceAfterDiscount = this.totalPriceAfterDiscount + this.itempriceAfterDiscount;
          this.totalDiscount = this.totalDiscount + this.discountPerItem;

      /*     (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).value = this.itempriceAfterDiscount + '';
          (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).innerHTML = this.itempriceAfterDiscount + ''; */

          this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

          this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;

          (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
          (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

          this.poFinalObj[0].totalpayble[0].discount = discountPrice;

          (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
          (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

          this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

          (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
          (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + '';

          this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

          (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
          (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

          this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
          this.tempData =  this.tempTotalPayAmt;
        });
      }
    }
  }

   calcDiscount(t, i, c, totalpayble) {
    console.log('calc clicked');
    var e = document.getElementById('discounttype') as HTMLSelectElement;
    var v = (e.options[e.selectedIndex]).value;
    let discountType = totalpayble.discounttype;
    let discountPrice = c;
    let totalPurchasePrise = totalpayble.totalpurchaseprise;

    if ((e.options[e.selectedIndex]).value === 'price') {
     // this.poFinalObj.forEach((ele,index1) => {
        if (this.poFinalObj[0].product.length > 0) {
          this.totalPriceAfterDiscount = 0;
          this.poFinalObj[0].product.forEach((ele, index) => {
            this.totalTax = 0;
            this.itempriceAfterDiscount = 0;
            this.totalDiscount = 0;

            let tax = +(ele.tax);
            this.discountPerItem = ((ele.totalpurchasepricewithouttax) / totalPurchasePrise) * (discountPrice);
            this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
            this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
            this.totalTax = this.totalTax + this.taxPerItem;
            this.totalPriceAfterDiscount = this.totalPriceAfterDiscount + this.itempriceAfterDiscount;
            this.totalDiscount = this.totalDiscount + this.discountPerItem;

        /*     (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).value = this.itempriceAfterDiscount + '';
            (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).innerHTML = this.itempriceAfterDiscount + ''; */

            this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

            this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;

            (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
            (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

            this.poFinalObj[0].totalpayble[0].discount = discountPrice;

            (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
            (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

            this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

            (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
            (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + '';

            this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

            (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
            (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

            this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
            this.tempData =  this.tempTotalPayAmt;
          });
        }

    } else if ((e.options[e.selectedIndex]).value === 'percentage') {
    console.log('before json', this.poFinalObj);
    this.poFinalObj[0].product.forEach((ele, index) => {
    this.totalPriceAfterDiscount = 0;
    this.totalTax = 0;
    this.itempriceAfterDiscount = 0;

    this.totalDiscount = 0;
    this.contributionOnTotalPerItem = 0;

    let tax = +(ele.tax);
    this.totalPriceAfterDiscount = totalPurchasePrise - (totalPurchasePrise * (discountPrice / 100));
    this.totalDiscount =   this.totalDiscount + (totalPurchasePrise * (discountPrice / 100));
    this.contributionOnTotalPerItem = (ele.totalpurchasepricewithouttax / totalPurchasePrise) * (100);
    this.discountPerItem = (this.totalDiscount * (this.contributionOnTotalPerItem / 100) );
    this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
    this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
    this.totalTax = +(this.totalTax + this.taxPerItem).toFixed(2);

 /*    (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).value = this.itempriceAfterDiscount + '';
    (document.getElementById('totalpurchasepriceondiscount') as HTMLInputElement).innerHTML = this.itempriceAfterDiscount + ''; */

    this.poFinalObj[0].product[index].totalpurchasepriceondiscount = this.itempriceAfterDiscount;

    this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;


    (document.getElementById('discount') as HTMLInputElement).value = discountPrice;
    (document.getElementById('discount') as HTMLInputElement).innerHTML = discountPrice;

    this.poFinalObj[0].totalpayble[0].discount = discountPrice;

    (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).value = this.totalPriceAfterDiscount + '';
    (document.getElementById('totalpriceafterdiscount') as HTMLInputElement).innerHTML = this.totalPriceAfterDiscount + '';

    this.poFinalObj[0].totalpayble[0].totalpriceafterdiscount = this.totalPriceAfterDiscount;

    (document.getElementById('totaltax') as HTMLInputElement).value = this.totalTax + '';
    (document.getElementById('totaltax') as HTMLInputElement).innerHTML = this.totalTax + '';

    this.poFinalObj[0].totalpayble[0].totaltax = this.totalTax;

    (document.getElementById('totalpayble') as HTMLInputElement).value = this.tempTotalPayAmt + '';
    (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  this.tempTotalPayAmt + '';

    this.poFinalObj[0].totalpayble[0].totalpayble =  this.tempTotalPayAmt;
    this.tempData =  this.tempTotalPayAmt;
    console.log('after json', this.poFinalObj);

  });
}
}
editChargesName(extracharges, i) {
  this.poFinalObj[0].extracharges.extracharges[i].chargesname = extracharges ;
  this.obj[i].chargesname = extracharges;
}

editChargesAmount(extracharges, i, totalpayble, product) {
  this.obj[i].amount = extracharges;
  this.poFinalObj[0].extracharges.extracharges[i].amount = (+extracharges);
  let discount = (+totalpayble[0].discount);
  let tax = (+product.tax);
  let extraChargeAmount = 0;
  let totalPaybleAmount = (+totalpayble[0].totalpayble);

  this.poFinalObj[0].extracharges.extracharges.forEach((ele) => {
          extraChargeAmount = (extraChargeAmount) +  (ele.amount);
      });


  if (+extraChargeAmount && discount !== 0 && tax !== 0) {
      totalPaybleAmount  = (+this.tempData) + (+extraChargeAmount);
      (document.getElementById('totalpayble') as HTMLInputElement).value = totalPaybleAmount + '';
      (document.getElementById('totalpayble') as HTMLInputElement).innerHTML = totalPaybleAmount + '';
      this.poFinalObj[0].totalpayble[0].totalpayble = totalPaybleAmount;
     // this.tempData = totalPaybleAmount;
    } else if (extraChargeAmount && (discount === 0 && tax === 0)) {
      totalPaybleAmount = totalPaybleAmount + extraChargeAmount;
      (document.getElementById('totalpayble') as HTMLInputElement).value = totalPaybleAmount + '';
      (document.getElementById('totalpayble') as HTMLInputElement).innerHTML = totalPaybleAmount + '';
      this.poFinalObj[0].totalpayble[0].totalpayble = totalPaybleAmount;
      //this.tempData = totalPaybleAmount;

    } else {
      totalPaybleAmount  =  (+this.tempData)  - (extraChargeAmount);
      (document.getElementById('totalpayble') as HTMLInputElement).value = totalPaybleAmount + '';
      (document.getElementById('totalpayble') as HTMLInputElement).innerHTML = totalPaybleAmount + '';
      this.poFinalObj[0].totalpayble[0].totalpayble = totalPaybleAmount;
      //this.tempData = totalPaybleAmount;

      // extraChargeAmount = 0;
    }

}

deleteRow(x, extracharges){
  var delBtn = confirm(' Do you want to delete added Extra Charge?');
  let extraChargeAmount = 0;
 // let totalpayble = (+this.poFinalObj[0].totalpayble[0].totalpayble);
  let totalpayble = (+this.tempData + extracharges.extracharges[x].amount);

  if ( delBtn == true ) {
    /* this.poFinalObj[0].extracharges.extracharges.forEach((ele) => {
      extraChargeAmount =  (ele.amount);
  }); */


   totalpayble = this.poFinalObj[0].totalpayble[0].totalpayble - extracharges.extracharges[x].amount;
   this.poFinalObj[0].totalpayble[0].totalpayble = totalpayble;
   (document.getElementById('totalpayble') as HTMLInputElement).value = totalpayble + '' ;
   (document.getElementById('totalpayble') as HTMLInputElement).innerHTML =  totalpayble + '';
   this.tempData = totalpayble;
   this.poFinalObj[0].extracharges.extracharges.splice(x, 1 );
   this.obj.splice(x, 1);
  }
}

addTable() {
  const obj = {
    chargesname: '',
    amount: '',
  }
  this.poFinalObj[0].extracharges.extracharges.push(obj);
  this.obj.push(obj);
}

displayExtraCharges(charges) {
  this.displayExtraChargeArr.push( charges.value );
}

addNewFreeItem() {
  const obj = {
    price: '',
    productcode: '',
    productname: '',
    materialname: '',
    unit: '',
    quantity: ''
  }
  this.poFinalObj[0].freeitem.freeitems.push(obj);
}

deleteFreeItem(x){
  var delBtn = confirm(' Do you want to delete added free item?');
  if ( delBtn == true ) {
    this.poFinalObj[0].freeitem.freeitems.splice(x, 1 );
  }
}

addNewWhatsAppNo() {
  const obj = {
    whatsapps: ''
  }
  this.poFinalObj[0].notification.whatsapps.push(obj);
}

deleteWhatsAppNo(x){
  var delBtn = confirm(' Do you want to delete added whatsapp No??');
  if ( delBtn == true ) {
    this.poFinalObj[0].notification.whatsapps.splice(x, 1 );
  }
}

addNewEmail() {
  const obj = {
   emails: ''
  }
  this.poFinalObj[0].notification.emails.push(obj);
}

deleteEmail(x){
  var delBtn = confirm(' Do you want to delete added Email?');
  if ( delBtn == true ) {
    this.poFinalObj[0].notification.emails.splice(x, 1 );
  }
}

addNewSms() {
  const obj = {
   sms: ''
  }
  this.poFinalObj[0].notification.sms.push(obj);
}

deleteSms(x){
  var delBtn = confirm(' Do you want to delete added Sms No?');
  if ( delBtn == true ) {
    this.poFinalObj[0].notification.sms.splice(x, 1 );
  }
}


freeItemQuantity(quantity, i) {
  /* (document.getElementById('quantity') as HTMLInputElement).value = quantity ;
  (document.getElementById('quantity') as HTMLInputElement).innerHTML =  quantity; */
  this.poFinalObj[0].freeitem.freeitems[i].quantity = quantity ;

}

freeItemPrice(price, i) {
 /*  (document.getElementById('price') as HTMLInputElement).value = price ;
  (document.getElementById('price') as HTMLInputElement).innerHTML =  price; */
  this.poFinalObj[0].freeitem.freeitems[i].price = price ;
}

whatsappFunc(whatsapp, i) {
  this.poFinalObj[0].notification.whatsapps[i].whatsapps = whatsapp ;
}

emailFunc(emails, i) {
  this.poFinalObj[0].notification.emails[i].emails = emails ;
}
 smsFunc(sms, i) {
  this.poFinalObj[0].notification.sms[i].sms = sms ;
 }

saveEditedPO() {
  console.log('edited PO sucessfully1', this.poFinalObj[0]);

  this.material_service.updatePO(this.poFinalObj[0]).subscribe(res => {
    //console.log('edited PO sucessfully', res);
  });
}

}
