import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgbootstrapComponent } from './ngbootstrap.component';
import { AlertComponent } from './alert/alert.component';
import { AccordionComponent } from './accordion/accordion.component';
import { NgbAlertConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PartialsModule } from '../../partials/partials.module';
import { CoreModule } from '../../../core/core.module';
import { MaterialPreviewModule } from '../../partials/content/general/material-preview/material-preview.module';
import {MatRadioModule} from '@angular/material/radio';
import { ButtonsComponent } from './buttons/buttons.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarouselComponent } from './carousel/carousel.component';
import { HttpClientModule } from '@angular/common/http';
import { CollapseComponent } from './collapse/collapse.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ModalComponent, NgbdModalContentComponent } from './modal/modal.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PopoverComponent } from './popover/popover.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { RatingComponent } from './rating/rating.component';
import { TabsComponent } from './tabs/tabs.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TypeheadComponent } from './typehead/typehead.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { GeneratePoComponent } from './generate-po/generate-po.component';
import { LiveInventoryComponent } from './live-inventory/live-inventory.component';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AddmaterialComponent } from './addmaterial/addmaterial.component';
import { StockviewComponent } from './stockview/stockview.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { RowitemComponent } from './rowitem/rowitem.component';
import { AddretailproductComponent } from './addretailproduct/addretailproduct.component';
import { SuitableSupplierComponent } from './suitable-supplier/suitable-supplier.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ClosingStockAuditComponent } from './closing-stock-audit/closing-stock-audit.component';
import { UpdateStocksComponent } from './update-stocks/update-stocks.component';
import { GoodReceivedComponent } from './good-received/good-received.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AddToStockComponent } from './add-to-stock/add-to-stock.component';
import { AddToStockMultipleItemComponent } from './add-to-stock-multiple-item/add-to-stock-multiple-item.component';
import { ReplaceGoodsSingleItemComponent } from './replace-goods-single-item/replace-goods-single-item.component';
import { CancelOrderSingleItemComponent } from './cancel-order-single-item/cancel-order-single-item.component';
import { CancelOrderMultipleItemComponent } from './cancel-order-multiple-item/cancel-order-multiple-item.component';
import { PurchaseOrderRaisePoComponent } from './purchase-order-raise-po/purchase-order-raise-po.component';
import { PurchaseOrderGenerateComponent } from './purchase-order-generate/purchase-order-generate.component';
import { VarianceComponent } from './variance/variance.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { AddInventoryComponent } from './add-inventory/add-inventory.component';
import { AddSupplierComponent } from './add-supplier/add-supplier.component';
import { BulkOrderComponent } from './bulk-order/bulk-order.component';
import { AddBulkOrderComponent } from './add-bulk-order/add-bulk-order.component';
import { CashManagementComponent } from './cash-management/cash-management.component';
import { CashMgtAddExpensesComponent } from './cash-mgt-add-expenses/cash-mgt-add-expenses.component';
import { CashMgtAddCashComponent } from './cash-mgt-add-cash/cash-mgt-add-cash.component';
import { CashMgtRemoveCashComponent } from './cash-mgt-remove-cash/cash-mgt-remove-cash.component';
import { VendorPaymentDetailsComponent } from './vendor-payment-details/vendor-payment-details.component';
//******material module*****/
import { MaterialModule } from '../../pages/material/material.module';
import {MatTreeModule} from '@angular/material';
import { TreeModule } from "angular-tree-component";
import { MatIconModule } from "@angular/material/icon";
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatChipsModule} from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material';
import { NgxSpinnerModule } from 'ngx-spinner';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
//*******components****/
import { AddEquipmentsComponent } from './add-equipments/add-equipments.component';
import { AddPOMaterialComponent } from './add-pomaterial/add-pomaterial.component';
import { ViewComponent } from './view/view.component';
import { SuppliersComponent } from './add-suppliers/suppliers.component';
import { SuppliersDashboardComponent } from './suppliers-dashboard/suppliers-dashboard.component';
import { ViewEditSuppliersComponent } from './view-edit-suppliers/view-edit-suppliers.component';
const routes: Routes = [
	{
		path: '',
		component: NgbootstrapComponent,
		children: [
			{
				path: 'accordion',
				component: AccordionComponent
			},
			{
				path: 'alert',
				component: AlertComponent
			},
			{
				path: 'buttons',
				component: ButtonsComponent
			},
			{
				path: 'carousel',
				component: CarouselComponent
			},
			{
				path: 'collapse',
				component: CollapseComponent
			},
			{
				path: 'datepicker',
				component: DatepickerComponent
			},
			{
				path: 'dropdown',
				component: DropdownComponent
			},
			{
				path: 'modal',
				component: ModalComponent
			},
			{
				path: 'pagination',
				component: PaginationComponent
			},
			{
				path: 'popover',
				component: PopoverComponent
			},
			{
				path: 'progressbar',
				component: ProgressbarComponent
			},
			{
				path: 'rating',
				component: RatingComponent
			},
			{
				path: 'tabs',
				component: TabsComponent
			},
			{
				path: 'timepicker',
				component: TimepickerComponent
			},
			{
				path: 'tooltip',
				component: TooltipComponent
			},
			{
				path: 'typehead',
				component: TypeheadComponent
			},
			{
				path: 'purchase-order',
				component: PurchaseOrderComponent,
			},
			{
				path: 'generate-po',
				component: GeneratePoComponent
			},
			{
				path: 'live-inventory',
				component: LiveInventoryComponent
			},
			{
				path: 'addmaterial',
				component: AddmaterialComponent
			},
			{
				path: 'stockview',
				component: StockviewComponent
			},
			{
				path: 'rowitem',
				component: RowitemComponent
			},
			{
				path: 'addretailproduct',
				component: AddretailproductComponent
			},
			{
				path: 'suitable-supplier',
				component: SuitableSupplierComponent
			},
			{
				path: 'activities',
				component: ActivitiesComponent
			},
			{
				path: 'closing-stock-audit',
				component: ClosingStockAuditComponent
			},
			{
				path: 'update-stocks',
				component: UpdateStocksComponent
			},
			{
				path: 'good-received',
				component: GoodReceivedComponent
			},
			{
				path: 'add-to-stock',
				component: AddToStockComponent
			},
			{
				path: 'add-to-stock-multiple-item',
				component: AddToStockMultipleItemComponent
			},
			{
				path: 'replace-goods-single-item',
				component: ReplaceGoodsSingleItemComponent
			},
			{
				path: 'cancel-order-single-item',
				component: CancelOrderSingleItemComponent
			},
			{
				path: 'cancel-order-multiple-item',
				component: CancelOrderMultipleItemComponent
			},
			{
				path: 'variance',
				component: VarianceComponent
			},
			{
				path: 'purchase-order-raise-po',
				component: PurchaseOrderRaisePoComponent
			},
			{
				path: 'purchase-order-generate',
				component: PurchaseOrderGenerateComponent
			},
			{
				path: 'add-inventory',
				component: AddInventoryComponent
			},
			{
				path: 'add-supplier',
				component: AddSupplierComponent
			},
			{
				path: 'bulk-order',
				component: BulkOrderComponent
			},
			{
				path: 'add-bulk-order',
				component: AddBulkOrderComponent
			},
			{
				path: 'cash-management',
				component: CashManagementComponent
			},
			{
				path: 'cash-mgt-add-expenses',
				component: CashMgtAddExpensesComponent
			},
			{
				path: 'cash-mgt-add-cash',
				component: CashMgtAddCashComponent
			},
			{
				path: 'cash-mgt-remove-cash',
				component: CashMgtRemoveCashComponent
			},
			{
				path: 'vendor-payment-details',
				component: VendorPaymentDetailsComponent
			},
			{
				path: 'add-equipments',
				component: AddEquipmentsComponent
			},
			{
				path: 'add-pomaterial',
				component: AddPOMaterialComponent
			},
			{
				path: 'view',
				component: ViewComponent
			},
			{
				path: 'suppliers-dashboard',
				component: SuppliersDashboardComponent
			},
			{
				path: 'add-suppliers',
				component: SuppliersComponent
			},
			{
				path: 'view-edit-suppliers',
				component: ViewEditSuppliersComponent
			}
		]
	}
];

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		NgbModule,
		CoreModule,
		MaterialPreviewModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		PerfectScrollbarModule,
		MatTableModule,
		MatFormFieldModule,
		MatInputModule,
		MatTabsModule,
		MatCheckboxModule,
		MatButtonToggleModule,
		MatDatepickerModule,
		MatRadioModule,
		MaterialModule,
		MatTreeModule,
		TreeModule,
		MatIconModule,
		MatSelectModule,
		MatExpansionModule,
		MatChipsModule,
		MatPaginatorModule,
		NgxSpinnerModule,
		MatSlideToggleModule,
	],
	exports: [RouterModule],
	declarations: [
		NgbootstrapComponent,
		AlertComponent,
		AccordionComponent,
		ButtonsComponent,
		CarouselComponent,
		CollapseComponent,
		DatepickerComponent,
		DropdownComponent,
		ModalComponent,
		NgbdModalContentComponent,
		PaginationComponent,
		PopoverComponent,
		ProgressbarComponent,
		RatingComponent,
		TabsComponent,
		TimepickerComponent,
		TooltipComponent,
		TypeheadComponent,
		PurchaseOrderComponent,
		GeneratePoComponent,
		GeneratePoComponent,
		LiveInventoryComponent,
		AddmaterialComponent,
		StockviewComponent,
		RowitemComponent,
		AddretailproductComponent,
		SuitableSupplierComponent,
		ActivitiesComponent,
		ClosingStockAuditComponent,
		UpdateStocksComponent,
		GoodReceivedComponent,
		AddToStockComponent,
		AddToStockMultipleItemComponent,
		ReplaceGoodsSingleItemComponent,
		CancelOrderSingleItemComponent,
		CancelOrderMultipleItemComponent,
		PurchaseOrderRaisePoComponent,
		PurchaseOrderGenerateComponent,
		VarianceComponent,
		AddInventoryComponent,
		AddSupplierComponent,
		BulkOrderComponent,
		AddBulkOrderComponent,
		CashManagementComponent,
		CashMgtAddExpensesComponent,
		CashMgtAddCashComponent,
		CashMgtRemoveCashComponent,
		VendorPaymentDetailsComponent,
		AddEquipmentsComponent,
		AddPOMaterialComponent,
		ViewComponent,
		SuppliersComponent,
		SuppliersDashboardComponent,
		ViewEditSuppliersComponent
	],
	providers: [
		NgbAlertConfig,],
	entryComponents: [
		NgbdModalContentComponent
	]
})
export class NgbootstrapModule {
}
