import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator, MatSort, _MatMenuDirectivesModule } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
//*****draft table interface*****/
export interface DraftElement {
  Date: number;
  Suppliers: string;
  ContactNumber: number;
  ItemQty: number;
  TotalBillAmount: number;
  Status: string;
  Modified: string;
  Action: string;
}

//*****End drft Table******/

//*****submit table interface*****/
export interface submittedElement {
  PoNumber: number;
  supplierName: string;
  contactNo: number;
  orderQty: number;
  totalPayble: number;
  estimatedDeliveryTime: number;
  Action: string;
  Status: string;
  Activity: string;
}

//*******End Submitted table*******/

//*******Approved Table*****/
export interface approvedElement {
  PoNumber: number;
  Suppliers: string;
  ContactNumber: number;
  CreatedByOn: number;
  ExpectedDeliveryRange: number;
  TotalBillAmount: number;
  GoodsRecivedDate: number;
  Action: string;
  Status: string;
  Activity: string;
}

//*******End Approved table*********/

//*******Recieved Table*****/
export interface ricievedElement {
  PoNumber: number;
  Suppliers: string;
  ContactNumber: number;
  CreatedByOn: number;
  ActualDeliveryRange: number;
  TotalBillAmount: number;
  GoodsRecivedDate: number;
  Action: string;
  Status: string;
  Activity: string;
}

//*******End Approved table*********/

@Component({
  selector: 'kt-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {
  //  id:_id;

  displayedColumns: string[] = ['estimateddeliverytime', 'supplierName', 'contactNo', 'materialName','orderQty', 'totalpurchaseprice', 'Status', 'Modified', 'Action'];
  Draft_dataSource: MatTableDataSource<DraftElement>

  @ViewChild("paginator", { static: true }) draft_paginator: MatPaginator;
  @ViewChild("sort", { static: true }) draft_sort: MatSort;
  //********** Start Submitted********** */
  submitDisplayedColumns: string[] = ['purchaseorderId', 'supplierName', 'contactNo', 'materialName', 'orderQty', 'totalpayble', 'estimateddeliverytime', 'Action', 'Activity'];
  Submit_dataSource: MatTableDataSource<submittedElement>

  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;
  //*********End submitted********* */
  approvedDisplayedColumns: string[] = ['purchaseorderId', 'supplierName', 'contactNo', 'estimateddeliverytime', 'totalpurchaseprice', 'Status', 'Action', 'Activity'];
  Approved_dataSource: MatTableDataSource<approvedElement>

  @ViewChild("paginator", { static: true }) app_paginator: MatPaginator;
  @ViewChild("sort", { static: true }) app_sort: MatSort;

  recievedDisplayedColumns: string[] = ['purchaseorderId', 'supplierName', 'contactNo', 'totalpurchaseprice', 'Status', 'Action', 'Activity'];
  Recieved_dataSource: MatTableDataSource<ricievedElement>

  @ViewChild("paginator", { static: true }) rcv_paginator: MatPaginator;
  @ViewChild("sort", { static: true }) rcv_sort: MatSort;

  submittedPOData: any = [];
  draftPOData: any = [];
  title = 'appBootstrap';

  closeResult: string;
  allCategories: any = [];

  selectedMoreInfos = [];
  dupChkMoreInfo = [];

  BeverageCategory: any = [];
  LiquorCategory: any = [];
  retailFoodCategory: any = [];
  InventoryFoodCategory: any = [];
  categoryData: any = [];
  AllSelected: any = []
  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];
  demo: any = []
  subCategoryData: any = [];

  categoryRetData: any = [];
  categoryLiqData: any = []
  InvfirstLevel: any = [];
  InvsecondLevel: any = [];
  InvthirdLevel: any = [];
  InvthirdLevel2: any = [];
  InvforthLevel: any = [];
  InvfiftLevel: any = [];
  RetfirstLevel: any = [];
  RetsecondLevel: any = []
  RetthirdLevel: any = [];
  RetthirdLevel2: any = [];
  RetforthLevel: any = [];
  RetfiftLevel: any = [];
  LiqfirstLevel: any = []
  LigsecondLevel: any = [];
  LiqthirdLevel: any = [];
  LiqthirdLevel2: any = []
  LiqforthLevel: any = [];
  LiqfiftLevel: any = [];
  CancatAllArray: any = [];
  categoryInvData: any = [];
  CategoryAllData: any = [];
  showNoMaterialSelected = true;
  showNoProductSelected = true;
  hideModal = false;
  isDraftData: any;
  isSubmittedReviewData: any;
  approvedData = [];
  receivedData = [];
  submittedReviewData = [];
  draftData = [];
  beveragesCategories = [];
  inventoryFoodCategories = [];
  retailFoodCategories = [];
  liquorCategories = [];
  materialSelected;
  filteredSubmittedRecords: any;
  productNames = [];
  productBrands = [];
  productArr = [];
  selectedProd: boolean;
  selectedProductArr: any = [];

  @ViewChild('closeModal', { static: true }) closeModal: ElementRef;
  constructor(private modalService: NgbModal, private fb: FormBuilder, private material_service: MaterialServiceService, private router: Router
    ,         private ngxSpinner: NgxSpinnerService, private cdr: ChangeDetectorRef) {
    //*******get all genrated Purchase Order***** */
    this.getAllPos();
    this.getCategoriesForInventoryFood();
    this.getCategoriesForRetailFood();
    this.getCategoriesForLiquor();
    this.getCategoriesForBeverages();
  }

  ngOnInit() {
    console.log('inside init');
    this.showNoMaterialSelected = false;
    this.getAllPos();
    this.Submit_dataSource = new MatTableDataSource(this.submittedPOData);
    this.Submit_dataSource.paginator = this.paginator;
  }
  
  applyFilter(event: Event) {
   // let filterValue = (event.target as HTMLInputElement).value;
   // this.Draft_dataSource.filter = filterValue.trim().toLowerCase();
    let filterValue = (event.target as HTMLInputElement).value;
    console.log("Filter : ", filterValue);
    filterValue = filterValue.toLowerCase();
    this.filteredSubmittedRecords = this.submittedPOData.filter(record => {
      if(
      (record.purchaseOrder.supplierId.supplierName).toLowerCase().includes(filterValue) ||
      (record.purchaseOrder.podate).toLowerCase().includes(filterValue) ||
      (record.purchaseOrder.supplierId.contactNo).toString().includes(filterValue) 
      ){
      return record;
      }

      console.log(record);
      })

    console.log(this.filteredSubmittedRecords);
    this.Submit_dataSource = new MatTableDataSource(this.filteredSubmittedRecords);

    if(filterValue == ""){
      this.Submit_dataSource.paginator = this.paginator;
      }
  }
  
  get totalRows(): number {
    let total = 0;
    total = this.submittedReviewData.length;
    return total;
  }

  get totalRowsForDraft(): number {
    let total = 0;
    total = this.draftData.length;
    return total;
  }

  get totalRowsForApproved(): number {
    let total = 0;
    total = this.approvedData.length;
    return total;
  }

  get totalRowsForReceived(): number {
    let total = 0;
    total = this.receivedData.length;
    return total;
  }

  getAllPos() {
    let countForIsSubmitted = 0;
    let countForIsDraft = 0;
   
    this.material_service.getAllPurchaseOrders(123).subscribe(data => {

      this.submittedPOData = data['data']

      let submitReviwTab = this.submittedPOData.filter(
        ele => ele.isSubmitForReview === true);
      console.log('sub is',submitReviwTab);
      this.submittedReviewData = submitReviwTab;

      let draftData = this.submittedPOData.filter(
          ele => ele.isDraft === true);
      this.draftData = draftData;

      console.log('draft is', draftData);
        
      let approvedData = this.submittedPOData.filter(
            ele => ele.isApproved === true);
      this.approvedData = approvedData;
  
      console.log('approvedData is', approvedData);
  
      let receivedData = this.submittedPOData.filter(
              ele => ele.isReceived === true);
      this.receivedData = receivedData;
    
      console.log('receivedData is', receivedData);
    
      this.Submit_dataSource = new MatTableDataSource(submitReviwTab);
      this.Submit_dataSource.paginator = this.paginator;
      this.Submit_dataSource.sort = this.sort;

      // this.Draft_dataSource = new MatTableDataSource(draftData);
      // this.Draft_dataSource.paginator = this.draft_paginator;
      // this.Draft_dataSource.sort = this.draft_sort;
        
      // this.Approved_dataSource = new MatTableDataSource(approvedData);
      // this.Approved_dataSource.paginator = this.paginator;
      // this.Approved_dataSource.sort = this.sort;
        
      // this.Recieved_dataSource = new MatTableDataSource(receivedData);
      // this.Recieved_dataSource.paginator = this.paginator;
      // this.Recieved_dataSource.sort = this.sort;

    })
  }

  deleteItem(poId, materialId) {
    let result = confirm('Are you sure you want to delete?');
    if (!result) {
      return;
    } else {
      this.material_service.deleteMaterial(poId, materialId).subscribe(ele => {
           if (ele ['success']) {
             alert('Material Deleted');
             this.getAllPos();
           } else {
             alert('Please try Again');
           }
      });
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', backdrop: "static", keyboard: false }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  loading() {
    this.ngxSpinner.show()
  }
  //******** get all categories*******/
  getallCategories() {
    // dupChkMoreInfo contains the value of selected Items
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    return new Promise((resolve, reject) => {

      this.material_service.getAllCategories().subscribe(data => {
        //*****All Categories*****/
        this.allCategories = data
        //this.ngxSpinner.hide()
        console.log("This is All Categories", this.allCategories);



        //*****All Bevrages*******/
        this.BeverageCategory = data['BeverageCategory']
        console.log('all Bevrages categories', this.BeverageCategory);





        resolve(this.BeverageCategory)
      })
    }).then(id => {

      console.log("this is id", id);
      this.categoryData = id
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories)

      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel)
    })
  }
  
  getCategoriesForBeverages() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['BeverageCategory'].length) {
         res['BeverageCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.beveragesCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }

  getCategoriesForInventoryFood() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['InventoryFoodCategory'].length) {
         res['InventoryFoodCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.inventoryFoodCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }

  getCategoriesForLiquor() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['LiquorCategory'].length) {
         res['LiquorCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.liquorCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }
  getCategoriesForRetailFood() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['retailFoodCategory'].length) {
         res['retailFoodCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.retailFoodCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }
  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.secondLevel)
    }).then(sid => {
      this.thirdLevel = sid

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach(element => {

        element.forEach(ele => {
          this.thirdLevel2.push(ele)
          this.forthLevel.push(ele.subSubCategoryType);

        });
      });
      this.forthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.fiftLevel.push(ele)

        });
        // this.ngxSpinner.hide()
      })


      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);


    })
  }

  categoriesInvChild(data) {
    //console.log("firstlevel",firstLevel);


    //console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.InvsecondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.InvsecondLevel)
    }).then(sid => {
      this.InvthirdLevel = sid

      // console.log("third level", this.InvthirdLevel);

      this.InvthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.InvthirdLevel2.push(ele)
          this.InvforthLevel.push(ele.subSubCategoryType);

        });
      });
      this.InvforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.InvfiftLevel.push(ele)

        });

      })


      // console.log("this is fourth level final of food inventory", this.InvforthLevel);
      console.log("this is fifth level final of food inventory", this.InvfiftLevel);


    })
  }
  //**************started functionality of Retail Food***** */
 
  categoriesRetChild(data) {
    //console.log("firstlevel",firstLevel);


    // console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.RetsecondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.RetsecondLevel)
    }).then(sid => {
      this.RetthirdLevel = sid

      // console.log("third level", this.RetthirdLevel);

      this.RetthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.RetthirdLevel2.push(ele)
          this.RetforthLevel.push(ele.subSubCategoryType);

        });
      });
      this.RetforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.RetfiftLevel.push(ele)

        });

      })


      //console.log("this is fourth level final of Retail", this.RetforthLevel);
      console.log("this is fifth level final of Retail", this.RetfiftLevel);

      //this.cancatArray()
    });

  }

  categoriesLiqChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.LigsecondLevel.push(ele.liquorSubVarients)

        });

      });
      resolve(this.LigsecondLevel)
    }).then(sid => {
      this.LiqthirdLevel = sid

      //console.log("third level", this.LiqthirdLevel);

      this.LiqthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.LiqthirdLevel2.push(ele)
          this.LiqforthLevel.push(ele.liquorSubSubVarientType);

        });
      });
      this.LiqforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.LiqfiftLevel.push(ele)

        });

      })


      // console.log("this is fourth level final of Liqour", this.LiqforthLevel);
      console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

      //this.cancatArray()
    });

  }

  // ON MOREINFO chips
  selectBevInfo(groupName, s) {
    
    if (!this.dupChkMoreInfo.includes(s)) {
      let obj = {
        group: groupName,
        item: s

      }
      this.showNoMaterialSelected = false;
      this.dupChkMoreInfo.push(obj)
      // this.BeverageCategory.push({ title: a, moreinfo: b })
      console.log('selected moreinfos', this.dupChkMoreInfo);
      this.material_service.selectedMaterialsAddMaterialPopUp = this.dupChkMoreInfo;
      console.log('pop',this.material_service.selectedMaterialsAddMaterialPopUp );
    }
    else {
      alert('Already Added!!');
    } 
  }
  // On Remove MoreInfo 
  removeMoreInfo(s) {
    //this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.rootCategoryName), 1)
    this.dupChkMoreInfo = this.dupChkMoreInfo.filter(item => s.item !== item.item);
    if (this.dupChkMoreInfo.length === 0) {
      this.showNoMaterialSelected = true;
    }
    this.material_service.selectedMaterialsAddMaterialPopUp = this.dupChkMoreInfo;
  }

   // ON MOREINFO chips
   selectProductInfo(s) {
     console.log('s', s);
     if (!this.productNames.includes(s)) {
     
      this.showNoProductSelected = false;
      this.productNames.push(s);
      console.log('prodName', this.productNames);
     
    }
    else {
      alert('Already Added!!');
    } 
  }

   // On Remove MoreInfo 
   removeSelectedProductInfo(s) {
    //this.selectedProd = true;
    this.productNames = this.productNames.filter(item => s!==item);
    if (this.productNames.length === 0) {
      this.showNoProductSelected = true;
    }
  }

   // ON MOREINFO chips
   selectProductBrandInfo(s) {
    console.log('s', s);
    if (!this.productBrands.includes(s)) {
    
     this.showNoProductSelected = false;
     this.productBrands.push(s);
     console.log('prodName', this.productBrands);
    
   }
   else {
     alert('Already Added!!');
   } 
 }

  // On Remove MoreInfo 
  removeSelectedProductBrandInfo(s) {
    this.selectedProd = false;
    this.productBrands = this.productBrands.filter(item => s!==item);
    if (this.productBrands.length === 0) {
      this.showNoProductSelected = true;
    }
  }

  //Save and Send selected data
  SaveAllCategories() {
    this.productArr.forEach(product => {
      this.productNames.forEach(selectedproduct => {
        if (selectedproduct === product.productName) {
          this.selectedProductArr.push(product);
        }
      });
    });
    this.material_service.SelectedItems = this.dupChkMoreInfo;
    this.material_service.selectedProducts = this.selectedProductArr;

    this.modalService.dismissAll();
    if (window.location.pathname === '/ngbootstrap/purchase-order') {
      this.router.navigateByUrl('/ngbootstrap/purchase-order-raise-po');
    } else if (window.location.pathname === '/ngbootstrap/purchase-order-raise-po') {
      this.modalService.dismissAll();
    }
  }

  clickMe() {
    this.modalService.dismissAll();
  }

  navigateToViewEdit() {
    this.router.navigate[('/ngbootstrap/purchase-order/view')]
  }
  
  onClickOpenViewEdit(row){
    console.log("Record : ", row);
    this.material_service.editPO = row;
    this.material_service.editable = false;
    this.router.navigate(['/ngbootstrap/view']);
  }

  patchProduct() {
    this.material_service.getAllProductIventory(123).subscribe(data => {
  
      this.productArr = data['data'];

    });
  }
}

