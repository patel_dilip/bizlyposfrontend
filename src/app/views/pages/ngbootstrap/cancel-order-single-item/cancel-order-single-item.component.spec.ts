import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelOrderSingleItemComponent } from './cancel-order-single-item.component';

describe('CancelOrderSingleItemComponent', () => {
  let component: CancelOrderSingleItemComponent;
  let fixture: ComponentFixture<CancelOrderSingleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelOrderSingleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelOrderSingleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
