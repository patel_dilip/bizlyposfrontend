import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';

const tabset = {
		// beforeCodeTitle: 'Tabset',
		viewCode: ``,
		isCodeVisible: false,
		isExampleExpanded: true
	};






@Component({
	selector: 'kt-tabs',
	templateUrl: './tabs.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [NgbTabsetConfig] // add NgbTabsetConfig to the component providers
})
export class TabsComponent implements OnInit {
	exampleTabset;
	examplePills;
	exampleSelectAnActiveTabById;
	examplePreventTabChange;
	exampleNavJustification;
	exampleNavOrientation;
	exampleglobalConfigurationOfTabs;

	currentJustify = 'start';
	currentOrientation = 'horizontal';
	globalJustify = 'center';

	constructor(config: NgbTabsetConfig) {
		// customize default values of tabsets used by this component tree
		// config.justify = 'center';
		// config.type = 'pills';
	}

	ngOnInit() {
		this.exampleTabset = tabset;
	}

	public beforeChange($event: NgbTabChangeEvent) {
		if ($event.nextId === 'tab-preventchange2') {
			$event.preventDefault();
		}
	}
	// onRaisePo(){
	// 	this.router.navigate(['ngbootstrap/purchase-order'])
	// }
}
