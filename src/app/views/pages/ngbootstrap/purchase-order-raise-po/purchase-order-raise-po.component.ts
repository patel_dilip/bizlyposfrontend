import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { Router } from '@angular/router';
import { parse } from 'path';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { element } from 'protractor';
import moment, {Moment} from 'moment';
@Component({
  selector: 'kt-purchase-order-raise-po',
  templateUrl: './purchase-order-raise-po.component.html',
  styleUrls: ['./purchase-order-raise-po.component.scss']
})
export class PurchaseOrderRaisePoComponent implements OnInit {
  addMaterialToCartForm: FormGroup;
  ProductForm:FormGroup;
  SelectedItems: any = [];
  closeResult: string;
  AllSuppliers:any=[];
  currentStockData: any=[];
  modalIndex: any;
  ParentPOForm: FormGroup;
  POForm: FormArray;
  productDetails: any = [];
  productArr: any = [];
  filteredProductArr = [];
  SelectedItemsFilteredArr = [];
  allCategories: any = [];

  selectedMoreInfos = [];
  dupChkMoreInfo = [];

  BeverageCategory: any = [];
  LiquorCategory: any = [];
  retailFoodCategory: any = [];
  InventoryFoodCategory: any = [];
  categoryData: any = [];
  AllSelected: any = []
  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];
  demo: any = []
  subCategoryData: any = [];

  categoryRetData: any = [];
  categoryLiqData: any = []
  InvfirstLevel: any = [];
  InvsecondLevel: any = [];
  InvthirdLevel: any = [];
  InvthirdLevel2: any = [];
  InvforthLevel: any = [];
  InvfiftLevel: any = [];
  RetfirstLevel: any = [];
  RetsecondLevel: any = []
  RetthirdLevel: any = [];
  RetthirdLevel2: any = [];
  RetforthLevel: any = [];
  RetfiftLevel: any = [];
  LiqfirstLevel: any = []
  LigsecondLevel: any = [];
  LiqthirdLevel: any = [];
  LiqthirdLevel2: any = []
  LiqforthLevel: any = [];
  LiqfiftLevel: any = [];
  CancatAllArray: any = [];
  categoryInvData: any = [];
  CategoryAllData: any = [];
  showNoMaterialSelected = true;
  showNoProductSelected = true;
  hideModal = false;
  isDraftData: any;
  isSubmittedReviewData: any;
  approvedData = [];
  receivedData = [];
  submittedReviewData = [];
  draftData = [];
  beveragesCategories = [];
  inventoryFoodCategories = [];
  retailFoodCategories = [];
  liquorCategories = [];
  materialSelected;
  filteredSubmittedRecords: any;
  productNames = [];
  productBrands = [];
  //productArr: any = [];
  selectedProd = [];
  selectedProductArr: any = [];
  addMaterialSelectedItems = [];
  selectedItemsRaisePopUp = [];
  toggleFlagForTemp = true;

  constructor(private modalService: NgbModal, private fb: FormBuilder, private material_service: MaterialServiceService, private router: Router) {
    this.addMaterialSelectedItems = this.material_service.selectedMaterialsAddMaterialPopUp;
    console.log('pop-raise',this.addMaterialSelectedItems);

    this.productFormDetails();

    this.POForm = this.fb.array([
      
    ])

    //***********Items Product Form*********** */
    this.ProductForm = this.fb.group({
      totalPurchaseArr: this.fb.array([])
    });
   
    

    console.log("Selected Items Chips", this.material_service.SelectedItems);
    this.SelectedItems = this.material_service.SelectedItems;
    this.selectedProd = this.material_service.selectedProducts ;
  //  this.addMaterialSelectedItems = this.SelectedItems;
    console.log("selected prod", this.selectedProd);
    console.log("data",this.SelectedItems);

   /*  if (this.SelectedItems.length == 0) {
     // this.router.navigate(['/ngbootstrap/add-pomaterial'])
    } */
    this.getSuppliers();

   //*********add to cart items table************ */

    this.SelectedItems.forEach(element => {
    //this.newItems();
    this.addItems(element.item.subSubCategoryTypeName);

  });
   // this.addItems(this.SelectedItems);
    
    console.log("Product form>>>>>",this.ProductForm.value);

    //*******Get current Stock******/
    // this.material_service.getCurrentStock().subscribe(data=>{
    //   this.currentStockData=data['data']
    //   console.log(">>>>>Current Stock",this.currentStockData);
      
    // })
    this.getCategoriesForInventoryFood();
    this.getCategoriesForRetailFood();
    this.getCategoriesForLiquor();
    this.getCategoriesForBeverages();
    
  }

  createPOForm(id): FormGroup {
    return this.fb.group({
      restaurantId: ['123'],
      supplierId: id,
      podate: [],
      isIncludeGST: [''],
      deliveryAddress: [''],
    })
  }

  addRecordsToPO(id){
    this.POForm.push(this.createPOForm(id));
  }

  ngOnInit() {

  }
  //****************form array functionality of product********** */
  
  items(): FormArray {
    return this.ProductForm.get("totalPurchaseArr") as FormArray
  }
  newItems(): FormGroup {
    return this.fb.group({
      materialName: [''],      
      priceUnit: ['0'],
      orderQty: ['0'],
      currentStock: [''],
      unit: [''],
      itemNumber: [''],
      supplier: [''],
      totalpurchaseprice:[''],
  
      // total:[0],
      // totalPayble:[0],
      // discount:[0],

    })

  }

  addItems(element) {
  const newProItem=this.newItems()
  newProItem.patchValue({
     materialName: element
   })
  console.log("New items",newProItem.value);
    
  this.items().push(newProItem);
  }

  removeItems(i) {
    this.items().removeAt(i);
  }
 

  //***Function of add total price**** */
  totalPrice(tOBJ) {
    let qauntity = parseInt(tOBJ.controls.orderQty.value)
    let price = parseInt(tOBJ.controls.priceUnit.value);
    let total = price * qauntity;
    console.log("total is ", total);
    console.log("order quantity",tOBJ.controls.orderQty.value);
    console.log("Price unit",tOBJ.controls.priceUnit.value);
    tOBJ.patchValue({
    totalpurchaseprice: total
  });
  
  // console.log("This is Main formgroup",this.addMaterialToCartForm.value);
    console.log("index of total",tOBJ);
  
  
  }
  //*****modal function******/
  open(content,i) {
    this.addMaterialSelectedItems = this.material_service.selectedMaterialsAddMaterialPopUp;
    this.selectedItemsRaisePopUp=this.SelectedItems;
   // this.selectedItemsRaisePopUp.splice(0,this.selectedItemsRaisePopUp.length);
    this.modalIndex=i;
    console.log("Index of Modal",i);
    
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //********End Modal Function********/
  getSuppliers() {
    this.material_service.getAllSupliers().subscribe(data => {
      console.log("get all supliers", data);
      this.AllSuppliers=data['data'];
    })
  }
  SetSupplier(a){
    console.log("Set supplier",a);
    this.modalIndex.controls.supplier.setValue(a.supplierDetails.supplierName);
    this.addRecordsToPO(a._id);
    console.log("PO Elements : ",this.POForm.value);
    
  }

  productFormDetails() {
    this.material_service.getAllProductIventory(123).subscribe(data => {
      this.productDetails = data['data'];
      var temp;
      if (this.toggleFlagForTemp === true) {
         temp = this.SelectedItems.length - 1;
      } else {
         temp = (this.SelectedItems.length +  this.material_service.selectedProducts.length) - 1;
      }
      let flag = 0;

      if (this.SelectedItems.length > 0) {
        this.SelectedItems.forEach((filtereditems) => {
            this.SelectedItemsFilteredArr.push(filtereditems.item.subSubCategoryTypeName);
        });
        this.SelectedItems.forEach((selectedItem, itemindex) => {
          this.material_service.selectedProducts.forEach(selectedproduct => {
            if (this.SelectedItemsFilteredArr.includes(selectedproduct.productCategory)) {
              flag = 1;
              if (selectedItem.item.subSubCategoryTypeName === selectedproduct.productCategory) {
                this.ProductForm.controls.totalPurchaseArr['controls'][itemindex].patchValue({
                  itemNumber: selectedproduct.productName,
                  currentStock : selectedproduct.availableQtys[0].quantity,
                  unit: selectedproduct.availableQtys[0].unit,
                }); 
              } 
            } else {
              flag = 0;
              if (flag === 0) {
                     let addMaterialFlag = 0;
                     this.ProductForm.value.totalPurchaseArr.forEach(element => {
                      if (element.materialName === selectedproduct.productCategory) {
                        addMaterialFlag = 1;
                      } /* else {
                        addMaterialFlag = 0;
                      } */

                    });
                     if (addMaterialFlag === 0) { 
                      this.addItems(selectedproduct.productCategory);
                      console.log('prodform', this.ProductForm.value);
                      if (this.toggleFlagForTemp === true) {
                      temp++;
                      }
                      console.log('temp', temp);
                      this.ProductForm.controls.totalPurchaseArr['controls'][temp].patchValue({
                        itemNumber: selectedproduct.productName,
                        currentStock : selectedproduct.availableQtys[0].quantity,
                        unit: selectedproduct.availableQtys[0].unit,
                      });
                     }
               }
              }
          });
        });
      }
        else {
     this.productDetails.forEach(productdetails => {
          this.material_service.selectedProducts.forEach((selectedproduct,prodindex)  => {
            if (productdetails.productName === selectedproduct.productName) {
              this.addItems(selectedproduct.productCategory);
              this.ProductForm.controls.totalPurchaseArr['controls'][prodindex].patchValue({
                itemNumber: productdetails.productName,
                currentStock : productdetails.availableQtys[0].quantity,
                unit: productdetails.availableQtys[0].unit,
              });

            }
          });

        } );
     // });
    }
  });
}

  GeneratePO(){

   /*  this.AllSuppliers.forEach(element => {
      console.log(element);
      this.addRecordsToPO(element._id);
    }); */

    
    console.log("PO Elements : ",this.POForm.value);

    this.material_service.POdetails = this.POForm;

    let addMaterialobj=this.ProductForm.value;
    console.log("Genrate PO",addMaterialobj);
    this.material_service.AddMaterialObj=addMaterialobj;
    if (this.ProductForm.value.totalPurchaseArr.length) {
      this.ProductForm.value.totalPurchaseArr.forEach((ele) =>{
        if (ele.currentStock === '' || ele.itemNumber === '' || ele.orderQty === '' || ele.priceUnit === '' || ele.supplier === '') {
          alert ('Please fill all the fields!')
        } else {
          this.router.navigate(['/ngbootstrap/purchase-order-generate']);
        }
      });
    }
  }

  Selectedsupplier(event){
    console.log("This is Selected supplier",event);
  }


  backBtnOnPORaise() {
    console.log('clicked');
     // Show confirm alert
    let result = confirm('Do you want to go back?');
    if (!result) {
        return;
      } else {
        // Do nothing!
        this.router.navigate(['/ngbootstrap/purchase-order']);
      }
  }
  
  cancel() {
    // Show confirm alert
    let result = confirm('Do you want to go back?');
    if (!result) {
        return;
      } else {
        // Do nothing!
        this.router.navigate(['/ngbootstrap/purchase-order']);
      }
  }
  addInventory(){
    this.router.navigate(['/ngbootstrap/purchase-order']);
  }

  getCategoriesForBeverages() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['BeverageCategory'].length) {
         res['BeverageCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.beveragesCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }

  getCategoriesForInventoryFood() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['InventoryFoodCategory'].length) {
         res['InventoryFoodCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.inventoryFoodCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }

  getCategoriesForLiquor() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['LiquorCategory'].length) {
         res['LiquorCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.liquorCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }
  getCategoriesForRetailFood() {
    let beverageCategoryTemp;
    let subCategoriesTemp;
    let subSubCategoriesTemp;
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    this.material_service.getAllCategories().subscribe((res) => {
       if (res['retailFoodCategory'].length) {
         res['retailFoodCategory'].forEach((beverageCategory) => {
          beverageCategoryTemp = beverageCategory;
          beverageCategoryTemp.subCategories.forEach((subCategories) => {
          subCategoriesTemp = subCategories;
          subCategoriesTemp.subSubCategories.forEach((subSubCategories) =>  {
            subSubCategoriesTemp = subSubCategories;
            subSubCategoriesTemp.subSubCategoryType.forEach((subSubCategoryType) => {
              // console.log('subSubCategoryType', subSubCategoryType);
              this.retailFoodCategories.push(subSubCategoryType);
              });
            });
          });
         });
       }
    });
  }

  selectBevInfo(groupName, s) {
    
    if (!this.dupChkMoreInfo.includes(s)) {
      let obj = {
        group: groupName,
        item: s

      }
      this.showNoMaterialSelected = false;
     // this.dupChkMoreInfo.push(obj);
      this.addMaterialSelectedItems.push(obj);
      // this.BeverageCategory.push({ title: a, moreinfo: b })
      console.log('selected moreinfos', this.dupChkMoreInfo);
      console.log('selected addpopup', this.material_service.selectedMaterialsAddMaterialPopUp);
      this.SelectedItems = this.material_service.selectedMaterialsAddMaterialPopUp;
      this.addItems(obj.item.subSubCategoryTypeName);
      this.toggleFlagForTemp = false;
      this.productFormDetails();
    }
    else {
      alert('Already Added!!');
    } 
  }

  // On Remove MoreInfo 
  removeMoreInfo(s) {
    //this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.rootCategoryName), 1)
    this.dupChkMoreInfo = this.dupChkMoreInfo.filter(item => s.item !== item.item);
    if (this.dupChkMoreInfo.length === 0) {
      this.showNoMaterialSelected = true;
    }
  }

   // ON MOREINFO chips
   selectProductInfo(s) {
     console.log('s', s);
     if (!this.productNames.includes(s)) {
     
      this.showNoProductSelected = false;
      this.productNames.push(s);
      console.log('prodName', this.productNames);
     
    }
    else {
      alert('Already Added!!');
    } 
  }

   // On Remove MoreInfo 
   removeSelectedProductInfo(s) {
    //this.selectedProd = true;
    this.productNames = this.productNames.filter(item => s!==item);
    if (this.productNames.length === 0) {
      this.showNoProductSelected = true;
    }
  }

   // ON MOREINFO chips
   selectProductBrandInfo(s) {
    console.log('s', s);
    if (!this.productBrands.includes(s)) {
    
     this.showNoProductSelected = false;
     this.productBrands.push(s);
     console.log('prodName', this.productBrands);
    
   }
   else {
     alert('Already Added!!');
   } 
 }

  // On Remove MoreInfo 
  removeSelectedProductBrandInfo(s) {
    //this.selectedProd = false;
    this.productBrands = this.productBrands.filter(item => s!==item);
    if (this.productBrands.length === 0) {
      this.showNoProductSelected = true;
    }
  }

  patchProduct() {
    this.material_service.getAllProductIventory(123).subscribe(data => {
  
      this.productArr = data['data'];

    
    });
  }

  SaveAllCategories() {
    this.productArr.forEach(product => {
      this.productNames.forEach(selectedproduct => {
        if (selectedproduct === product.productName) {
          this.material_service.selectedProducts.push(product);
        }
      });
    });
    this.toggleFlagForTemp = false;
    this.productFormDetails();
   // this.material_service.selectedProductsRaisePopUp = this.selectedProductArr;
   // console.log('selectedProductsRaisePopUp',  this.material_service.selectedProductsRaisePopUp);
   // this.dupChkMoreInfo=this.SelectedItems;
    this.modalService.dismissAll();
  }
}
