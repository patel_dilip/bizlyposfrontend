import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderRaisePoComponent } from './purchase-order-raise-po.component';

describe('PurchaseOrderRaisePoComponent', () => {
  let component: PurchaseOrderRaisePoComponent;
  let fixture: ComponentFixture<PurchaseOrderRaisePoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderRaisePoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderRaisePoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
