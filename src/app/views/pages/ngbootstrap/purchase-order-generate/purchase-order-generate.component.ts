import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Form, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'kt-purchase-order-generate',
  templateUrl: './purchase-order-generate.component.html',
  styleUrls: ['./purchase-order-generate.component.scss']
})
export class PurchaseOrderGenerateComponent implements OnInit {
  isShowFreeItemTable: boolean = false;
  whatsNoArr: any = [];
  purchaseOrders = [];
  PoObject = { purchaseOrders: [] };
  PurchaseOrderForm: FormGroup;
  extraChargeForm: FormGroup;
  PurchaseForm: FormGroup;
  itemsForm: FormGroup;
  noificationForm: FormGroup;
  notifications: FormArray;
  emails: FormArray;
  sms: FormArray;
  ProductForm: FormGroup;
  FreeItemsForm: FormGroup;
  freeItems: FormArray;
  purchaseOrderForm: FormGroup;
  totalPaybleForm: FormGroup;
  //charges:any =[];
  // extraCharges: any = []
  extraChargesData: FormArray;
  extraCharges: FormArray;
  ExtraChargesArr: any = [];
  isRemoveextra: boolean = false;
  isAddextra: boolean = true;
  closeResult: string;
  AllSuppliers: any = [];
  materialObj: any = []
  poFinalObj: any = []
  materialPurchase: any = []
  incrPlus = -1;
  incrWPlus = -1;
  incrNoPlus = -1;
  len: number;
  productObj: any = [];
  isShowExtraCharges: boolean = false;
  finalTotal: number = 0;
  objectFinal: any = [];
  calculateTotal: any = [];
  finalExtraTotal: number = 0;
  finalFreeItemTotal: number = 0;
  dateIndex: any;
  isShowWhatsApp: boolean = false;
  isShowEmail: boolean = false;
  isShowSMS: boolean = false;
  isShowExtracharges: boolean = false;
  supplierIdPost: any = [];
  newFinalObj: any = [];
  suppliersPODetails: any;
  finalSuppliersObject = [];
  suppliersDetails: FormArray;
  tempSuppliers = [];
  suppliers: any;
  totalPayableWithExtraCharge;
  extraAmount;
  totalPaybleData: FormArray;
  finalTotalArr = [];
  totalPaybleValue: any;
  tempTotalPayAmt = 0;
  selectedSuppliers = [];
  poFinalObject: any;
  currentDate ;
  isValid;
  priceWithoutTax = 0;
  priceWithTax = 0;
  totalPriceAfterDiscount = 0;
  discountPerItem = 0;
  totalDiscount = 0;
  itempriceAfterDiscount = 0;
  taxPerItem = 0 ;
  totalTax = 0;
  contributionOnTotalPerItem = 0;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  displayExtraChargeArr = [];
  isGstIncludeFlag:boolean = false;
  updatedDiscount = 0;
  freeItemAmount = 0;
  quantityPriceProd = 0;
  selectedFreeItem = 0;
  freeItemIndexValue = 0;
  freeItemIndexValueCount = 0;
  whatsAppIndexValueCount = 0;
  freeItemIndexArr = [];
  whatsAppArr = [];
  emailsArr = [];
  smsArr = [];
  whatsAppIndexValue = 0;
  emailIndexValue = 0;
  smsIndexValue = 0;
  discountFlag: boolean = false;
  freeItemTableValue = 0;
  freeItemTableValueCount = 0;
  freeItemTableArr = [];
  displayFreeItem = [];
  freeItemSupplierList = [];
  productArr = [];
  selectedProductArr = [];
  materialArr = [];
  selectedMaterialArr = [];

  currentJustify = 'start';
  counter = 0;

  filteredSuppliers = [];

constructor(private router: Router, private fb: FormBuilder, private modalService: NgbModal, private material_service: MaterialServiceService, ) {
    this.currentDate = moment(). format('YYYY-MM-DD');
    this.getSuppliers();
    console.log('inside const1',this.AllSuppliers);

   // this.taxValuespatch();
    console.log('inside const2',this.AllSuppliers);

    //*****Notification Form*******/
    this.noificationForm = this.fb.group({
      notifications: this.fb.array([]),
      emails: this.fb.array([]),
      sms: this.fb.array([])
    });
    this.addWhatsApp();
    this.addEmails();
    this.addsms();

    console.log('Notification : ', this.noificationForm.value);

    //***********Items Product Form*********** */
    this.ProductForm = this.fb.group({
      totalPurchaseArr: this.fb.array([])
    })
    this.productObj = this.material_service.AddMaterialObj
    //this.calculateTotal=this.productObj['totalPurchaseArr']
    //console.log("this is calculate total",this.calculateTotal)
    console.log('this is product  object>>>>>', this.productObj);
    this.addItems();
    console.log('this is form array product', this.ProductForm.value);
    this.patchValue();

    /***************Total Payble Data Form ********************/
    this.totalPaybleForm = this.fb.group({
      totalPaybleData: this.fb.array([])
    });

    this.createTotalPaybleData();

    //***********Free Items Form***********/
    this.FreeItemsForm = this.fb.group({
      freeItems: this.fb.array([])
    });
    this.freeItemsdata();

    console.log('Free Items : ', this.FreeItemsForm.value);

    //**************Extra charge Form********** */
    // this.extraChargeForm = this.fb.group({
    //   extraCharges: this.fb.array([])
    // });
    // this.addExtracharges();
    this.extraChargeForm = this.fb.group({
      extraCharges: this.fb.array([
        // this.extraCharges = this.fb.array([

        // ])
      ])
    });
    this.addExtraChargesData();
    console.log('Extra Charges Form : ', this.extraChargeForm.controls.extraCharges['controls']);

    //this.calcTotal();

    this.material_service.getAllProductIventory(123).subscribe(data => {
      this.productArr = data['data'];
      console.log()
    });

    this.patchProductInFreeItem();
  }

ngOnInit() {

    this.purchaseOrderForm = this.fb.group({
      suppliersPODetails: this.material_service.POdetails
    });
    console.log('Purchase Order : ', this.purchaseOrderForm.value);
  }

patchProductInFreeItem(){
  this.material_service.getAllProductIventory(123).subscribe(data => {
    this.productArr = data['data'];
  });
}

autoCompleteProduct(supplier, i, j){
  // document.getElementById('product-block').style.display = "block";

  let productData = this.FreeItemsForm.controls.freeItems['controls'][i].value[j].productname;

  console.log(productData);

  if(document.getElementById('product-block').style.display = "none"){
    document.getElementById('product-block').style.display = "block";
  }
  
  this.selectedProductArr = this.productArr.filter(product => 
    product.productName.toLowerCase().includes(productData.toLowerCase())
  );
  
  console.log(JSON.stringify(this.selectedProductArr));
}

patchSelectedProduct(product,material,unit,i,j){
  this.FreeItemsForm.controls.freeItems['controls'][i].controls[j].patchValue(
    {
      productname : product,
      materialname : material,
      unit: unit
    }
  )
  
  this.selectedProductArr = [];
  document.getElementById('product-block').style.display = "none";
}

autoCompleteMaterial(supplier, i, j){

  let materialData = this.FreeItemsForm.controls.freeItems['controls'][i].value[j].materialname;
  
  console.log(materialData);

  if(document.getElementById('material-block').style.display = "none"){
    document.getElementById('material-block').style.display = "block";
  }

  this.selectedMaterialArr = this.productArr.filter(product => 
      product.productCategory.toLowerCase().includes(materialData.toLowerCase())
    );
  
  console.log(JSON.stringify(this.selectedMaterialArr));
}

patchSelectedMaterial(material,i,j){
  this.FreeItemsForm.controls.freeItems['controls'][i].controls[j].patchValue(
    {
      materialname : material,
      productname: '',
      unit: ''
    }
  )

  this.selectedMaterialArr = [];
  document.getElementById('material-block').style.display = "none";
}

taxValuespatch() {
    console.log('inside ngOninte', this.AllSuppliers);
    let priceWithNoTax = 0;
    this.ProductForm.get('totalPurchaseArr').value.forEach((element, index) => {
          this.ProductForm.controls.totalPurchaseArr['controls'][index].patchValue({
            totalpurchasepricewithouttax: element.totalpurchaseprice,
          });
          this.priceWithoutTax = 0;
           // priceWithNoTax = priceWithNoTax + this.ProductForm.get("totalPurchaseArr")['controls'][0].value.totalpurchaseprice;
          this.priceWithoutTax = this.priceWithoutTax + element.totalpurchaseprice;
          priceWithNoTax = this.priceWithoutTax;
           // this.priceWithoutTax =  priceWithNoTax;
          this.totalPaybleForm.controls.totalPaybleData['controls'][index].patchValue({
               totalpurchaseprise: priceWithNoTax,
               totalpayble: priceWithNoTax,
               totalpriceafterdiscount: priceWithNoTax
                 // totaltax: total_tax
               });
          });
  }

createTotalPaybleData() {
    this.material_service.POdetails.value.forEach(element => {
      console.log(element);
      const add = this.totalPaybleForm.get('totalPaybleData') as FormArray;
      add.push(this.fb.group({
        totalpurchaseprise: [''],
        discount: [''],
        discounttype: [''],
        totalpriceafterdiscount: [''],
        totaltax: ['0'],
        totalpayble: [''],
      }))
    });
  }


  //on delivery address changed
changeDeliveryAddress(id) {
    console.log(id)
    // this.suppliersPODetails.forEach(element =>{
    //   if(element.supplierId == id){
    //     element.deliveryAddress = item.value;
    //     console.log("Delivery Address : ",element.deliveryAddress);
    //   }
    // })
  }

  //******purchase order Gst Checked or not*** */
isGstInclude(event) {
    console.log('GST include or not', event);
    this.isGstIncludeFlag =  event.checked;
  }
  //***********Notification Form functionality**** */

  // // notifications(): FormArray {
  // //   return this.noificationForm.get("notifications") as FormArray
  // // }
  // newWhatsAppNot(): FormGroup {
  //   return this.fb.group({
  //     whatsapps: [''],
  //   })

  // }

addWhatsappBlock(supplier, i,c,d,e) {
  /* this.noificationForm.get('notifications')['controls'][i].patchValue({
    whatsapps: [e],
  }); */
    this.tempSuppliers.forEach(element => {
      if (supplier == element) {
        const add = this.noificationForm.get('notifications')['controls'][c] as FormArray;
        add.push(this.fb.group({
          whatsapps: ['']
        }))
      }

    })
  }

addWhatsApp() {
    let index = 0;
    this.material_service.POdetails.value.forEach((element, index) => {
      const addWhatsappData = this.noificationForm.get('notifications') as FormArray;
      addWhatsappData.push(this.fb.array([]));
      this.addWhatsappGroup(index);
    });
  }

addWhatsappGroup(index) {
    const addWhatsapp = this.noificationForm.get('notifications')['controls'][index] as FormArray;
    addWhatsapp.push(this.fb.group({
      whatsapps: ['']
    }))
  }
  // onChangeWhatsApp(event) {
  //   console.log("checked Whats App", event.checked);

  // }

removeWhatsapp(a,c) {
  let addWhatsApp;
  addWhatsApp = this.noificationForm.get('notifications')['controls'][a] as FormArray;
  addWhatsApp.removeAt(c);
}

removeEmail(a,c) {
  let addWhatsApp;
  addWhatsApp = this.noificationForm.get('emails')['controls'][a] as FormArray;
  addWhatsApp.removeAt(c);
}

removeSms(a,c) {
  let addWhatsApp;
  addWhatsApp = this.noificationForm.get('sms')['controls'][a] as FormArray;
  addWhatsApp.removeAt(c);
}

showWhatsapp(checkEvent, contactNo,i,supplierName) {
   this.whatsAppArr.push(i);
   this.tempSuppliers.forEach((element,index) => {
    if(index==i){
      if(element === supplierName){
        this.whatsAppIndexValue = i;
        this.whatsAppIndexValueCount++;
        this.isShowWhatsApp = true;
        this.noificationForm.controls.notifications['controls'][0].controls[0].patchValue({ whatsapps: contactNo});
      }  else {
        this.isShowWhatsApp = false;
      }
    }
   });
  /*   console.log(' Event : ', checkEvent.checked);
    if ((this.materialObj[i].supplier === supplierName)) {
      console.log('**************');
      checkEvent.checked == true;
      this.isShowWhatsApp = true;
      this.noificationForm.controls.notifications['controls'][i].controls[0].patchValue({ whatsapps: contactNo});
    } else {
      this.isShowWhatsApp = false;
    } */
  }
  //****Email functions**** */

  // emails(): FormArray {
  //   return this.noificationForm.get("emails") as FormArray
  // }
  // newEmails(): FormGroup {
  //   return this.fb.group({
  //     emails: [''],

  //   })

  // }

addEmailBlock(supplier, i) {
    this.tempSuppliers.forEach(element => {
      if (supplier == element) {
        const add = this.noificationForm.get('emails')['controls'][i] as FormArray;
        add.push(this.fb.group({
          emails: ['']
        }))
      }
    })
  }

addEmails() {
    let index = 0;
    this.material_service.POdetails.value.forEach((element, index) => {
      const addEmailData = this.noificationForm.get('emails') as FormArray;
      addEmailData.push(this.fb.array([]));
      this.addEmailGroup(index);
    });
  }

addEmailGroup(index) {
    const addEmail = this.noificationForm.get('emails')['controls'][index] as FormArray;
    addEmail.push(this.fb.group({
      emails: ['']
    }))
  }

  // addEmails() {
  //   this.emails().push(this.newEmails());
  // }
showEmails(checkEvent,tempSuppliers,i,supplier) {
  this.emailsArr.push(i);
  this.tempSuppliers.forEach((element,index) => {
    if(index==i){
      if(element === supplier){
        this.emailIndexValue = i;
        this.emailIndexValue++;
        this.isShowEmail = true;
      }  else {
        this.isShowEmail = true;
      }
    }
   });
    /* if (checkEvent.checked == true) {
      this.isShowEmail = true;
     // this.noificationForm.controls.notifications['controls'][i].controls[0].patchValue({ whatsapps: contactNo});
    }
    else {
      this.isShowEmail = false;
    } */
  }

addSMSBlock(supplier, i) {
    this.tempSuppliers.forEach(element => {
      if (supplier == element) {
        const add = this.noificationForm.get('sms')['controls'][i] as FormArray;
        add.push(this.fb.group({
          sms: ['']
        }))
      }
    })
  }

addsms() {
    let index = 0;
    this.material_service.POdetails.value.forEach((element, index) => {
      const addSMSData = this.noificationForm.get('sms') as FormArray;
      addSMSData.push(this.fb.array([]));
      this.addSMSGroup(index);
    });
  }

addSMSGroup(index) {
    const addSMS = this.noificationForm.get('sms')['controls'][index] as FormArray;
    addSMS.push(this.fb.group({
      sms: ['']
    }))
  }


showSMS(checkEvent,tempSuppliers, i,supplier) {
  this.smsArr.push(i);
  this.tempSuppliers.forEach((element,index) => {
    if(index==i){
      if(element === supplier){
        this.smsIndexValue = i;
        this.smsIndexValue++;
        this.isShowSMS  = true;
      }  else {
        this.isShowSMS = true;
      }
    }
   });
    /* if (checkEvent.checked == true) {
      this.isShowSMS = true;
    }
    else {
      this.isShowSMS = false;
    } */
  }
  //*************Product item form functionality********* */
items(): FormArray {
    return this.ProductForm.get('totalPurchaseArr') as FormArray
  }
newItems(): FormGroup {
    return this.fb.group({
      materialName: [''],
      itemNumber: [''],
      orderQty: [''],
      priceUnit: [''],
      supplier: [''],
      estimateddeliverytime: [''],
      tax: [''],
      totalpurchaseprice: [''],
      totalpurchasepricewithouttax: [''],
      totalpurchasepriceondiscount: ['']
    });

  }

addItems() {
    this.items().push(this.newItems());
  }
patchValue() {

    this.clearFormArray()
    var data = this.productObj
    console.log('this is patch function', data);

    data.totalPurchaseArr.forEach(t => {
      var item = this.newItems()
      this.items().push(item)


    });

    this.ProductForm.patchValue(this.productObj);
  }

clearFormArray() {


    this.items().clear();


  }

addFreeItems(supplier, i) {
    this.tempSuppliers.forEach(element => {
      if (supplier == element) {
        const add = this.FreeItemsForm.get('freeItems')['controls'][i] as FormArray;
        add.push(this.fb.group({
         // productcode: '',
          productname: '',
          materialname: '',
          unit: '',
          quantity: '',
          price: ''
        }));
      }
    });
  }
removeItems(t,j, supplier, index, i) {
  console.log('quantityprod', this.quantityPriceProd);
  /* this.quantityPriceProd = 0;
    this.tempSuppliers.forEach(element => {
      if(supplier == element){
       const add = this.FreeItemsForm.get('freeItems')['controls'][i] as FormArray;
       this.FreeItemsForm.controls.freeItems['controls'][i].value.forEach((ele)=> {
        let quantityPriceProd = (+ele.quantity)*(+ele.price) ;
        this.quantityPriceProd =  this.quantityPriceProd - quantityPriceProd;
       });
       add.removeAt(i);
      }
    }) */
  console.log('t,j,supplier,index', t, j, supplier, index);
   //this.tempSuppliers.forEach(element => {
     //  if (supplier === element) {
  this.selectedFreeItem = 0;
  t.value.forEach(element => {
    this.selectedFreeItem = (element.quantity) * (element.price);
  });
     
  this.displayFreeItem.forEach(element => {
    if(element.supplier === supplier){
      element.quantityPriceProd = element.quantityPriceProd - (this.selectedFreeItem);
      element.finalFreeItemTotal = element.finalFreeItemTotal - this.selectedFreeItem;
    }
  })
  // this.quantityPriceProd = this.quantityPriceProd - (this.selectedFreeItem);
  // this.finalFreeItemTotal = this.finalFreeItemTotal - this.selectedFreeItem;

  this.FreeItemsForm.get('freeItems')['controls'][i].removeAt(j)
     //  }
  // })
  }

freeItemsdata() {
    let index = 0;
    this.material_service.POdetails.value.forEach((element, index) => {
      const addFreeItemData = this.FreeItemsForm.get('freeItems') as FormArray;
      addFreeItemData.push(this.fb.array([]));
      this.freeItem(index);
    });
  }

freeItem(index) {
    const addFreeItem = this.FreeItemsForm.get('freeItems')['controls'][index] as FormArray;
    addFreeItem.push(this.fb.group({
    //  productcode: '',
      productname: '',
      materialname: '',
      unit: '',
      quantity: '',
      price: ''
    }));

    console.log('Array of free items : ', addFreeItem);
    // this.items().push(addFreeItem);
  }

  //>>>>>>>>Show Free item Table>>>>>>>>
FreeItemTable(a, i) {
  // if (event.type === 'click') {
  // this.isShowFreeItemTable = true;
  // }
  // console.log('in if');
  // return;
  this.selectedMaterialArr = [];
  console.log
  this.selectedProductArr = [];

  this.freeItemTableArr.push(i);
      this.tempSuppliers.forEach((element,index) => {
        if(index==i){
          if(element == a){
            this.freeItemTableValue = i;
            this.freeItemTableValueCount++;
            this.isShowFreeItemTable = true;
          }
          else{
            this.isShowFreeItemTable = false;
          }
        }
      })

  }

calcFreeItemTotal(t, j, supplier, indexValue) {
  let discountPrice = this.totalPaybleForm.controls.totalPaybleData['controls'][indexValue].value.discount;
  let discountType = this.totalPaybleForm.controls.totalPaybleData['controls'][indexValue].value.discounttype;
  let totalPrice = this.totalPaybleForm.controls.totalPaybleData['controls'][indexValue].value.totalpurchaseprise;

  this.finalFreeItemTotal = 0;
  this.quantityPriceProd = 0;
  this.updatedDiscount = 0;
  if (discountType === 'price') {
    this.updatedDiscount = this.updatedDiscount + discountPrice ;
  } else if (discountType === 'percentage') {
    this.updatedDiscount =  this.updatedDiscount + ((+(totalPrice * (discountPrice)) / 100));
  }
  this.tempSuppliers.forEach( element => {
    if (supplier === element) {
        t.value.forEach(ele => {
          this.quantityPriceProd = this.quantityPriceProd + ((+ele.quantity) * (+ele.price));
        });

        this.finalFreeItemTotal = this.quantityPriceProd + (+this.updatedDiscount);
    }
   });


   var keepgoing = true;
   if(this.displayFreeItem.length === 0){
    this.displayFreeItem.push({
      updatedDiscount : this.updatedDiscount,
      quantityPriceProd : this.quantityPriceProd,
      finalFreeItemTotal : this.finalFreeItemTotal,
      supplier : supplier
    })
    this.freeItemSupplierList.push(supplier);
  }
  else{
    this.displayFreeItem.forEach(element => {
      if(keepgoing){
        if(element.supplier === supplier){
          if(element.quantityPriceProd !== this.quantityPriceProd){
            element.updatedDiscount = discountPrice
            element.quantityPriceProd = this.quantityPriceProd;
            element.finalFreeItemTotal = element.quantityPriceProd + element.updatedDiscount;
            element.supplier = supplier;
          }     
          keepgoing = false;
        }
        else{
          // this.displayFreeItem.forEach((display,index) => { 
          //   if(this.displayFreeItem[index].supplier !== supplier && this.displayFreeItem[index].quantityPriceProd !== this.quantityPriceProd){
            if(this.freeItemSupplierList.includes(supplier)){
              return;
            }  
            else{
              this.displayFreeItem.push({
                updatedDiscount : this.updatedDiscount,
                quantityPriceProd : this.quantityPriceProd,
                finalFreeItemTotal : this.finalFreeItemTotal,
                supplier : supplier
              })
              this.freeItemSupplierList.push(supplier);
              keepgoing = false;
            }
          //   }
          // }) 
        }
      }
    })
  }
  }

addExtrachargesBlock(supplier, i) {
      this.tempSuppliers.forEach(element => {
        if (supplier == element) {
          const add = this.extraChargeForm.get('extraCharges')['controls'][i] as FormArray;
          add.push(this.fb.group({
            chargesname: [''],
            amount: ['']
          }))
        }
      })
    }

addExtraChargesData(){
      let index = 0;
      this.material_service.POdetails.value.forEach((element, index) => {
        const addExtraChargeData = this.extraChargeForm.get('extraCharges') as FormArray;
        addExtraChargeData.push(this.fb.array([]));
        this.addExtraCharges(index);
      });
    }

addExtraCharges(index){
      const addExtraCharge = this.extraChargeForm.get('extraCharges')['controls'][index] as FormArray;
      addExtraCharge.push(this.fb.group({
        chargesname: [''],
        amount: ['']
      }))
    }
  displayExtraCharges(charges,supplier,i,j) {
    let discount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discount;
    let totalPaybleAmount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpayble;

    if(discount === ""){
      // this.discountFlag = true;
      document.getElementById('error-msg').style.display = "block";
      this.extraChargeForm.controls.extraCharges['controls'][j].patchValue({
        chargesname : "",
        amount : ""
      });
    }
    else{
      this.discountFlag = false;
      if(charges.value.chargesname !== "" && charges.value.amount !== ""){
          if(this.displayExtraChargeArr.length === 0){
            this.displayExtraChargeArr.push( 
              { 
                chargesname : charges.value.chargesname,
                amount : charges.value.amount,
                supplier : supplier
              } 
            );

            totalPaybleAmount = totalPaybleAmount + (+charges.value.amount);
            this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
              totalpayble: totalPaybleAmount,
            });

            document.getElementById('error-msg').style.display = "none";
          }
          else{
            this.displayExtraChargeArr.forEach(element => {
              if(element.chargesname !== charges.value.chargesname && element.amount !== charges.value.amount){
                this.displayExtraChargeArr.push( 
                  { 
                    chargesname : charges.value.chargesname,
                    amount : charges.value.amount,
                    supplier : supplier
                  } 
                );
                totalPaybleAmount = totalPaybleAmount + (+charges.value.amount);
                this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
                  totalpayble: totalPaybleAmount,
                });
                document.getElementById('error-msg').style.display = "none";
              }
            })
          }
      }
    }
  }
removeExtraCharges(i,indexValue,j, charges) {
      let totalPaybleAmount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpayble;
      console.log('iv', indexValue);
      console.log('j', j);
      let addExtraCharge;
      addExtraCharge = this.extraChargeForm.get('extraCharges')['controls'][i] as FormArray;
      addExtraCharge.removeAt(j);

      var index = this.displayExtraChargeArr.indexOf(charges.value.amount);

      this.displayExtraChargeArr.splice(index,1);

      totalPaybleAmount = totalPaybleAmount - charges.value.amount;

      this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
        totalpayble: totalPaybleAmount,
      });

      //addExtraCharge  = addExtraCharge.filter(obj => obj[j] !== addExtraCharge.value[j]);
    }
increCharges(i) {

      this.incrPlus = i;
    }

ShowExtracharges(supplierName, i) {
      this.freeItemIndexArr.push(i);
      this.tempSuppliers.forEach((element,index) => {
        if(index==i){
          if(element == supplierName){
            this.freeItemIndexValue = i;
            this.freeItemIndexValueCount++;
            this.isShowExtracharges = true;
          }
          else{
            this.isShowExtracharges = false;
          }
        }
      })
    }

    //***********End Extra Charges Functionality******** */
showObject() {
      console.log('this is purchase obj', this.poFinalObj);

    }
    //***Function of add total price with Tax**** */
totalPrice(t, supplier, i) {
      let discountPrice = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discount;
      let totalPurchasePrise = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpurchaseprise;

      const qauntity = parseInt(t.controls.orderQty.value)
      const price = parseInt(t.controls.priceUnit.value);
      const TaxPer = parseInt(t.controls.tax.value);
      const totalPrice = parseInt(t.controls.totalpurchaseprice.value);
     // const totalpurchasepriceWithTax = +(t.controls.totalpurchasepriceWithTax.value);
      console.log('this is total price inculde tax', totalPrice);

      console.log(supplier);
    
      if (TaxPer >= 1 && TaxPer < 100) {
        if (discountPrice === '' ) {
          this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
            totalpriceafterdiscount: totalPurchasePrise
          });
        }
        this.isValid = true;
        const totalpurchasepricewithouttax = (price * qauntity);
        const totalWithTax = ((price * qauntity) / 100) * TaxPer;
        const TaxtotalPrice = (price * qauntity) + (totalWithTax);
  
  
        console.log('this is tax', TaxPer);
  
        console.log('order quantity', qauntity);
        console.log('Price unit', price);
        console.log('total price with tax', totalWithTax);
        //*********Patch value of total price */
        t.patchValue({
          totalpurchaseprice: TaxtotalPrice,
          totalpurchasepricewithouttax: totalpurchasepricewithouttax,
        });
        console.log('this is product form', this.ProductForm.value);
  
        this.calcTotal(supplier, i);
      } else {
        this.isValid = false;
        alert('Please Enter a tax between 0-100');
      }
    }
    //************total of all items calculate **********************/
calcTotal(supplier, i) {
      console.log('Value of i : ', i);
      let arrayObj = this.ProductForm.controls.totalPurchaseArr.value;
      this.finalTotal = 0;
      const totalarr = this.productObj.totalPurchaseArr
      console.log('total arr>>>>', totalarr);
      console.log('Array Obj : ', arrayObj);
      arrayObj.forEach(element => {
        if (supplier == element.supplier) {
          this.calcDiscount(supplier, i);
          console.log('>>>>>Price', element.totalpurchaseprice);
          this.finalTotal = this.finalTotal + element.totalpurchaseprice;
          this.totalPaybleValue = this.finalTotal;
      
          console.log(this.totalPaybleValue);
        }
      
      });
      console.log('this is final total', this.finalTotal);

    }

taxCalculation(t, supplier, i) {
      let totalPriceWithTax = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpurchaseprice;
      let discountType = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discounttype;
      let discountPrice = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discount;
      let totalPurchasePrise = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpurchaseprise;

      const quantity = parseInt(t.controls.orderQty.value)
      const price = parseInt(t.controls.priceUnit.value);
      const tax = parseInt(t.controls.tax.value);
      const totalpurchaseprice = parseInt(t.controls.totalpurchaseprice.value);
      const totalWithTax = ((price * quantity) / 100) * tax;
      const TaxtotalPrice = (price * quantity) + (totalWithTax);
      t.patchValue({
        totalpurchaseprice: TaxtotalPrice,
       // totalpurchasepricewithouttax: totalpurchasepricewithouttax,
      });
      if (discountPrice === '' && discountType === '') {
        this.tempSuppliers.forEach((element, tempIndex) => {
          if(supplier === element){
          if (this.productObj.totalPurchaseArr.length) {
            // this.totalPriceAfterDiscount = 0;
            this.totalTax = 0;
            let totalPayableAmt = 0;
            let totalWithTaxes = 0;
            let taxtotal = 0;

            this.ProductForm.get('totalPurchaseArr').value.forEach(ele => {
            if (ele.supplier === element) {
              taxtotal = taxtotal + tax;
              totalWithTaxes = totalWithTaxes + (ele.totalpurchaseprice);
              // totalPayableAmt =  totalPayableAmt + totalPriceWithTax;
              this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
                totalpriceafterdiscount: totalPurchasePrise,
                totaltax: taxtotal,
                totalpayble: totalWithTaxes
              });
            }
          });
          }
      }
    });
  } else {
     this.calcDiscount(supplier, i);
  }
    }
    
TotalExtraItems(supplier,i ,j) {
      let discount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discount;
      // let tax = this.ProductForm.controls.totalPurchaseArr['controls'][j].value.tax;
      let extraChargeAmount = 0;
      let totalPaybleAmount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpayble;
      this.extraChargeForm.controls.extraCharges['controls'][i].value.forEach((ele) => {
          extraChargeAmount = (extraChargeAmount) +  (+ele.amount);
      });
     
      this.tempSuppliers.forEach(element => {
        if (supplier === element) {
          if (+extraChargeAmount && discount !== '') {
            totalPaybleAmount  = this.tempTotalPayAmt + (+extraChargeAmount);
          } else if (extraChargeAmount && (discount === '')) {
            totalPaybleAmount = totalPaybleAmount + extraChargeAmount;
          } 
          else {
            totalPaybleAmount  = this.tempTotalPayAmt  - (+extraChargeAmount);
          } 
        }
      });
      this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
        totalpayble: totalPaybleAmount,
      });
    }

calcDiscount(supplier , i) {
      // this.discountFlag = false;
      let totalAmount = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.totalpurchaseprise;
      let discountType = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discounttype;
      let discountPrice = this.totalPaybleForm.controls.totalPaybleData['controls'][i].value.discount;
      this.tempSuppliers.forEach((element,indexValue) => {
        if(supplier === element){

      if ( discountType === 'price') {

        if (this.productObj.totalPurchaseArr.length) {
           this.totalPriceAfterDiscount = 0;
           this.totalTax = 0;
           if (discountPrice === '') {
             return;
           } else {

              this.displayFreeItem.forEach(free => {
                if(free.supplier == supplier){
                  free.updatedDiscount = 0;
                }
              });
              this.ProductForm.get('totalPurchaseArr').value.forEach((ele, index1) => {
              if (ele.supplier === element) {
                this.itempriceAfterDiscount = 0;
                this.totalDiscount = 0;
  
                let tax = +(ele.tax);
                this.discountPerItem = ((ele.totalpurchasepricewithouttax) / totalAmount) * (discountPrice);
                this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
                this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
                this.totalTax = this.totalTax + this.taxPerItem;
                this.totalPriceAfterDiscount = this.totalPriceAfterDiscount + this.itempriceAfterDiscount;
                this.totalDiscount = this.totalDiscount + this.discountPerItem;

                this.displayFreeItem.forEach(freeitem => {
                  if(freeitem.supplier == supplier){
                    freeitem.updatedDiscount = freeitem.updatedDiscount + discountPrice;
                    freeitem.finalFreeItemTotal = freeitem.quantityPriceProd + this.updatedDiscount;
                  }
                })

                console.log(this.displayFreeItem);
              }
              if (ele.supplier === supplier) {
                this.ProductForm.controls.totalPurchaseArr['controls'][index1].patchValue({
                  totalpurchasepriceondiscount: this.itempriceAfterDiscount,
                });
              }
            });
             this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;
           }
    }

      } else if (discountType === 'percentage') {
        if (this.productObj.totalPurchaseArr.length) {
          this.totalPriceAfterDiscount = 0;
          this.totalTax = 0;
          this.itempriceAfterDiscount = 0;

          this.displayFreeItem.forEach(free => {
            if(free.supplier == supplier){
              free.updatedDiscount = 0;
            }
            });
            this.ProductForm.get('totalPurchaseArr').value.forEach((ele,index1) => {
            if (ele.supplier === element) {
              this.totalDiscount = 0;
              this.contributionOnTotalPerItem = 0;

              let tax = +(ele.tax);
              this.totalPriceAfterDiscount = totalAmount - (totalAmount * (discountPrice / 100));
              this.totalDiscount =   this.totalDiscount + (totalAmount * (discountPrice / 100));
              this.contributionOnTotalPerItem = (ele.totalpurchasepricewithouttax / totalAmount) * (100);
              this.discountPerItem = (this.totalDiscount * (this.contributionOnTotalPerItem / 100) );
              this.itempriceAfterDiscount = (ele.totalpurchasepricewithouttax)  - (this.discountPerItem);
              this.taxPerItem = this.itempriceAfterDiscount * (tax / 100);
              this.totalTax = +(this.totalTax + this.taxPerItem).toFixed(2);
              console.log('tottax', this.totalTax);
              
              this.displayFreeItem.forEach(freeItemDisplay => {
                if(freeItemDisplay.supplier == supplier){
                  freeItemDisplay.updatedDiscount = freeItemDisplay.updatedDiscount + (totalAmount * (discountPrice / 100));
                  freeItemDisplay.finalFreeItemTotal = freeItemDisplay.quantityPriceProd + freeItemDisplay.updatedDiscount;
                }
              });
            }
            if (ele.supplier === supplier) {
            this.ProductForm.controls.totalPurchaseArr['controls'][index1].patchValue({
              totalpurchasepriceondiscount: this.itempriceAfterDiscount,
            });
          }
      
          });
          this.tempTotalPayAmt =  this.totalPriceAfterDiscount + this.totalTax;
        }
      }
      
      this.totalPaybleForm.controls.totalPaybleData['controls'][i].patchValue({
        discount: discountPrice,
        discounttype: discountType,
        totalpriceafterdiscount: this.totalPriceAfterDiscount,
        totaltax: this.totalTax,
        totalpayble:  this.tempTotalPayAmt,
      })
    }
  });
    
     
    }
    //**********Estimated Date Same To all Functionality********** */
onSameAs(i) {
      this.dateIndex = i;
      console.log('Index of estimated date', i);
    }

    //******modal function***** */
open(content) {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  //***********End Of Modal Function*********** */

  //**********Save Post Purchase Orders*************** */

getPO() {
    this.suppliersPODetails = this.purchaseOrderForm.get('suppliersPODetails') as FormArray;
  }

save_purchaseOrder() {
    //*************this is Final Object******Combination of all formgroup********* */

   
    this.getPO();
    console.log('Purchase order : ', this.suppliersPODetails.value);

    let index = 0;
    console.log('Po : ', this.PoObject.purchaseOrders);

    /* PO Object Logic */
    this.poFinalObj.forEach((element, index) => {
      if (element.items.length > 0) {
      this.suppliersPODetails.value.forEach((ele) => {
        ele.podate  = element.podate;
      })

      /* Product value logic */
      let productValue = [];
      this.ProductForm.controls.totalPurchaseArr.value.forEach(prodelement => {
        if (element.supplierName == prodelement.supplier) {
          productValue.push(prodelement);
          console.log('Product Value : ', productValue);
        }
      });

      /*Total Payable logic*/
      let totalPaybleJson = [];
      let totalindex = 0;
      var keepgoing = true;
      this.totalPaybleForm.controls.totalPaybleData['controls'].forEach((totalelement, totalindex) => {
        console.log('Total Payble array : ', totalelement);
        // if (index == totalindex) {
          if(keepgoing){
            this.filteredSuppliers.forEach((filteredData, filterIndex) => {
              console.log(filterIndex + " " +totalindex);
              if(filterIndex == totalindex){
                if(this.totalPaybleForm.controls.totalPaybleData['controls'][totalindex].value.totalpayble === totalelement.value.totalpayble){
                  totalPaybleJson.push(totalelement.value);
                  keepgoing = false;
                }
              }
            })
          }
        // }
      });

      /*extraCharges logic*/
      let totalExtraChargesJson = { extracharges: [] };
      this.extraChargeForm.controls.extraCharges['controls'][0].value.forEach((chargesElement) => {
        console.log(chargesElement);
        if (chargesElement.amount !== '' && chargesElement.chargesname !== '') {
          totalExtraChargesJson.extracharges.push(chargesElement); 
        }
        console.log('Total Extra Charges Json : ', totalExtraChargesJson);
      })

      /*Notification Logic*/
      let notificationJson = { whatsapps: [], emails: [], sms: [] };
      let whatsappJson = [];
      let emailJson = [];
      let smsJson = [];
      this.noificationForm.controls.notifications['controls'][0].value.forEach((whatsappElement) => {
        if (whatsappElement.whatsapps !== '') {
          notificationJson.whatsapps.push(whatsappElement);
        }
      })
      this.noificationForm.controls.emails['controls'][0].value.forEach((emailElement) => {
        if(emailElement.emails !== '') {
          notificationJson.emails.push(emailElement);
        }
      })
      this.noificationForm.controls.sms['controls'][0].value.forEach((smsElement) => {
        if(smsElement.sms !== '') {
          notificationJson.sms.push(smsElement);
        }
      })

      /*FreeItem Logic*/
      let freeItemJson = { freeitems: [] }
      this.FreeItemsForm.controls.freeItems['controls'][0].value.forEach((freeElement) => {
        if (freeElement.materialname !== '' &&  freeElement.price !== '' && freeElement.quantity !== '' ) {
          freeItemJson.freeitems.push(freeElement);
        }
      })
      console.log('Free Items Json : ', freeItemJson);
      let currentDateTime = moment().format('YYYY-MM-DD HH:mm:ss');

      let user = 'userABC';

      this.objectFinal = {
        //purchaseOrder
        purchaseOrder: this.suppliersPODetails.at(0).value,
        //product          
        product: productValue,
        //extracharges
        extracharges: totalExtraChargesJson,
        //totalPayble
        totalpayble: totalPaybleJson,
        //FreeItem
        freeitem: freeItemJson,
        //notification
        notification: notificationJson,

         createdAt: currentDateTime,
         
         createdBy: user,
         
         isSubmitForReview: true,
      }

      this.PoObject.purchaseOrders.push(this.objectFinal);

      console.log('PO Object : ', this.PoObject);
    }

    });

    // this.material_service.postPurchaseOrder(this.PoObject).subscribe(res => {
    //   console.log('Added Purchase Order Data', res);

    // })
  }

  //**************Get all suppliers Functionality****** */
getSuppliers() {

    return new Promise((resolve, reject) => {
      this.materialPurchase = this.material_service.AddMaterialObj
      this.materialObj = this.materialPurchase['totalPurchaseArr']
      console.log('This Material for po', this.materialObj);
      /* if (this.materialObj == undefined) {
        this.router.navigate(['/ngbootstrap/add-pomaterial'])
      } */
      this.material_service.getAllSupliers().subscribe(data => {

        console.log('get all supliers', data);
        this.AllSuppliers = data['data'];
        resolve(this.AllSuppliers);
      });
    }).then(allSupplier => {

      this.AllSuppliers = allSupplier
      this.AllSuppliers.forEach(element => {
        if (element.supplierDetails.supplierName) {
          this.supplierIdPost.push(element._id);
          this.tempSuppliers.push(element.supplierDetails.supplierName);
          this.poFinalObj.push({
            supplierId: element.supplierId,
            supplierName: element.supplierDetails.supplierName,
            city: element.supplierDetails.city,
            contactNo: element.supplierDetails.supplierContactNo[0].contactNo,
            gstNo: element.supplierDetails.gstNo,
            items: [],
            podate: this.currentDate,
            //supplierIndex: ''
          });
        }
      });



      this.poFinalObj.forEach((elementpo , poIndex)=> {
         if (this.materialObj.length) {
           this.materialObj.forEach((mat,index1) => {
             if (elementpo.supplierName == mat.supplier) {
               elementpo.items.push(mat);
               if(this.filteredSuppliers.length == 0){
                 this.filteredSuppliers.push(mat.supplier);
               }
               else if(!(this.filteredSuppliers.includes(mat.supplier))){
                 this.filteredSuppliers.push(mat.supplier);
               }

               console.log("Filtered Suppliers : ", this.filteredSuppliers);
               console.log('inside getAllSupp', this.AllSuppliers);
               //this.materialObj.forEach((allSupplier , index1) => {
               let priceWithNoTax = 0;

               this.ProductForm.get('totalPurchaseArr').value.forEach((element, productIndex) => {
                  if (mat.supplier === element.supplier) {
                     this.ProductForm.controls.totalPurchaseArr['controls'][productIndex].patchValue({
                       totalpurchasepricewithouttax: element.totalpurchaseprice,
                     });
                    
                     priceWithNoTax = priceWithNoTax + (element.totalpurchaseprice);
                     this.priceWithoutTax = priceWithNoTax;
                   }
               });
             // });
               this.totalPaybleForm.controls.totalPaybleData['controls'][index1].patchValue({
              totalpurchaseprise: priceWithNoTax,
              totalpayble: priceWithNoTax,
              }); 
             // elementpo.supplierIndex = this.counter++;
             // console.log("Supplier index : ", elementpo.supplierIndex);
              console.log("PO Final Obj: ", this.poFinalObj);
             }
   
           });
         }
      });
      console.log('all po final obj', this.poFinalObj);
      console.log('Temp Suppliers : ', this.tempSuppliers);

      console.log('this is new object of po', this.newFinalObj);
    });


  }
   
Save_AsDraft() {
    this.getPO();
    console.log('Purchase order : ', this.suppliersPODetails.value);

    let index = 0;
    console.log('Po : ', this.PoObject.purchaseOrders);

    /* PO Object Logic */
    this.poFinalObj.forEach((element, index) => {

      this.suppliersPODetails.value.forEach((ele) => {
        ele.podate  = element.podate;
      })

      /* Product value logic */
      let productValue = [];
      this.ProductForm.controls.totalPurchaseArr.value.forEach(prodelement => {
        if (element.supplierName == prodelement.supplier) {
          productValue.push(prodelement);
          console.log('Product Value : ', productValue);
        }
      });

      /*Total Payable logic*/
      let totalPaybleJson = [];
      let totalindex = 0;
      this.totalPaybleForm.controls.totalPaybleData.value.forEach((totalelement, totalindex) => {
        console.log('Total Payble array : ', totalelement);
        if (index == totalindex) {
          totalPaybleJson.push(totalelement);
        }
      });

      /*extraCharges logic*/
      let totalExtraChargesJson = { extracharges: [] };
      this.extraChargeForm.controls.extraCharges['controls'][index].value.forEach((chargesElement) => {
        console.log(chargesElement);
        totalExtraChargesJson.extracharges.push(chargesElement);
        console.log('Total Extra Charges Json : ', totalExtraChargesJson);
      })

      /*Notification Logic*/
      let notificationJson = { whatsapps: [], emails: [], sms: [] };
      let whatsappJson = [];
      let emailJson = [];
      let smsJson = [];
      this.noificationForm.controls.notifications['controls'][index].value.forEach((whatsappElement) => {
        notificationJson.whatsapps.push(whatsappElement);
      })
      this.noificationForm.controls.emails['controls'][index].value.forEach((emailElement) => {
        notificationJson.emails.push(emailElement);
      })
      this.noificationForm.controls.sms['controls'][index].value.forEach((smsElement) => {
        notificationJson.sms.push(smsElement);
      })

      /*FreeItem Logic*/
      let freeItemJson = { freeitems: [] }
      this.FreeItemsForm.controls.freeItems['controls'][index].value.forEach((freeElement) => {
        freeItemJson.freeitems.push(freeElement);
      })
      console.log('Free Items Json : ', freeItemJson);

      this.objectFinal = {
        //purchaseOrder
        purchaseOrder: this.suppliersPODetails.at(index).value,
        //product          
        product: productValue,
        //extracharges
        extracharges: totalExtraChargesJson,
        //totalPayble
        totalpayble: totalPaybleJson,
        //FreeItem
        freeitem: freeItemJson,
        //notification
        notification: notificationJson,

         isDraft : true,

         isSubmitForReview: false,
      }

      this.PoObject.purchaseOrders.push(this.objectFinal);

      console.log('PO Object : ', this.PoObject);
    });

    this.material_service.postPurchaseOrder(this.PoObject).subscribe(res => {
      console.log('Added Purchase Order Data', res);

    })
  }

  goToRaisePO(){
    this.material_service.patchPreviousData = true;
    this.material_service.previousData = this.ProductForm.value;
    console.log(this.material_service.previousData);
  }
checkValidDate() {
    // if(!this.valideDate()) {
    //   return;
    // }

    var today = new Date().toISOString().split('T')[0];
    document.getElementsByName("date")[0].setAttribute('min', today);
  }

  // valideDate() {
  //   let validated = false;
  //   this.ProductForm.value.totalPurchaseArr.forEach(ele => {
  //       if (ele.estimateddeliverytime >= this.currentDate) {
  //         validated = true;
  //       } else if (ele.estimateddeliverytime < this.currentDate) {
  //         alert('Please Select a date greater than or equal to today');
  //         validated = false;
  //       }
  //      });
  //   return validated;
  // }

  /* add(event: MatChipInputEvent, supplier , i): void {
    const input = event.input;
    const value = event.value;
    
    this.tempSuppliers.forEach(element => {
      if (supplier == element) {
    // Add
    if ((value || '').trim()) {
      this.whatsappNos.push({whatsapps: value.trim()});
      console.log(this.whatsappNos);
      const add = this.noificationForm.get('notifications')['controls'][i] as FormArray;
      add.push(this.fb.group({
        whatsapps: ['']
      }));
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }});
  }
  */

  /* remove(no: whatsapps): void {
    const index = this.whatsappNos.indexOf(no);

    if (index >= 0) {
      this.whatsappNos.splice(index, 1);
    }
  } */
}
