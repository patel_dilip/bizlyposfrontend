import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseOrderGenerateComponent } from './purchase-order-generate.component';

describe('PurchaseOrderGenerateComponent', () => {
  let component: PurchaseOrderGenerateComponent;
  let fixture: ComponentFixture<PurchaseOrderGenerateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseOrderGenerateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseOrderGenerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
