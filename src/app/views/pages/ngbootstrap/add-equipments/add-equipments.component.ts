import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service'
@Component({
  selector: 'kt-add-equipments',
  templateUrl: './add-equipments.component.html',
  styleUrls: ['./add-equipments.component.scss']
})
export class AddEquipmentsComponent implements OnInit {
  addEquipmentForm:FormGroup;
  allAssetInventoryCategory:any =[]
  constructor(private fb:FormBuilder,private material_service: MaterialServiceService,) { }

  ngOnInit() {
    this.addEquipmentForm = this.fb.group({
      equipment_name: [''],
      category: [''],
      //assignattributeSet: [''],
     
     // userid: this.loginUserID

    })
  }
  getAllAssetInventoryCategory() {
    this.material_service.getAllAssetInventoryCategories().subscribe(data => {
      console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
    })
  }

}
