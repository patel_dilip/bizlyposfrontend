import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelOrderMultipleItemComponent } from './cancel-order-multiple-item.component';

describe('CancelOrderMultipleItemComponent', () => {
  let component: CancelOrderMultipleItemComponent;
  let fixture: ComponentFixture<CancelOrderMultipleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelOrderMultipleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelOrderMultipleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
