import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
//import { MaterialServiceService } from 'src/app/core/auth/_services/material-service.service';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service'
import { element, promise } from 'protractor';
import { resolve } from 'dns';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators';


import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { TodoItemNode } from '../../material/layout/tree/tree.component';
import moment from 'moment';
//import {FlatTreeControl} from '@angular/cdk/tree';


//import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

interface Pokemon {
  value: string;
  viewValue: string;
}

interface PokemonGroup {
  disabled?: boolean;
  name: string;
  pokemon: Pokemon[];
}
@Component({
  selector: 'kt-addmaterial',
  templateUrl: './addmaterial.component.html',
  styleUrls: ['./addmaterial.component.scss']
})
export class AddmaterialComponent implements OnInit {
  addMaterialForm: FormGroup;
  addProductForm: FormGroup;
  materialForm: FormGroup;
  liqourMaterialForm: FormGroup;

  beveragesCategoryValue : boolean = false;
  retailCategoryValue : boolean = false;
  foodInventoryValue : boolean = false;
  liquorCategoryValue : boolean = false;

  isAddMaterial: boolean = true;
  isAddProduct: boolean = false;

  quantityArray: any = [];
  OutOfStock:any;


  materialData: any = [];
  itemChk: any = ' ';
  matName:any;

  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];

  InvfirstLevel:any =[];
  InvsecondLevel:any=[];
  InvthirdLevel:any=[];
  InvthirdLevel2:any=[];
  InvforthLevel:any=[];
  InvfiftLevel:any=[];
  RetfirstLevel:any=[];
  RetsecondLevel:any=[]
  RetthirdLevel:any=[];
  RetthirdLevel2:any=[];
  RetforthLevel:any=[];
  RetfiftLevel:any=[];
  LiqfirstLevel:any=[]
  LigsecondLevel:any=[];
  LiqthirdLevel:any=[];
  LiqthirdLevel2:any=[]
  LiqforthLevel:any=[];
  LiqfiftLevel:any=[];
  CancatAllArray:any=[]

  BeverageCategory: any = [];
  allCategories: any = [];
  categoryData: any = [];
  categoryInvData:any=[];


  subCategoryData: any = [];
  InventoryFoodCategory: any = [];
  LiquorCategory: any = [];
  retailFoodCategory: any = [];
  categoryRetData:any =[];
  categoryLiqData:any=[]

  showBevCategories: boolean = false;
  showIntCategories: boolean = false;
  showLiqCategories: boolean = false;
  showRetCategories: boolean = false;

  isReadOnly: boolean = false
  options: any = []
  filteredOptions: Observable<string[]>;
  existOrg: any;

  public model: any;

  children: TodoItemNode[];
  item: string;


  level: number;
  expandable: boolean;

  pokemonControl = new FormControl();
  pokemonGroups: PokemonGroup[]

  constructor(private formbuilder: FormBuilder, private router: Router, private material_service: MaterialServiceService, ) {
    // ***********get all beverages category****************//

    this.getallCategories();
    this.getallInvCategories();
    this.getallRetCategories();
    this.getallLiqCategories();

    this.getallBeverageCategoriesForProduct();

  }

  ngOnInit() {
    this.addMaterialForm = this.formbuilder.group({

      restaurantid: ['123'],
      materialname: [''],
      categoryid: {
        rootcategoryid: '',
        subcategoryid: '',
        subsubcategoryid: ''
      },
      unit: [''],
      availablestock: [''],
      stockthreshhold: [''],
      createdBy: [''],
      createdAt: [''],
      // alerts:['']
    });
    this.addProductForm = this.formbuilder.group({
      restaurantid: ['123'],

      productCategory: [''],
      productName: [''],
      productBrand: [''],
      availableQtys:this.formbuilder.array([])
      

    });

    this.materialForm = this.formbuilder.group({
      subSubCategoryTypeName : [''],
      isApproved: ['']
    });
  
    this.liqourMaterialForm = this.formbuilder.group({
      liquorSubSubVarientTypeName: [''],
      isApproved: ['']
    });

    this.addQuantity();
  }
  get quantityForm(){
    return this.addProductForm.controls.availableQtys as FormArray /// **** Second Step in Form Array
    }
    addQuantity(){
      const qty=this.formbuilder.group({
        quantity:[''],
        unit:['']
      });
      this.quantityForm.push(qty)
    }

  //**********Available threshold ********/
  availableOutOfstock(){
    let available=parseInt(this.addMaterialForm.controls.availablestock.value);
    let threshold=parseInt(this.addMaterialForm.controls.stockthreshhold.value);
    console.log("this is available stock",available);
    console.log("this is threshold",threshold);
    
    if(threshold>available){
this.OutOfStock="Out Of Stock"

    }else{
      this.OutOfStock="Available";
    }
    // this.addMaterialForm.patchValue({
    //   alerts:this.OutOfStock
    // }) 
    console.log("this material is out of stock",this.OutOfStock);
    console.log("this is stock form",JSON.stringify(this.addMaterialForm.value));
    
    
  }


  //********save material**** */
  save_Material() {
    let currentDateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    this.addMaterialForm.patchValue({
      createdBy: "abc",
      createdAt: currentDateTime
    }) 
    let obj = this.addMaterialForm.value
    console.log("Material is saved", obj);

    this.material_service.postMaterial(obj).subscribe(res=>{
      console.log("Post Data Of Add Material",res);
      if(res['success']){
        Swal.fire('Added Material Succesfully','','success')
      }
      else{
        Swal.fire('Failed to Add Material','','warning')
      }
    })

    this.materialForm.patchValue({
      subSubCategoryTypeName: obj.materialname,
      isApproved: false
    })

    let object = this.materialForm.value
    console.log(object.subSubCategoryTypeName);

    if(this.foodInventoryValue == true){
      this.material_service.putMaterialForFoodInventory(obj.categoryid.rootcategoryid, obj.categoryid.subsubcategoryid, object).subscribe(res => {
        console.log(res);
        this.getallInvCategories();
      })
    }

    if(this.beveragesCategoryValue == true){
      this.material_service.putMaterialForBeverages(obj.categoryid.rootcategoryid, obj.categoryid.subsubcategoryid, object).subscribe(res => {
        console.log(res);
        this.getallCategories();
      })
    }

    if(this.liquorCategoryValue == true){
      this.liqourMaterialForm.patchValue({
        liquorSubSubVarientTypeName: obj.materialname,
        isApproved: false
      });

      let object = this.liqourMaterialForm.value;
  
      this.material_service.putMaterialForLiqour(obj.categoryid.rootcategoryid, obj.categoryid.subsubcategoryid, object).subscribe(res => {
        console.log(res);
        this.getallLiqCategories();
      })
    }

    if(this.retailCategoryValue == true){
      this.material_service.putMaterialForRetail(obj.categoryid.rootcategoryid, obj.categoryid.subsubcategoryid, object).subscribe(res => {
        console.log(res);
        this.getallRetCategories();
      })
    }
  }
  //******route to listing*** */
  back_to_page() {
    this.router.navigate(['ngbootstrap/live-inventory']);

  }
  //******hide add material show add product form */
  ShowAddProduct() {
    this.isAddMaterial = false;
    this.isAddProduct = true;
  }
  //******save product***** */
  save_product() {

    let obj = this.addProductForm.value;
    console.log("product is saved", obj);
    this.material_service.postProduct(obj).subscribe(res => {
      console.log("Post Data Of Add Product", res);
      if(res['success']){
        Swal.fire('Added Product Succesfully','','success')
      }
      else{
        Swal.fire('Failed to Add Product','','warning')
      }
    });
    console.log("product for value",this.addProductForm.value);
  }

  //******** get all categories and bevrages functionality*******/
  getallCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe(data => {
        //*****All Categories*****/
        this.allCategories = data
        console.log("This is All Categories", this.allCategories);

     

        //*****All Bevrages*******/
        this.BeverageCategory = data['BeverageCategory']
        console.log('all Bevrages categories', this.BeverageCategory);
       

       



        resolve(this.BeverageCategory)
      })
    }).then(id => {

      console.log("this is id", id);
      this.categoryData = id
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories)

      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel)
    })
  }

  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.secondLevel)
    }).then(sid => {
      this.thirdLevel = sid

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach(element => {

        element.forEach(ele => {
          this.thirdLevel2.push(ele)
          this.forthLevel.push(ele.subSubCategoryType);

        });
      });
      this.forthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.fiftLevel.push(ele)

        });

      })


      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);


    })
  }

  getallBeverageCategoriesForProduct(){
    this.material_service.getAllLiveInevntory(123).subscribe(data => {
      console.log(data);
    })
  }


//******End of Bevrages functionality************** */


//*************started functionality of food inventory*********/
getallInvCategories() {
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

      //****All Inventory Food*****/
      this.InventoryFoodCategory = data['InventoryFoodCategory']
      console.log("this is Inventory Category", this.InventoryFoodCategory);




      resolve(this.InventoryFoodCategory)
    })
  }).then(id => {

    console.log("this is inventory id", id);
    this.categoryInvData = id
    console.log("Food Inventory category data", this.categoryInvData);

    this.categoryInvData.forEach((element) => {
      this.InvfirstLevel.push(element.subCategories)

    });
    console.log("sub category", this.InvfirstLevel);
    this.categoriesInvChild(this.InvfirstLevel)
  })
}
categoriesInvChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.InvsecondLevel.push(ele.subSubCategories)

      });

    });
    resolve(this.InvsecondLevel)
  }).then(sid => {
    this.InvthirdLevel = sid

    console.log("third level", this.InvthirdLevel);

    this.InvthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.InvthirdLevel2.push(ele)
        this.InvforthLevel.push(ele.subSubCategoryType);

      });
    });
    this.InvforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.InvfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of food inventory", this.InvforthLevel);
    console.log("this is fifth level final of food inventory", this.InvfiftLevel);


  })
}
//******End Functionality of food Inventory************** */

//**************started functionality of Retail Food***** */
getallRetCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

    //****All Retail Food*****/
        this.retailFoodCategory = data['retailFoodCategory'];
        console.log("all retail food ", this.retailFoodCategory);




      resolve(this.retailFoodCategory)
    })
  }).then(id => {

    console.log("this is Reatil id", id);
    this.categoryRetData = id
    console.log("Retail category data", this.categoryRetData);

    this.categoryRetData.forEach((element) => {
      this.RetfirstLevel.push(element.subCategories)

    });
    console.log("sub category", this.RetfirstLevel);
    this.categoriesRetChild(this.RetfirstLevel)
  })
}
categoriesRetChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.RetsecondLevel.push(ele.subSubCategories)

      });

    });
    resolve(this.RetsecondLevel)
  }).then(sid => {
    this.RetthirdLevel = sid

    console.log("third level", this.RetthirdLevel);

    this.RetthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.RetthirdLevel2.push(ele)
        this.RetforthLevel.push(ele.subSubCategoryType);

      });
    });
    this.RetforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.RetfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of Retail", this.RetforthLevel);
    console.log("this is fifth level final of Retail", this.RetfiftLevel);

// this.cancatArray()
  });
  
}
//***************End Of Retail Functionality************* */
//************started functionality of liqour*********** */
getallLiqCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

 //*****All Liquor******/
        this.LiquorCategory = data['LiquorCategory']
        console.log("all Liqoure Categories", this.LiquorCategory);




      resolve(this.LiquorCategory)
    })
  }).then(id => {

    console.log("this is Reatil id", id);
    this.categoryLiqData = id
    console.log("Liqour category data", this.categoryLiqData);

    this.categoryLiqData.forEach((element) => {
      this.LiqfirstLevel.push(element.liquorVarients)

    });
    console.log("sub category", this.LiqfirstLevel);
    this.categoriesLiqChild(this.LiqfirstLevel)
  })
}
categoriesLiqChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.LigsecondLevel.push(ele.liquorSubVarients)

      });

    });
    resolve(this.LigsecondLevel)
  }).then(sid => {
    this.LiqthirdLevel = sid

    console.log("third level", this.LiqthirdLevel);

    this.LiqthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.LiqthirdLevel2.push(ele)
        this.LiqforthLevel.push(ele.liquorSubSubVarientType);

      });
    });
    this.LiqforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.LiqfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of Liqour", this.LiqforthLevel);
    console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

this.cancatArray()
  });
  
}
//****************End of liqour functionality****** */

//*****Cancat all array**** */
cancatArray(){
  let Array1=this.InvfiftLevel.concat(this.RetfiftLevel)
  this.CancatAllArray = this.fiftLevel.concat(Array1)
 // this.CancatAllArray=this.fiftLevel.concat(this.InvfiftLevel,this.RetfiftLevel);
  console.log("this is the concatination of array",this.CancatAllArray);
  
}
  //************metronic autocomplete code**** */
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? [] : this.CancatAllArray.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  //*********this is on search function** */
  onSearch(event) {
    //  console.log("this is event",event);

    console.log("this is material name", this.addMaterialForm.controls.materialname.value);
    this.matName = this.addMaterialForm.controls.materialname.value
    
    console.log("this is 2nd third level", this.thirdLevel2);
    this.thirdLevel2.forEach(element => {
      if (element.subSubCategoryType.includes(this.matName)) {
        console.log("this is root", element.subSubCategoryName);
        this.itemChk = element.subSubCategoryName
        this.addMaterialForm.patchValue({
          categoryid: this.itemChk
        })
      }
    });
    this.selectedProChild(this.matName)
  }

  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  selectedChild(event, child, subsubid, subid, rootid) {
    console.log("subsub value : "+ subsubid + " \nsub value : "+ subid + " \nroot value : "+rootid);

    this.addMaterialForm.controls.categoryid.patchValue(
      {
        rootcategoryid : rootid,
        subcategoryid: subid,
        subsubcategoryid: subsubid
      }
    )

    if(this.material_service.allCategoriesData == true){
      let material_details;

      this.material_service.getMaterialData(child.subSubCategoryTypeName).subscribe(res => {
        console.log(res);
        material_details = res['data'];
        console.log(material_details.availablestock)
  
        this.addProductForm.controls.availableQtys['controls'][0].patchValue({
          quantity : material_details.availablestock,
          unit: material_details.unit
        })
      })  
    }

    if(this.material_service.liqourData == true){
      let material_details;
  
      this.material_service.getMaterialData(child.liquorSubSubVarientTypeName).subscribe(res => {
        console.log(res);
        material_details = res['data'];
  
        this.addProductForm.controls.availableQtys['controls'][0].patchValue({
          quantity : material_details.availablestock,
          unit: material_details.unit
        })
      }) 
    }
  }
  selectedProChild(child){
    this.addProductForm.patchValue({
      productCategory:child
    })

    
  }
  //*****bev function***** */
  BevragesCategory() {
    this.showBevCategories = true;
    this.foodInventoryValue = false;
    this.retailCategoryValue = false;
    this.liquorCategoryValue = false;

    this.showIntCategories = false;
    this.showLiqCategories = false;
    this.showRetCategories = false;

    this.beveragesCategoryValue = true;

    document.getElementById("beverages").style.display = "block";
    document.getElementById("foodCategory").style.display = "none";
    document.getElementById("liqour").style.display = "none";
    document.getElementById("retail").style.display = "none";

    this.material_service.allCategoriesData = true;
    this.material_service.liqourData = false;
  }
  //****Inv function**** */
  InventoryCategory() {
    this.showBevCategories = false;
    this.foodInventoryValue = true;
    this.retailCategoryValue = false;
    this.liquorCategoryValue = false;

    this.showIntCategories = true;
    this.showLiqCategories = false;
    this.showRetCategories = false;
    this.showBevCategories = false;

    document.getElementById("beverages").style.display = "none";
    document.getElementById("foodCategory").style.display = "block";
    document.getElementById("liqour").style.display = "none";
    document.getElementById("retail").style.display = "none";
  
    this.material_service.allCategoriesData = true;
    this.material_service.liqourData = false;
  }
  //*****liq function** */
  LiqourCategory() {
    this.showBevCategories = false;
    this.foodInventoryValue = false;
    this.retailCategoryValue = false;
    this.liquorCategoryValue = true;

    this.showLiqCategories = true;
    this.showRetCategories = false;
    this.showBevCategories = false;
    this.showIntCategories = false;
    document.getElementById("beverages").style.display = "none";
    document.getElementById("foodCategory").style.display = "none";
    document.getElementById("liqour").style.display = "block";
    document.getElementById("retail").style.display = "none";
    
    this.material_service.allCategoriesData = false;
    this.material_service.liqourData = true;
  }
  //****ret function****/
  retailCategory() {
    this.showBevCategories = false;
    this.foodInventoryValue = false;
    this.retailCategoryValue = true;
    this.liquorCategoryValue = false;

    this.showRetCategories = true;
    this.showBevCategories = false;
    this.showIntCategories = false;
    this.showLiqCategories = false;
    document.getElementById("beverages").style.display = "none";
    document.getElementById("foodCategory").style.display = "none";
    document.getElementById("liqour").style.display = "none";
    document.getElementById("retail").style.display = "block";
    
    this.material_service.allCategoriesData = true;
    this.material_service.liqourData = false;
  }
  //***category function*** */

}
