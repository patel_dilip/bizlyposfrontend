import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
export interface PeriodicElement {
  date: number;
  user: string;
  lowStockR: string;
  outStockR: string;
  lowstockRetail: string;
  outofStockRetail: string;
  liquorOutofStock: string;
  liquorLowStock: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  {date: 1, user: 'hh', lowStockR: 'ddd', outStockR: 'H', lowstockRetail: 'hh', outofStockRetail: 'dd', liquorOutofStock: 'dd', liquorLowStock: 'dd', action: 'dd'},
  // tslint:disable-next-line:max-line-length
  {date: 1, user: 'hh', lowStockR: 'ddd', outStockR: 'H', lowstockRetail: 'hh', outofStockRetail: 'dd', liquorOutofStock: 'dd', liquorLowStock: 'dd', action: 'dd'},
];


@Component({
  selector: 'kt-closing-stock-audit',
  templateUrl: './closing-stock-audit.component.html',
  styleUrls: ['./closing-stock-audit.component.scss']
})
export class ClosingStockAuditComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['date', 'user', 'lowStockR', 'outStockR', 'lowstockRetail', 'outofStockRetail', 'liquorOutofStock', 'liquorLowStock', 'action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   }

  ngOnInit() {
  }
}