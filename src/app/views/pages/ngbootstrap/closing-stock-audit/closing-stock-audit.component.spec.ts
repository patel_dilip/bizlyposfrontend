import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosingStockAuditComponent } from './closing-stock-audit.component';

describe('ClosingStockAuditComponent', () => {
  let component: ClosingStockAuditComponent;
  let fixture: ComponentFixture<ClosingStockAuditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosingStockAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosingStockAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
