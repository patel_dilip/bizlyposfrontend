import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorPaymentDetailsComponent } from './vendor-payment-details.component';

describe('VendorPaymentDetailsComponent', () => {
  let component: VendorPaymentDetailsComponent;
  let fixture: ComponentFixture<VendorPaymentDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorPaymentDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorPaymentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
