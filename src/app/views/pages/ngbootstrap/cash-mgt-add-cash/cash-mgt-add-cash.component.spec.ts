import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashMgtAddCashComponent } from './cash-mgt-add-cash.component';

describe('CashMgtAddCashComponent', () => {
  let component: CashMgtAddCashComponent;
  let fixture: ComponentFixture<CashMgtAddCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashMgtAddCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashMgtAddCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
