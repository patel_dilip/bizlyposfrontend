import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { Router } from '@angular/router';
@Component({
  selector: 'kt-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.scss']
})
export class AddSupplierComponent implements OnInit {
  addsupplierForm:FormGroup;
  constructor(private fb:FormBuilder, private material_service: MaterialServiceService, private router: Router) { }

  ngOnInit() {
    this.addsupplierForm=this.fb.group({
      supplierName:[''],
      area:[''],
      contactNo:[],
      email:[''],
      panNumber:[''],
      gstinno:['']
    })
  }
//*****save supplier form******/
Save_Suppliers(){
  let obj=this.addsupplierForm.value
  console.log("save suppliers",JSON.stringify(obj));
  this.material_service.addSupplier(obj).subscribe(res => {
    console.log(res)
    if(res['success']){
      this.router.navigate(['/ngbootstrap/purchase-order-raise-po']);
    }
  });
}
}
