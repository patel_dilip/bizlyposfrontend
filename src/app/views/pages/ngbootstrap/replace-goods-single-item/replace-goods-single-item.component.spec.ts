import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplaceGoodsSingleItemComponent } from './replace-goods-single-item.component';

describe('ReplaceGoodsSingleItemComponent', () => {
  let component: ReplaceGoodsSingleItemComponent;
  let fixture: ComponentFixture<ReplaceGoodsSingleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplaceGoodsSingleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplaceGoodsSingleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
