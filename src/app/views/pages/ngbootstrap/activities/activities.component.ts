import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
export interface PeriodicElement {
  date: number;
  pono: number;
  itemname: string;
  brand: string;
  orderQty: number;
  unit: number;
  status: string;
  suppliers: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  {date: 1, pono: 44, itemname: 'ddd', brand: 'H', orderQty: 5, unit: 55, status: 'dd', suppliers: 'dd'},
  // tslint:disable-next-line:max-line-length
  {date: 1, pono: 44, itemname: 'ddd', brand: 'H', orderQty: 5, unit: 55, status: 'dd', suppliers: 'dd'},
];


@Component({
  selector: 'kt-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['date', 'pono', 'itemname', 'brand', 'orderQty', 'unit', 'status', 'suppliers'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   }

  ngOnInit() {
  }
}