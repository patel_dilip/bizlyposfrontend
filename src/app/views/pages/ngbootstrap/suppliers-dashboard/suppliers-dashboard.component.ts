import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { MaterialServiceService } from "../../../../core/auth/_services/material-service.service";
import { MatAccordion } from "@angular/material/expansion";
import { element } from "protractor";

//*****submit table interface*****/
export interface suppliersElement {
  supplierId: string;
  supplierName: string;
  city: string;
  contactDetails: string;
  contactPersonDetails: string;
  gstNo: string;
  category: string;
  rating: string;
  feedback: string;
}

@Component({
  selector: "kt-suppliers-dashboard",
  templateUrl: "./suppliers-dashboard.component.html",
  styleUrls: ["./suppliers-dashboard.component.scss"],
})
export class SuppliersDashboardComponent implements OnInit {
  suppliersColumn = [];
  displayedColumns: string[] = [
    "Supplier Code",
    "Supplier Name",
    "City",
    "Contact Details",
    "Contact Person Details",
    "GST Number",
    "Category",
    "Ratings",
    "Feedback",
  ];
  dataSource: MatTableDataSource<suppliersElement>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @ViewChild(MatAccordion, { static: true }) accordion: MatAccordion;

  constructor(
    private route: Router,
    private material_service: MaterialServiceService
  ) {
    this.material_service.getAllSupliers().subscribe((res) => {
      console.log(res["data"]);
      // this.suppliersColumn.push(res['data']);
      this.dataSource = new MatTableDataSource(res["data"]);
      // this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    console.log(this.dataSource);
  }

  ngOnInit() {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  gotoviewEdit(id) {
    console.log(this.dataSource);
    this.dataSource.data.forEach((element) => {
      if (element.supplierId == id) {
        this.material_service.selectedSupplier.push(element);
      }
    });
    console.log(this.material_service.selectedSupplier);
    this.route.navigateByUrl("/ngbootstrap/view-edit-suppliers");
  }

  navigateToAdd() {
    this.route.navigateByUrl("/ngbootstrap/add-suppliers");
  }
}
