import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
export interface PeriodicElement {
  Time: number;
  Date: number;
  CashInOut: number;
  Amount: number;
  Category: string;
  Note: string;
  DoneBy: string;
  Action: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  { Time: 1, Date: 55, CashInOut: 55, Amount: 66, Category: 'vv', Note: 'jj', DoneBy: 'dd', Action: 'j' },
  // tslint:disable-next-line:max-line-length
  { Time: 1, Date: 55, CashInOut: 55, Amount: 66, Category: 'vv', Note: 'jj', DoneBy: 'dd', Action: 'j' },
];


@Component({
  selector: 'kt-cash-management',
  templateUrl: './cash-management.component.html',
  styleUrls: ['./cash-management.component.scss']
})
export class CashManagementComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['Time', 'Date', 'CashInOut', 'Category', 'Note', 'DoneBy', 'Action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor() { }

  ngOnInit() {
  }

}
