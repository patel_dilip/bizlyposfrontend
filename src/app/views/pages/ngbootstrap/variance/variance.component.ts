import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
export interface PeriodicElement {
  MaterialCode: number;
  MaterialName: string;
  OpeningStock: number;
  ClosingStock: number;
  ActualConsumption: string;
  EstimatedConsumption: string;
  VarianceQty: number;
  VariancePer: number;
  VarianceValue: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  { MaterialCode: 1, MaterialName: 'Hydrogen', OpeningStock: 55, ClosingStock: 66, ActualConsumption: 'dd', EstimatedConsumption: 'jj', VarianceQty: 6, VariancePer: 5, VarianceValue: 'hhh' },
  // tslint:disable-next-line:max-line-length
  { MaterialCode: 1, MaterialName: 'Hydrogen', OpeningStock: 55, ClosingStock: 66, ActualConsumption: 'dd', EstimatedConsumption: 'jj', VarianceQty: 6, VariancePer: 5, VarianceValue: 'hhh' },
];

@Component({
  selector: 'kt-variance',
  templateUrl: './variance.component.html',
  styleUrls: ['./variance.component.scss']
})

export class VarianceComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['MaterialCode', 'MaterialName', 'OpeningStock', 'ClosingStock', 'ActualConsumption', 'EstimatedConsumption', 'VarianceQty', 'VariancePer', 'VarianceValue'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor() { }

  ngOnInit() {
  }

}
