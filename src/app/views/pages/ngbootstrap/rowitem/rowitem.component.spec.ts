import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowitemComponent } from './rowitem.component';

describe('RowitemComponent', () => {
  let component: RowitemComponent;
  let fixture: ComponentFixture<RowitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
