import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router'
import { from, Observable } from 'rxjs';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service'
import Swal from 'sweetalert2';
import { TodoItemNode } from '../../material/layout/tree/tree.component';
@Component({
  selector: 'kt-rowitem',
  templateUrl: './rowitem.component.html',
  styleUrls: ['./rowitem.component.scss']
})
export class RowitemComponent implements OnInit {
  addMaterialForm: FormGroup;
  addProductForm: FormGroup;

  isAddMaterial: boolean = true;
  isAddProduct: boolean = false;

  quantityArray: any = [];

  materialData: any = [];
  itemChk: any = ' ';
  matName:any;

  OutOfStock:any;

  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];

  InvfirstLevel:any =[];
  InvsecondLevel:any=[];
  InvthirdLevel:any=[];
  InvthirdLevel2:any=[];
  InvforthLevel:any=[];
  InvfiftLevel:any=[];
  RetfirstLevel:any=[];
  RetsecondLevel:any=[]
  RetthirdLevel:any=[];
  RetthirdLevel2:any=[];
  RetforthLevel:any=[];
  RetfiftLevel:any=[];
  LiqfirstLevel:any=[]
  LigsecondLevel:any=[];
  LiqthirdLevel:any=[];
  LiqthirdLevel2:any=[]
  LiqforthLevel:any=[];
  LiqfiftLevel:any=[];
  CancatAllArray:any=[]

  BeverageCategory: any = [];
  allCategories: any = [];
  categoryData: any = [];
  categoryInvData:any=[];


  subCategoryData: any = [];
  InventoryFoodCategory: any = [];
  LiquorCategory: any = [];
  retailFoodCategory: any = [];
  categoryRetData:any =[];
  categoryLiqData:any=[]

  showBevCategories: boolean = false;
  showIntCategories: boolean = false;
  showLiqCategories: boolean = false;
  showRetCategories: boolean = false;

  isReadOnly: boolean = false
  options: any = []
  filteredOptions: Observable<string[]>;
  existOrg: any;

  public model: any;

  children: TodoItemNode[];
  item: string;

  level: number;
  expandable: boolean;

  selectedValue = "Unit";
  constructor(private formbuilder:FormBuilder,private router:Router,private material_service: MaterialServiceService,) {
    this.getallCategories();
    this.getallInvCategories();
    this.getallRetCategories();
    this.getallLiqCategories();
   }

  ngOnInit() {
    this.addProductForm = this.formbuilder.group({
      restaurantid: ['123'],

      productCategory: [''],
      productName: [''],
      productBrand: [''],
      availableQtys:this.formbuilder.array([])
      

    });
  
    this.addQuantity();
  }
  //**********FormArray Functionality**** */
  get quantityForm(){
    return this.addProductForm.controls.availableQtys as FormArray /// **** Second Step in Form Array
  }
  addQuantity(){
    const qty=this.formbuilder.group({
      quantity:[''],
      unit:['']
    });
    this.quantityForm.push(qty)
  }
//**********save items form**** */
// save_items(){
//   let obj=this.addProductForm.value
//   console.log("items are saved",obj);
//   this.material_service.postProduct(obj).subscribe(res=>{
//     console.log("Products are saved",res);
//     console.log("Post Data Of Add Material",res);
//     if(res['success']){
//       Swal.fire('Added Product Succesfully','','success')
//     }
//     else{
//       Swal.fire('Failed to Add Product','','warning')
//     }
//   })
// }

//******save product***** */
save_product() {

  let obj = this.addProductForm.value;
  console.log("product is saved", obj);
  this.material_service.postProduct(obj).subscribe(res => {
    console.log("Post Data Of Add Product", res);
    if(res['success']){
      Swal.fire('Added Product Succesfully','','success')
    }
    else{
      Swal.fire('Failed to Add Product','','warning')
    }
  });
  console.log("product for value",this.addProductForm.value);
}


 //******route to listing*** */
 back_to_page(){
  this.router.navigate(['ngbootstrap/stockview']);

}
 
  //******** get all categories*******/
  // getallCategories() {
  //   return new Promise((resolve, reject) => {
  //     this.material_service.getAllCategories().subscribe(data => {
  //       this.allCategories = data
  //       console.log("This is All Categories", this.allCategories);

  //       this.InventoryFoodCategory = data['InventoryFoodCategory']
  //       console.log("this is Inventory Category", this.InventoryFoodCategory);

  //       this.BeverageCategory = data['BeverageCategory']
  //       console.log('all Bevrages categories', this.BeverageCategory);
        
  //       this.LiquorCategory = data['LiquorCategory']
  //       console.log("all Liqoure Categories", this.LiquorCategory);

  //       this.retailFoodCategory = data['retailFoodCategory'];
  //       console.log("all retail food ", this.retailFoodCategory);
        
  //       resolve(this.BeverageCategory)
  //     })
  //   }).then(id => {

  //     console.log("this is id", id);
  //     this.categoryData = id
  //     console.log("category data", this.categoryData);

  //     this.categoryData.forEach((element) => {
  //       this.firstLevel.push(element.subCategories)

  //     });
  //     console.log("sub category", this.firstLevel);
  //     this.categoriesChild(this.firstLevel)
  //   })
  // }

  // categoriesChild(data) {
  //   //console.log("firstlevel",firstLevel);


  //   console.log("second level", data);

  //   return new Promise((resolve, reject) => {
  //     data.forEach(element => {
  //       // console.log("element", element);
  //       element.forEach(ele => {
  //         // console.log("element sub sub",ele.subSubCategories);
  //         this.secondLevel.push(ele.subSubCategories)

  //       });

  //     });
  //     resolve(this.secondLevel)
  //   }).then(sid => {
  //     this.thirdLevel = sid

  //     console.log("third level", this.thirdLevel);

  //     this.thirdLevel.forEach(element => {

  //       element.forEach(ele => {
  //         this.thirdLevel2.push(ele)
  //         this.forthLevel.push(ele.subSubCategoryType);

  //       });
  //     });
  //     this.forthLevel.forEach(element1 => {

  //       element1.forEach(ele => {
  //         this.fiftLevel.push(ele)

  //       });

  //     })


  //     console.log("this is fourth level final", this.forthLevel);
  //     console.log("this is fifth level final", this.fiftLevel);


  //   })
  // }

  //******** get all categories and bevrages functionality*******/
  getallCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe(data => {
        //*****All Categories*****/
        this.allCategories = data
        console.log("This is All Categories", this.allCategories);

        //*****All Bevrages*******/
        this.BeverageCategory = data['BeverageCategory']
        console.log('all Bevrages categories', this.BeverageCategory);

        resolve(this.BeverageCategory)
      })
    }).then(id => {

      console.log("this is id", id);
      this.categoryData = id
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories)

      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel)
    })
  }

  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.secondLevel)
    }).then(sid => {
      this.thirdLevel = sid

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach(element => {

        element.forEach(ele => {
          this.thirdLevel2.push(ele)
          this.forthLevel.push(ele.subSubCategoryType);

        });
      });
      this.forthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.fiftLevel.push(ele)

        });

      })


      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);


    })
  }

  getallBeverageCategoriesForProduct(){
    this.material_service.getAllLiveInevntory(123).subscribe(data => {
      console.log(data);
    })
  }


//******End of Bevrages functionality************** */


//*************started functionality of food inventory*********/
getallInvCategories() {
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

      //****All Inventory Food*****/
      this.InventoryFoodCategory = data['InventoryFoodCategory']
      console.log("this is Inventory Category", this.InventoryFoodCategory);




      resolve(this.InventoryFoodCategory)
    })
  }).then(id => {

    console.log("this is inventory id", id);
    this.categoryInvData = id
    console.log("Food Inventory category data", this.categoryInvData);

    this.categoryInvData.forEach((element) => {
      this.InvfirstLevel.push(element.subCategories)

    });
    console.log("sub category", this.InvfirstLevel);
    this.categoriesInvChild(this.InvfirstLevel)
  })
}
categoriesInvChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.InvsecondLevel.push(ele.subSubCategories)

      });

    });
    resolve(this.InvsecondLevel)
  }).then(sid => {
    this.InvthirdLevel = sid

    console.log("third level", this.InvthirdLevel);

    this.InvthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.InvthirdLevel2.push(ele)
        this.InvforthLevel.push(ele.subSubCategoryType);

      });
    });
    this.InvforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.InvfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of food inventory", this.InvforthLevel);
    console.log("this is fifth level final of food inventory", this.InvfiftLevel);


  })
}
//******End Functionality of food Inventory************** */

//**************started functionality of Retail Food***** */
getallRetCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

    //****All Retail Food*****/
        this.retailFoodCategory = data['retailFoodCategory'];
        console.log("all retail food ", this.retailFoodCategory);




      resolve(this.retailFoodCategory)
    })
  }).then(id => {

    console.log("this is Reatil id", id);
    this.categoryRetData = id
    console.log("Retail category data", this.categoryRetData);

    this.categoryRetData.forEach((element) => {
      this.RetfirstLevel.push(element.subCategories)

    });
    console.log("sub category", this.RetfirstLevel);
    this.categoriesRetChild(this.RetfirstLevel)
  })
}
categoriesRetChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.RetsecondLevel.push(ele.subSubCategories)

      });

    });
    resolve(this.RetsecondLevel)
  }).then(sid => {
    this.RetthirdLevel = sid

    console.log("third level", this.RetthirdLevel);

    this.RetthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.RetthirdLevel2.push(ele)
        this.RetforthLevel.push(ele.subSubCategoryType);

      });
    });
    this.RetforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.RetfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of Retail", this.RetforthLevel);
    console.log("this is fifth level final of Retail", this.RetfiftLevel);

// this.cancatArray()
  });
  
}
//***************End Of Retail Functionality************* */
//************started functionality of liqour*********** */
getallLiqCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

 //*****All Liquor******/
        this.LiquorCategory = data['LiquorCategory']
        console.log("all Liqoure Categories", this.LiquorCategory);




      resolve(this.LiquorCategory)
    })
  }).then(id => {

    console.log("this is Reatil id", id);
    this.categoryLiqData = id
    console.log("Liqour category data", this.categoryLiqData);

    this.categoryLiqData.forEach((element) => {
      this.LiqfirstLevel.push(element.liquorVarients)

    });
    console.log("sub category", this.LiqfirstLevel);
    this.categoriesLiqChild(this.LiqfirstLevel)
  })
}
categoriesLiqChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.LigsecondLevel.push(ele.liquorSubVarients)

      });

    });
    resolve(this.LigsecondLevel)
  }).then(sid => {
    this.LiqthirdLevel = sid

    console.log("third level", this.LiqthirdLevel);

    this.LiqthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.LiqthirdLevel2.push(ele)
        this.LiqforthLevel.push(ele.liquorSubSubVarientType);

      });
    });
    this.LiqforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.LiqfiftLevel.push(ele)

      });

    })


    console.log("this is fourth level final of Liqour", this.LiqforthLevel);
    console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

this.cancatArray()
  });
  
}
//****************End of liqour functionality****** */

//*****Cancat all array**** */
cancatArray(){
  let Array1=this.InvfiftLevel.concat(this.RetfiftLevel)
  this.CancatAllArray = this.fiftLevel.concat(Array1)
 // this.CancatAllArray=this.fiftLevel.concat(this.InvfiftLevel,this.RetfiftLevel);
  console.log("this is the concatination of array",this.CancatAllArray);
  
}

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  selectedChild(event, child) {
    console.log("event value", event);
    console.log("child value", child);

    if(this.material_service.allCategoriesData == true){
      this.addProductForm.patchValue({
        productCategory: child.subSubCategoryTypeName
      })
      
      let material_details;

      this.material_service.getMaterialData(child.subSubCategoryTypeName).subscribe(res => {
      console.log(res);
      material_details = res['data'];
      console.log(material_details.availablestock)

        this.addProductForm.controls.availableQtys['controls'][0].patchValue({
          quantity : material_details.availablestock,
          unit: material_details.unit
        })
      });
    }

    if(this.material_service.liqourData == true){
      this.addProductForm.patchValue({
        productCategory: child.liquorSubSubVarientTypeName
      })
  
      let material_details;
  
      this.material_service.getMaterialData(child.liquorSubSubVarientTypeName).subscribe(res => {
        console.log(res);
        material_details = res['data'];
  
        this.addProductForm.controls.availableQtys['controls'][0].patchValue({
          quantity : material_details.availablestock,
          unit: material_details.unit
        })
      })   
    }
  }
  selectedProChild(child){
    console.log(child)
    this.addProductForm.patchValue({
      productCategory: child
    })
  }
    //*****bev function***** */
    BevragesCategory(){
   
   
      this.showBevCategories=true
   
      this.showIntCategories=false;
    this.showLiqCategories=false;
    this.showRetCategories=false;
  
      this.material_service.allCategoriesData = true;
      this.material_service.liqourData = false;
    }
    //****Inv function**** */
    InventoryCategory(){
      this.showIntCategories=true;
      this.showLiqCategories=false;
      this.showRetCategories=false;
      this.showBevCategories=false;

      this.material_service.allCategoriesData = true;
      this.material_service.liqourData = false;
    }
    //*****liq function** */
    LiqourCategory(){
      this.showLiqCategories=true;
      this.showRetCategories=false;
      this.showBevCategories=false;
      this.showIntCategories=false;

      this.material_service.liqourData = true;
      this.material_service.allCategoriesData = false;
    }
    //****ret function****/
    retailCategory(){
      this.showRetCategories=true;
      this.showBevCategories=false;
      this.showIntCategories=false;
      this.showLiqCategories=false;

      this.material_service.allCategoriesData = true;
      this.material_service.liqourData = false;
    }
      //******save product***** */
  // save_product() {

  //   let obj = this.addProductForm.value;
  //   console.log("product is saved", obj);
  //   this.material_service.postProduct(obj).subscribe(res => {
  //     console.log("Post Data Of Add Product", res);

  //   });
  //   console.log("product for value",this.addProductForm.value);
  // }



}
