import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'kt-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {
  addSupplier : FormGroup;
  dealcategory : FormArray;
  productArr = [];
  selectedProducts;
  supplierDetails : FormGroup;
  contactPersonDetails : FormGroup;

  //Beverage Category
  BeverageCategory: any = [];
  allCategories: any = [];
  categoryData: any = [];
  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];

  //Food Inventory
  InventoryFoodCategory: any = [];
  categoryInvData:any=[];
  InvfirstLevel:any =[];
  InvsecondLevel:any=[];
  InvthirdLevel:any=[];
  InvthirdLevel2:any=[];
  InvforthLevel:any=[];
  InvfiftLevel:any=[];

  //Retail Category
  retailFoodCategory: any = [];
  categoryRetData:any =[];
  RetfirstLevel:any=[];
  RetsecondLevel:any=[]
  RetthirdLevel:any=[];
  RetthirdLevel2:any=[];
  RetforthLevel:any=[];
  RetfiftLevel:any=[];

  //Liqour Category
  LiquorCategory: any = [];
  categoryLiqData:any=[]
  LiqfirstLevel:any=[]
  LigsecondLevel:any=[];
  LiqthirdLevel:any=[];
  LiqthirdLevel2:any=[]
  LiqforthLevel:any=[];
  LiqfiftLevel:any=[];
  CancatAllArray:any=[];

  showBevCategories: boolean = false;
  showIntCategories: boolean = false;
  showLiqCategories: boolean = false;
  showRetCategories: boolean = false;

  constructor(private materialservice : MaterialServiceService, private fb : FormBuilder, private route: Router) { 
    this.addSupplier = this.fb.group({
      supplierDetails : this.fb.group({
        supplierName : '',
        state : '',
        city : '',
        address : this.fb.array([]),
        supplierContactNo : this.fb.array([]),
        supplierWhatsappNo : this.fb.array([]),
        supplierEmail : this.fb.array([]),
        supplierWebsite : '',
        gstNo : '',
        dealCategory : this.fb.array([]),
        dealingProducts : '',
      }),
      contactPersonDetails : this.fb.group({
        contactPersonName : '',
        contactPersonDesignation : '',
        contactPersonNo : this.fb.array([]),
        contactPersonWhatsappNo : this.fb.array([]),
        contactPersonEmail : this.fb.array([])
      })
    })

    this.getallCategories();
    this.getallInvCategories();
    this.getallRetCategories();
    this.getallLiqCategories();

    //Supplier Methods
    this.addAddress();
    this.addSupplierContactNo();
    this.addSupplierWhatsappNo();
    this.addSupplierEmail();

    //Contact Person Methods
    this.addContactPersonNo();
    this.addContactPersonWhatsappNo();
    this.addContactPersonEmail();

    //Patch product Data
    this.patchProduct();

    console.log("Supplier : ", this.addSupplier.value);
  }

  ngOnInit() {
  }

  addAddress(){
    const add = this.addSupplier.controls.supplierDetails.get('address') as FormArray;
    add.push(this.fb.group({
      line1: '',
      line2: ''    
    }))
  }

  addSupplierContactNo(){
    const add = this.addSupplier.controls.supplierDetails.get('supplierContactNo') as FormArray;
    add.push(this.fb.group({
      contactNo : ''    
    }))
  }

  addContactNo(){
    this.addSupplierContactNo();
    // const add = this.addSupplier.controls.supplierDetails.get('supplierContactNo')['controls'] as FormArray;
    // add.push(this.fb.group({
    //   contactNo : ''    
    // }))
  }

  addSupplierWhatsappNo(){
    const add = this.addSupplier.controls.supplierDetails.get('supplierWhatsappNo') as FormArray;
    add.push(this.fb.group({
      whatsappNo : ''    
    }))
  }

  addWhatsappNo(){
    this.addSupplierWhatsappNo();
  }

  addSupplierEmail(){
    const add = this.addSupplier.controls.supplierDetails.get('supplierEmail') as FormArray;
    add.push(this.fb.group({
      email : ''    
    }))
  }

  addEmail(){
    this.addSupplierEmail();
  }

  // createDealCategory(){
  //   const add = this.addSupplier.controls.supplierDetails.get('dealCategory') as FormArray;
  //   add.push(this.fb.group({
  //     rootcategoryid: '',
  //     subcategoryid: '',
  //     subsubcategoryid: '',
  //     subsubcategorytypename: ''    
  //   }))
  // }

  addContactPersonNo(){
    const add = this.addSupplier.controls.contactPersonDetails.get('contactPersonNo') as FormArray;
    add.push(this.fb.group({
      contactNo : ''    
    }))
  }

  addContactPersonNoToArray(){
    this.addContactPersonNo();
  }

  addContactPersonWhatsappNo(){
    const add = this.addSupplier.controls.contactPersonDetails.get('contactPersonWhatsappNo') as FormArray;
    add.push(this.fb.group({
      whatsappNo : ''    
    }))
  }

  addContactPersonWhatsappNoToArray(){
    this.addContactPersonWhatsappNo();
  }

  addContactPersonEmail(){
    const add = this.addSupplier.controls.contactPersonDetails.get('contactPersonEmail') as FormArray;
    add.push(this.fb.group({
      email : ''    
    }))
  }

  addContactPersonEmailToArray(){
    this.addContactPersonEmail();
  }

  patchProduct() {
    this.materialservice.getAllProductIventory(123).subscribe(data => {
      this.productArr = data['data'];
    });
  }

  getallCategories() {
    return new Promise((resolve, reject) => {
      this.materialservice.getAllCategories().subscribe(data => {
        //*****All Categories*****/
        this.allCategories = data
        console.log("This is All Categories", this.allCategories);

        //*****All Bevrages*******/
        this.BeverageCategory = data['BeverageCategory']
        console.log('all Bevrages categories', this.BeverageCategory);

        resolve(this.BeverageCategory)
      })
    }).then(id => {

      console.log("this is id", id);
      this.categoryData = id
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories)

      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel)
    })
  }

  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.secondLevel)
    }).then(sid => {
      this.thirdLevel = sid

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach(element => {

        element.forEach(ele => {
          this.thirdLevel2.push(ele)
          this.forthLevel.push(ele.subSubCategoryType);

        });
      });
      this.forthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.fiftLevel.push(ele)

        });

      })


      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);


    })
  }

  getallInvCategories() {
    return new Promise((resolve, reject) => {
      this.materialservice.getAllCategories().subscribe(data => {
      

        this.InventoryFoodCategory = data['InventoryFoodCategory']
        console.log("this is Inventory Category", this.InventoryFoodCategory);




        resolve(this.InventoryFoodCategory)
      })
    }).then(id => {

      console.log("this is inventory id", id);
      this.categoryInvData = id
      console.log("Food Inventory category data", this.categoryInvData);

      this.categoryInvData.forEach((element) => {
        this.InvfirstLevel.push(element.subCategories)

      });
      console.log("sub category", this.InvfirstLevel);
      this.categoriesInvChild(this.InvfirstLevel)
    })
  }
  categoriesInvChild(data) {
    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        element.forEach(ele => {
          this.InvsecondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.InvsecondLevel)
    }).then(sid => {
      this.InvthirdLevel = sid

      console.log("third level", this.InvthirdLevel);

      this.InvthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.InvthirdLevel2.push(ele)
          this.InvforthLevel.push(ele.subSubCategoryType);

        });
      });
      this.InvforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.InvfiftLevel.push(ele)

        });

      })


      console.log("this is fourth level final of food inventory", this.InvforthLevel);
      console.log("this is fifth level final of food inventory", this.InvfiftLevel);


    })
  }
  getallRetCategories(){
    return new Promise((resolve, reject) => {
      this.materialservice.getAllCategories().subscribe(data => {
          this.retailFoodCategory = data['retailFoodCategory'];
          console.log("all retail food ", this.retailFoodCategory);
        resolve(this.retailFoodCategory)
      })
    }).then(id => {

      console.log("this is Reatil id", id);
      this.categoryRetData = id
      console.log("Retail category data", this.categoryRetData);

      this.categoryRetData.forEach((element) => {
        this.RetfirstLevel.push(element.subCategories)

      });
      console.log("sub category", this.RetfirstLevel);
      this.categoriesRetChild(this.RetfirstLevel)
    })
  }
  categoriesRetChild(data) {
    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        element.forEach(ele => {
          this.RetsecondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.RetsecondLevel)
    }).then(sid => {
      this.RetthirdLevel = sid

      console.log("third level", this.RetthirdLevel);

      this.RetthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.RetthirdLevel2.push(ele)
          this.RetforthLevel.push(ele.subSubCategoryType);

        });
      });
      this.RetforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.RetfiftLevel.push(ele)

        });

      })

      console.log("this is fourth level final of Retail", this.RetforthLevel);
      console.log("this is fifth level final of Retail", this.RetfiftLevel);
    });
  }
  getallLiqCategories(){
    return new Promise((resolve, reject) => {
      this.materialservice.getAllCategories().subscribe(data => {
          this.LiquorCategory = data['LiquorCategory']
          console.log("all Liqoure Categories", this.LiquorCategory);
        resolve(this.LiquorCategory)
      })
    }).then(id => {

      console.log("this is Reatil id", id);
      this.categoryLiqData = id
      console.log("Liqour category data", this.categoryLiqData);

      this.categoryLiqData.forEach((element) => {
        this.LiqfirstLevel.push(element.liquorVarients)

      });
      console.log("sub category", this.LiqfirstLevel);
      this.categoriesLiqChild(this.LiqfirstLevel)
    })
  }
  categoriesLiqChild(data) {


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        element.forEach(ele => {
          this.LigsecondLevel.push(ele.liquorSubVarients)

        });

      });
      resolve(this.LigsecondLevel)
    }).then(sid => {
      this.LiqthirdLevel = sid

      console.log("third level", this.LiqthirdLevel);

      this.LiqthirdLevel.forEach(element => {

        element.forEach(ele => {
          this.LiqthirdLevel2.push(ele)
          this.LiqforthLevel.push(ele.liquorSubSubVarientType);

        });
      });
      this.LiqforthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.LiqfiftLevel.push(ele)

        });

      })


      console.log("this is fourth level final of Liqour", this.LiqforthLevel);
      console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

  this.cancatArray()
    });
    
  }
  cancatArray(){
    let Array1=this.InvfiftLevel.concat(this.RetfiftLevel)
    this.CancatAllArray = this.fiftLevel.concat(Array1)
    console.log("this is the concatination of array",this.CancatAllArray);
    
  }

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  BevragesCategory(){
    this.showBevCategories=true;
    this.showLiqCategories=false;
    this.showRetCategories=false;
    this.showIntCategories=false;
  }
  InventoryCategory(){
    this.showBevCategories=false;
    this.showLiqCategories=false;
    this.showRetCategories=false;
    this.showIntCategories=true;
  }
  LiqourCategory(){
    this.showBevCategories=false;
    this.showLiqCategories=true;
    this.showRetCategories=false;
    this.showIntCategories=false;
  }
  retailCategory(){
    this.showBevCategories=false;
    this.showLiqCategories=false;
    this.showRetCategories=true;
    this.showIntCategories=false;
  }

  selectedChild(event,root,sub,subsub,child){
    console.log("event value", event);
    console.log(root + " " + sub + " " + subsub + " " + child);

    if(event){
      const add = this.addSupplier.controls.supplierDetails.get('dealCategory') as FormArray;
      add.push(this.fb.group({
        rootcategoryid: root,
        subcategoryid: sub,
        subsubcategoryid: subsub,
        subsubcategorytypename: child    
      }))
    }
    else{
      this.addSupplier.controls.supplierDetails.get('dealCategory').value.forEach((element,index) => {
        if(element.subsubcategorytypename === child){
          const deal = this.addSupplier.controls.supplierDetails.get('dealCategory') as FormArray;
          deal.removeAt(index);
          console.log("Removed from deal category");
        }
      })
    }
  }

  navigateToDashboard(){
    this.route.navigateByUrl('/ngbootstrap/suppliers-dashboard');
  }

  Save_Suppliers(){
    console.log("Supplier : ", this.addSupplier.value);
    this.materialservice.addSupplier(this.addSupplier.value).subscribe(res => {
      console.log(res);
    })

    this.route.navigateByUrl('/ngbootstrap/suppliers-dashboard');
  }
}
