import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
export interface PeriodicElement {
  BulkOrderNO: number;
  CustomerName: string;
  ContactNumber: number;
  DateOfRegestraion: number;
  DateOfExecution: number;
  TypeOfOrder: string;
  TotalAmount: number;
  Advance: number;
  AdvanceAmount: number;
  TotalBill: number;
}
const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  {
    BulkOrderNO: 1, CustomerName: 'Hydrogen', ContactNumber: 55, DateOfRegestraion: 66,
    DateOfExecution: 66, TypeOfOrder: 'jj', TotalAmount: 55, Advance: 55, AdvanceAmount: 55, TotalBill: 5005
  },
  // tslint:disable-next-line:max-line-length
  {
    BulkOrderNO: 1, CustomerName: 'Hydrogen', ContactNumber: 55, DateOfRegestraion: 66,
    DateOfExecution: 66, TypeOfOrder: 'jj', TotalAmount: 55, Advance: 55, AdvanceAmount: 55, TotalBill: 5005
  },
];


@Component({
  selector: 'kt-bulk-order',
  templateUrl: './bulk-order.component.html',
  styleUrls: ['./bulk-order.component.scss']
})
export class BulkOrderComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['BulkOrderNO', 'CustomerName', 'ContactNumber', 'DateOfRegestraion', 'DateOfExecution', 'TypeOfOrder', 'TotalAmount', 'Advance', 'AdvanceAmount', 'TotalBill'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor() { }

  ngOnInit() {
  }

}
