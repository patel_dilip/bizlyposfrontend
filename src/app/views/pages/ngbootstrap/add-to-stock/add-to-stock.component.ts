import { Component, OnInit } from '@angular/core';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';

@Component({
  selector: 'kt-add-to-stock',
  templateUrl: './add-to-stock.component.html',
  styleUrls: ['./add-to-stock.component.scss']
})
export class AddToStockComponent implements OnInit {
  addStockData;
  addStockDataArr;
  supplierName;
  displayedColumnsStockData: string[] = [ 'materialName', 'estimateddeliverytime', 'orderQty', 'receivedColumn'];

  constructor(private material_sevice: MaterialServiceService) {
    this.addStockData = this.material_sevice.addStockData;
    console.log('stockdata', this.addStockData);
   this.addStockDataArr = this.material_sevice.addStockDataArr;
   }

  ngOnInit() {
   
  }
  addToStock(id) {
    console.log('id', id);
  }
}
