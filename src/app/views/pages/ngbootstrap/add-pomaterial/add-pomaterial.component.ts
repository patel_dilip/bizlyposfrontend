import { Component, OnInit } from '@angular/core';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'kt-add-pomaterial',
  templateUrl: './add-pomaterial.component.html',
  styleUrls: ['./add-pomaterial.component.scss']
})
export class AddPOMaterialComponent implements OnInit {
  allCategories:any=[];

  selectedMoreInfos = [];
  dupChkMoreInfo = [];

  BeverageCategory:any=[];
  LiquorCategory:any=[];
  retailFoodCategory:any=[];
  InventoryFoodCategory:any=[];
  categoryData: any = [];
  AllSelected:any=[]
  firstLevel: any = [];
  secondLevel: any = [];
  thirdLevel: any = [];
  forthLevel: any = [];
  fiftLevel: any = [];
  thirdLevel2: any = [];
  demo:any=[]
  subCategoryData: any = [];
  
  categoryRetData:any =[];
  categoryLiqData:any=[]
  InvfirstLevel:any =[];
  InvsecondLevel:any=[];
  InvthirdLevel:any=[];
  InvthirdLevel2:any=[];
  InvforthLevel:any=[];
  InvfiftLevel:any=[];
  RetfirstLevel:any=[];
  RetsecondLevel:any=[]
  RetthirdLevel:any=[];
  RetthirdLevel2:any=[];
  RetforthLevel:any=[];
  RetfiftLevel:any=[];
  LiqfirstLevel:any=[]
  LigsecondLevel:any=[];
  LiqthirdLevel:any=[];
  LiqthirdLevel2:any=[]
  LiqforthLevel:any=[];
  LiqfiftLevel:any=[];
  CancatAllArray:any=[];
  categoryInvData:any=[];
  CategoryAllData: any =[];
  constructor(private material_service: MaterialServiceService,private ngxSpinner: NgxSpinnerService,) {
   this.demo=["Special Black Tea", "Leman tea", "Thumbs up", "Latte", "Expresso", "xyz", "Tetra Pack"]
    this.getallCategories() ;
    this.getallInvCategories();
    this.getallRetCategories();
    this.getallLiqCategories();
   }

  ngOnInit() {
  }
  loading(){
    this.ngxSpinner.show()
  }
  //******** get all categories*******/
  getallCategories() {
    // dupChkMoreInfo contains the value of selected Items
    this.dupChkMoreInfo = this.material_service.SelectedItems;
    return new Promise((resolve, reject) => {
    
      this.material_service.getAllCategories().subscribe(data => {
        //*****All Categories*****/
        this.allCategories = data
        //this.ngxSpinner.hide()
        console.log("This is All Categories", this.allCategories);



        //*****All Bevrages*******/
        this.BeverageCategory = data['BeverageCategory']
        console.log('all Bevrages categories', this.BeverageCategory);
       




        resolve(this.BeverageCategory)
      })
    }).then(id => {

      console.log("this is id", id);
      this.categoryData = id
      console.log("category data", this.categoryData);

      this.categoryData.forEach((element) => {
        this.firstLevel.push(element.subCategories)

      });
      console.log("sub category", this.firstLevel);
      this.categoriesChild(this.firstLevel)
    })
  }

  categoriesChild(data) {
    //console.log("firstlevel",firstLevel);


    console.log("second level", data);

    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.secondLevel.push(ele.subSubCategories)

        });

      });
      resolve(this.secondLevel)
    }).then(sid => {
      this.thirdLevel = sid

      console.log("third level", this.thirdLevel);

      this.thirdLevel.forEach(element => {

        element.forEach(ele => {
          this.thirdLevel2.push(ele)
          this.forthLevel.push(ele.subSubCategoryType);

        });
      });
      this.forthLevel.forEach(element1 => {

        element1.forEach(ele => {
          this.fiftLevel.push(ele)
          
        });
       // this.ngxSpinner.hide()
      })


      console.log("this is fourth level final", this.forthLevel);
      console.log("this is fifth level final", this.fiftLevel);


    })
  }
  getallInvCategories() {
    return new Promise((resolve, reject) => {
      this.material_service.getAllCategories().subscribe(data => {
      
       this.CategoryAllData = data['data'];
       console.log("This All category data",this.CategoryAllData);
       
        //****All Inventory Food*****/
        this.InventoryFoodCategory = data['InventoryFoodCategory']
      
        console.log("this is Inventory Category", this.InventoryFoodCategory);
  
  
  
  
        resolve(this.InventoryFoodCategory)
      })
    }).then(id => {
  
      console.log("this is inventory id", id);
      this.categoryInvData = id
      console.log("Food Inventory category data", this.categoryInvData);
  
      this.categoryInvData.forEach((element) => {
        this.InvfirstLevel.push(element.subCategories)
  
      });
      console.log("sub category", this.InvfirstLevel);
      this.categoriesInvChild(this.InvfirstLevel)
    })
  }
  categoriesInvChild(data) {
    //console.log("firstlevel",firstLevel);
  
  
    //console.log("second level", data);
  
    return new Promise((resolve, reject) => {
      data.forEach(element => {
        // console.log("element", element);
        element.forEach(ele => {
          // console.log("element sub sub",ele.subSubCategories);
          this.InvsecondLevel.push(ele.subSubCategories)
  
        });
  
      });
      resolve(this.InvsecondLevel)
    }).then(sid => {
      this.InvthirdLevel = sid
  
     // console.log("third level", this.InvthirdLevel);
  
      this.InvthirdLevel.forEach(element => {
  
        element.forEach(ele => {
          this.InvthirdLevel2.push(ele)
          this.InvforthLevel.push(ele.subSubCategoryType);
  
        });
      });
      this.InvforthLevel.forEach(element1 => {
  
        element1.forEach(ele => {
          this.InvfiftLevel.push(ele)
  
        });
  
      })
  
  
     // console.log("this is fourth level final of food inventory", this.InvforthLevel);
      console.log("this is fifth level final of food inventory", this.InvfiftLevel);
  
  
    })
  }
  //**************started functionality of Retail Food***** */
getallRetCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

    //****All Retail Food*****/
        this.retailFoodCategory = data['retailFoodCategory'];
       
        console.log("all retail food ", this.retailFoodCategory);




      resolve(this.retailFoodCategory)
    })
  }).then(id => {

    console.log("this is Reatil id", id);
    this.categoryRetData = id
    //console.log("Retail category data", this.categoryRetData);

    this.categoryRetData.forEach((element) => {
      this.RetfirstLevel.push(element.subCategories)

    });
    //console.log("sub category", this.RetfirstLevel);
    this.categoriesRetChild(this.RetfirstLevel)
  })
}
categoriesRetChild(data) {
  //console.log("firstlevel",firstLevel);


 // console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.RetsecondLevel.push(ele.subSubCategories)

      });

    });
    resolve(this.RetsecondLevel)
  }).then(sid => {
    this.RetthirdLevel = sid

   // console.log("third level", this.RetthirdLevel);

    this.RetthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.RetthirdLevel2.push(ele)
        this.RetforthLevel.push(ele.subSubCategoryType);

      });
    });
    this.RetforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.RetfiftLevel.push(ele)

      });

    })


    //console.log("this is fourth level final of Retail", this.RetforthLevel);
    console.log("this is fifth level final of Retail", this.RetfiftLevel);

//this.cancatArray()
  });
  
}

//************started functionality of liqour*********** */
getallLiqCategories(){
  return new Promise((resolve, reject) => {
    this.material_service.getAllCategories().subscribe(data => {
    

 //*****All Liquor******/
        this.LiquorCategory = data['LiquorCategory']
       
        console.log("all Liqoure Categories", this.LiquorCategory);




      resolve(this.LiquorCategory)
    })
  }).then(id => {

   // console.log("this is Reatil id", id);
    this.categoryLiqData = id
    //console.log("Liqour category data", this.categoryLiqData);

    this.categoryLiqData.forEach((element) => {
      this.LiqfirstLevel.push(element.liquorVarients)

    });
   // console.log("sub category", this.LiqfirstLevel);
    this.categoriesLiqChild(this.LiqfirstLevel)
  })
}
categoriesLiqChild(data) {
  //console.log("firstlevel",firstLevel);


  console.log("second level", data);

  return new Promise((resolve, reject) => {
    data.forEach(element => {
      // console.log("element", element);
      element.forEach(ele => {
        // console.log("element sub sub",ele.subSubCategories);
        this.LigsecondLevel.push(ele.liquorSubVarients)

      });

    });
    resolve(this.LigsecondLevel)
  }).then(sid => {
    this.LiqthirdLevel = sid

    //console.log("third level", this.LiqthirdLevel);

    this.LiqthirdLevel.forEach(element => {

      element.forEach(ele => {
        this.LiqthirdLevel2.push(ele)
        this.LiqforthLevel.push(ele.liquorSubSubVarientType);

      });
    });
    this.LiqforthLevel.forEach(element1 => {

      element1.forEach(ele => {
        this.LiqfiftLevel.push(ele)

      });

    })


   // console.log("this is fourth level final of Liqour", this.LiqforthLevel);
    console.log("this is fifth level final of Liqour", this.LiqfiftLevel);

//this.cancatArray()
  });
  
}

    // ON MOREINFO chips
    selectBevInfo(groupName,s) {
      if (!this.dupChkMoreInfo.includes(s)) {
        let obj = {
          group: groupName,
          item: s
         
          }
        this.dupChkMoreInfo.push(obj)
        // this.BeverageCategory.push({ title: a, moreinfo: b })
        console.log('selected moreinfos', this.dupChkMoreInfo)
      }
      else {
        alert('Already Added!!')
      }
  
    }
  
   // On Remove MoreInfo 
    removeMoreInfo(s) {
  
        this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.rootCategoryName), 1)
    
    }

  //Save and Send selected data
  SaveAllCategories(){
   this.material_service.SelectedItems=this.dupChkMoreInfo
    
  }
  

}
