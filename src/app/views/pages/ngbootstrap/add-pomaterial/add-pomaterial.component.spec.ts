import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPOMaterialComponent } from './add-pomaterial.component';

describe('AddPOMaterialComponent', () => {
  let component: AddPOMaterialComponent;
  let fixture: ComponentFixture<AddPOMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPOMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPOMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
