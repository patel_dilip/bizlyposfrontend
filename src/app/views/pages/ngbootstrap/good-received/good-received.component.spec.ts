import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodReceivedComponent } from './good-received.component';

describe('GoodReceivedComponent', () => {
  let component: GoodReceivedComponent;
  let fixture: ComponentFixture<GoodReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
