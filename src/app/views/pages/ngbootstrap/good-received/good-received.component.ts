import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { element } from 'protractor';


export interface submittedElement {
  supplierName: number;
  materialName: string;
  orderQty: number;
  podate:number;
  estimateddeliverytime: number;
  Status: string;
  Action: string;

}

@Component({
  selector: 'kt-good-received',
  templateUrl: './good-received.component.html',
  styleUrls: ['./good-received.component.scss']
})



export class GoodReceivedComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['purchaseorderId','supplierName', 'materialName', 'orderQty' , 'podate', 'estimateddeliverytime', 'Status', 'Action'];
  Submit_dataSource: MatTableDataSource<submittedElement>
  //dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<submittedElement>(true, []);
  submittedPOData: any = [];
  submittedReviewData = [];
  
  constructor(private material_service: MaterialServiceService) {
    this.getAllPos();
  }

  ngOnInit() {
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.submittedPOData.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.submittedPOData.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  /* checkboxLabel(row?: submittedElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  } */

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.submittedPOData.filter = filterValue.trim().toLowerCase();
   }
  
   getAllPos() {
    this.material_service.getAllPurchaseOrders(123).subscribe(data => {

      this.submittedPOData = data['data'];
      console.log('submitt',this.submittedPOData);
     
      /* this.submittedPOData.forEach((ele)=> {
         ele.product.forEach((element,index)=> {
          if (element.estimateddeliverytime > ele.purchaseOrder.podate) {
            if (this.submittedReviewData.length === 0) {
              this.submittedReviewData.push(ele);
            } else if (this.submittedReviewData.length > 0 && (!this.submittedReviewData.includes(ele))) {
              this.submittedReviewData.push(ele);
            }
          }
         });
        }); */
        this.material_service.addStockDataArr=this.submittedPOData;
        this.Submit_dataSource = new MatTableDataSource(this.submittedPOData);
       
     /*  let submitReviwTab = this.submittedPOData.filter(
        ele =>ele. === true);
      console.log('sub is',submitReviwTab);
      this.submittedReviewData = submitReviwTab;

     
      this.Submit_dataSource = new MatTableDataSource(submitReviwTab); */
     /*  this.Submit_dataSource.paginator = this.paginator;
      this.Submit_dataSource.sort = this.sort; */
     });
}
addToStock(id,podata) {
  this.submittedPOData.forEach(ele => {
    //this.material_service.filteredPODetails.push(ele.purchaseorderId);
    console.log('id goods received', id);
    if (ele.purchaseorderId === id) {
        this.material_service.addStockData = podata;
    }
    console.log('goods podata',this.material_service.addStockData);
});
}
}