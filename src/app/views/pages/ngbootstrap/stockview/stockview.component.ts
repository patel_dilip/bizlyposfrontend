import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator, MatSort } from '@angular/material';
//******Interface For Raw Material*****/
export interface PeriodicElement {
  productcode: number;
  productCategory: string;
  productName: string;
  productBrand: string;
  unit: number;
  quantity: number;
  probability: number;
  wasteRecorded: number;
  feedback: string;
}
//*****Interface For Retail******/
export interface retailElement {
  itemCode:number;
  productCategory:string;
  SubCategory:string;
  productName:string;
  productBrand:string;
  unit:number;
  quantity:number;
  averagePrice:number;
  probability:string;
  wasteRecorded:string;
  feedback:string

}

const Retail_DATA: retailElement[] = [
  // tslint:disable-next-line:max-line-length
  { itemCode: 1, productCategory: 'Hydrogen', SubCategory: 'Hot drink',productName:'Amul',productBrand:'Amul',unit:2,quantity:5,averagePrice:34,probability:'yes',wasteRecorded:'yes',feedback:'high'},
  // tslint:disable-next-line:max-line-length
  { itemCode: 1, productCategory: 'Hydrogen', SubCategory: 'Hot drink',productName:'Amul',productBrand:'Amul',unit:5,quantity:4,averagePrice:45,probability:'yes',wasteRecorded:'no',feedback:'low'},
];
@Component({
  selector: 'kt-stockview',
  templateUrl: './stockview.component.html',
  styleUrls: ['./stockview.component.scss']
})
export class StockviewComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['productcode', 'productCategory', 'productName', 'productBrand', 'unit', 'quantity', 'probability', 'wasteRecorded', 'feedback'];
  dataSource: MatTableDataSource<PeriodicElement>
  productData:any=[];
  displayedColumnsOfRetail: string[] = ['itemCode', 'productCategory', 'SubCategory', 'productName','productBrand','unit', 'quantity','averagePrice','probability', 'wasteRecorded', 'feedback'];
  dataSourceOfRetail=new MatTableDataSource(Retail_DATA)

  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;
  constructor(private material_service: MaterialServiceService,private ngxSpinner: NgxSpinnerService,) {

    this.material_service.getAllProductIventory(123).subscribe(data=>{
      this.ngxSpinner.show();
      this.productData=data['data']
      console.log("this is all data of Product data",this.productData);
      this.dataSource = new MatTableDataSource(this.productData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.ngxSpinner.hide();
    })

   }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   }

  ngOnInit() {
  }
}
