import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashMgtRemoveCashComponent } from './cash-mgt-remove-cash.component';

describe('CashMgtRemoveCashComponent', () => {
  let component: CashMgtRemoveCashComponent;
  let fixture: ComponentFixture<CashMgtRemoveCashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashMgtRemoveCashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashMgtRemoveCashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
