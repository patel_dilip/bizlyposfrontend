import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitableSupplierComponent } from './suitable-supplier.component';

describe('SuitableSupplierComponent', () => {
  let component: SuitableSupplierComponent;
  let fixture: ComponentFixture<SuitableSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitableSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitableSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
