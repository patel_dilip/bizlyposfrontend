import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
export interface PeriodicElement {
  mcode: number;
  mcategory: string;
  mname: string;
  productsasso: string;
  unit: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  // tslint:disable-next-line:max-line-length
  {mcode: 1, mcategory: 'Hydrogen', mname: 'ddd', productsasso: 'H', unit: 5},
  // tslint:disable-next-line:max-line-length
  {mcode: 1, mcategory: 'Hydrogen', mname: 'ddd', productsasso: 'H', unit: 5},
];

@Component({
  selector: 'kt-suitable-supplier',
  templateUrl: './suitable-supplier.component.html',
  styleUrls: ['./suitable-supplier.component.scss']
})
export class SuitableSupplierComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['mcode', 'mcategory', 'mname', 'productsasso', 'unit'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   }

  ngOnInit() {
  }
}
