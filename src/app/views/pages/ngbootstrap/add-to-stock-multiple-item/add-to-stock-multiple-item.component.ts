import { Component, OnInit } from '@angular/core';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';

@Component({
  selector: 'kt-add-to-stock-multiple-item',
  templateUrl: './add-to-stock-multiple-item.component.html',
  styleUrls: ['./add-to-stock-multiple-item.component.scss']
})
export class AddToStockMultipleItemComponent implements OnInit {
  addStockData;
  constructor(private material_sevice: MaterialServiceService) { }

  ngOnInit() {
    this.addStockData = this.material_sevice.addStockData;
  }

}
