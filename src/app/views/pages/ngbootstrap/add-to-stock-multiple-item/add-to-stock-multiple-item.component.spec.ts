import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToStockMultipleItemComponent } from './add-to-stock-multiple-item.component';

describe('AddToStockMultipleItemComponent', () => {
  let component: AddToStockMultipleItemComponent;
  let fixture: ComponentFixture<AddToStockMultipleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToStockMultipleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToStockMultipleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
