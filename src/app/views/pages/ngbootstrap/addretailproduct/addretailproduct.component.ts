import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'kt-addretailproduct',
  templateUrl: './addretailproduct.component.html',
  styleUrls: ['./addretailproduct.component.scss']
})
export class AddretailproductComponent implements OnInit {
  addRetailProductForm:FormGroup;
  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.addRetailProductForm=this.fb.group({
      productName:[''],
      productCategory:[''],
      productBrand:[''],
      attributeSet:[''],

    })
  }
save_RetailProduct(){
  let obj=this.addRetailProductForm.value
  console.log("Added Retail Product",obj);
  
}
}
