import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddretailproductComponent } from './addretailproduct.component';

describe('AddretailproductComponent', () => {
  let component: AddretailproductComponent;
  let fixture: ComponentFixture<AddretailproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddretailproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddretailproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
