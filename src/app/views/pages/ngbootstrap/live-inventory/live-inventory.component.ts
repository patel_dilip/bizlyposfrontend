import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MaterialServiceService } from '../../../../core/auth/_services/material-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator, MatSort } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { resolve } from 'dns';
export interface PeriodicElement {
  materialcode: number;
  categoryid: string;
  materialname: string;
  productName: string;
  unit: number;
  supplier: string;
  availablestock: number;
  closingstock: number;
  averagecost: number;
  activity: string;
  alerts: string;
  wishlist:string
}




@Component({
  selector: 'kt-live-inventory',
  templateUrl: './live-inventory.component.html',
  styleUrls: ['./live-inventory.component.scss']
})
export class LiveInventoryComponent implements OnInit {
  // tslint:disable-next-line:max-line-length
  displayedColumns: string[] = ['materialcode', 'categoryid', 'materialname', 'productName', 'unit', 'supplier', 'availablestock', 'closingstock', 'averagecost', 'activity', 'alerts','wishlist'];
  dataSource: MatTableDataSource<PeriodicElement>
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;

  inevtoryData:any=[];
  productData:any=[];
  inevtoryMatData:FormGroup
  inevtoryProductData:any=[]
  LiveInventoryForm:FormGroup;
  constructor(private fb:FormBuilder,private material_service: MaterialServiceService,private ngxSpinner: NgxSpinnerService,) {
this.getAllMatPro()
   }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   }

  ngOnInit() {

    this.inevtoryMatData=this.fb.group({
      restaurantid:['123'],
      materialname:[''],
      categoryid:[''],
      unit:[''],
      availablestock:[''],
      stockthreshhold:[''],
      materialcode:[''],
      //availableQtys: ['']

       productName:[''],
       productCategory:[''],
       productBrand:[''],
       productcode:['']

   
    })
   
    console.log("this is data of mat and pro",this.inevtoryMatData.value);
    
  }
    // /Loading fucntion
    loading(){
      this.ngxSpinner.show()
    }
  getAllMatPro(){
    return new Promise((resolve,reject)=>{
      this.material_service.getAllLiveInevntory(123).subscribe(data=>{
       
        this.inevtoryData=data['data']
    
     
        console.log("this is all data of Live Inventory",this.inevtoryData);
        this.dataSource = new MatTableDataSource(this.inevtoryData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
  
      });
      // this.material_service.getAllProductIventory(123).subscribe(data=>{
      //   this.productData=data['data']
      
      //   console.log("this is all data of Product data",this.productData);
      //   this.dataSource = new MatTableDataSource(this.productData);
      //   this.dataSource.paginator = this.paginator;
      //   this.dataSource.sort = this.sort;
      // })

      resolve(this.inevtoryData)
    }).then(id=>{
      console.log("inv id",id);
 







    })
  }
}