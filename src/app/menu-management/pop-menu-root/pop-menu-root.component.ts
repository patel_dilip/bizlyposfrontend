import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'kt-pop-menu-root',
  templateUrl: './pop-menu-root.component.html',
  styleUrls: ['./pop-menu-root.component.scss']
})
export class PopMenuRootComponent implements OnInit {

  // form #101
  treeForm: FormGroup
  value: any

   // autocomplete #102
   options: string[] = ['One', 'Two', 'Three'];
   filteredOptions: Observable<string[]>;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    // form group #101
    this.treeForm = this.fb.group({
      label: ['', Validators.required]
    })

    // autocomplete #102
    this.filteredOptions = this.treeForm.controls.label.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  addLabel(){
    this.value = this.treeForm.controls.label.value
    console.log(this.value)
  }
  


  // autocomplete #102
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }


}
