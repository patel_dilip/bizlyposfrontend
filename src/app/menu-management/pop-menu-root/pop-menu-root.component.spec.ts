import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopMenuRootComponent } from './pop-menu-root.component';

describe('PopMenuRootComponent', () => {
  let component: PopMenuRootComponent;
  let fixture: ComponentFixture<PopMenuRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopMenuRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopMenuRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
