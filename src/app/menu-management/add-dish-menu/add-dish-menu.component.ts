import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kt-add-dish-menu',
  templateUrl: './add-dish-menu.component.html',
  styleUrls: ['./add-dish-menu.component.scss']
})
export class AddDishMenuComponent implements OnInit {

  // select type
  isFood: boolean= false;
  isBeverage: boolean= false;
  isLiquor: boolean = false;

  // for sale
  isForSale: boolean = false;
  isNoSale: boolean = false;


  // available menu card for
  isDineIn: boolean = false;
  isDelievery: boolean = false;
  isSwiggy: boolean = false;
  isZomatoDunzo: boolean = false;

  // dish size available
  isDishSize: boolean = false;

  // GROUP SIZE OPTIONS
  groupSize = [
   
    {label:'200 ml', value: '200 ml'},
    {label:'300 ml', value: '300 ml'},
    {label:'1 litre', value: '1 litre'}
  ]

  availGroupSize = []


  // size option form
  isOption2: boolean = true
  isOption3: boolean = true


  constructor() { }

  ngOnInit() {

    this.groupSize.forEach(element => {
        new Promise((resolve,reject)=>{
          this.availGroupSize.push({label: element.label, value: false})
        })
    });

  }

  // select types
  onSelectType(item){
      if(item == 'food'){
        this.isFood= true
        this.isBeverage= false
        this.isLiquor= false
      }

      if(item == 'beverages'){
        this.isFood= false
        this.isBeverage= true
        this.isLiquor= false
      }

      if(item == 'liquor'){
        this.isFood= false
        this.isBeverage= false
        this.isLiquor= true
      }
  }


  // for sale selection

  onForSale(item){

    if(item == 'sale'){
      this.isForSale = true
      this.isNoSale = false
    }

    if(item == 'nosale'){
      this.isNoSale = true
      this.isForSale = false
    }
  }


  // MENU CARD AVAILABE FOR
  onAvailMenuCard(item){

    if(item == 'dine'){
      this.isDineIn = !this.isDineIn
    }
    if(item == 'delievery'){
      this.isDelievery = !this.isDelievery
    }
    if(item == 'swiggy'){
      this.isSwiggy = !this.isSwiggy
    }
    if(item == 'dunzo'){
      this.isZomatoDunzo = !this.isZomatoDunzo
    }
  }



  // on dish size availabel
  onDishSize(item){
if(item == 'yes'){
this.isDishSize= true
}
if(item == 'no'){
  this.isDishSize = false
}

  }


  // on select size group
  onselectSizeGroup(element){
    element.active = !element.active
  }


  // OPTION SELECTIONS
  sizeGroupFormOption(){
    this.isOption3 = false
    this.isOption2 = false
  }


  // CANCEL OPTION NO. 2
  cancelOption2(){
    this.isDishSize = true
    this.isOption3 = true
    this.isOption2 = true
  }

  // CANCEL OPTION NO. 3
  cancelOption3(){
    this.isDishSize = false
    this.isOption3 = true
    this.isOption2 = true
  }
}

