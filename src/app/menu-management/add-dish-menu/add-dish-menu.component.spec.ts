import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDishMenuComponent } from './add-dish-menu.component';

describe('AddDishMenuComponent', () => {
  let component: AddDishMenuComponent;
  let fixture: ComponentFixture<AddDishMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDishMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDishMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
