import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuHomeComponent } from './menu-home/menu-home.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms'
// MATERIAL IMPORTS
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// PRIME IMPORTS
import {TreeModule} from 'primeng/tree';
import { PopMenuRootComponent } from './pop-menu-root/pop-menu-root.component';
import { PopMenuSubComponent } from './pop-menu-sub/pop-menu-sub.component';
import { AddDishMenuComponent } from './add-dish-menu/add-dish-menu.component';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {FileUploadModule} from 'primeng/fileupload';
import {AccordionModule} from 'primeng/accordion';
import {TabViewModule} from 'primeng/tabview';


const routes: Routes = [
  {
    path: 'menu-management',
    component: MenuHomeComponent
  },
  {
    path: 'menu-management/add-dish',
    component: AddDishMenuComponent
  }
]


@NgModule({
  declarations: [MenuHomeComponent, PopMenuRootComponent, PopMenuSubComponent, AddDishMenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    TreeModule,
    MatDialogModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    DropdownModule,
    MatSlideToggleModule,
    SelectButtonModule,
    FormsModule,
    AutoCompleteModule,
    InputTextareaModule,
    FileUploadModule,
    AccordionModule,
    TabViewModule
  ],
  entryComponents: [PopMenuRootComponent, PopMenuSubComponent]
})
export class MenuMgmtModule { }
