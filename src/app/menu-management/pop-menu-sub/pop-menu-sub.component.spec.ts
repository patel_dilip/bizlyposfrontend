import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopMenuSubComponent } from './pop-menu-sub.component';

describe('PopMenuSubComponent', () => {
  let component: PopMenuSubComponent;
  let fixture: ComponentFixture<PopMenuSubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopMenuSubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopMenuSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
