import { Component, OnInit } from '@angular/core';
import {TreeNode} from 'primeng/api';

@Component({
  selector: 'kt-menu-home',
  templateUrl: './menu-home.component.html',
  styleUrls: ['./menu-home.component.scss']
})
export class MenuHomeComponent implements OnInit {


  categoryTree: TreeNode[] =  [
    {
        "label": "Documents",
        "data": "Documents Folder",
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "children": [{
                "label": "Work",
                "data": "Work Folder",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [{"label": "Expenses.doc", "icon": "pi pi-file", "data": "Expenses Document"}, {"label": "Resume.doc", "icon": "pi pi-file", "data": "Resume Document"}]
            },
            {
                "label": "Home",
                "data": "Home Folder",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [{"label": "Invoices.txt", "icon": "pi pi-file", "data": "Invoices for this month"}]
            }]
    },
    {
        "label": "Pictures",
        "data": "Pictures Folder",
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "children": [
            {"label": "barcelona.jpg", "icon": "pi pi-image", "data": "Barcelona Photo"},
            {"label": "logo.jpg", "icon": "pi pi-file", "data": "PrimeFaces Logo"},
            {"label": "primeui.png", "icon": "pi pi-image", "data": "PrimeUI Logo"}]
    },
    {
        "label": "Movies",
        "data": "Movies Folder",
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "children": [{
                "label": "Al Pacino",
                "data": "Pacino Movies",
                "children": [{"label": "Scarface", "icon": "pi pi-video", "data": "Scarface Movie"}, {"label": "Serpico", "icon": "pi pi-file-video", "data": "Serpico Movie"}]
            },
            {
                "label": "Robert De Niro",
                "data": "De Niro Movies",
                "children": [{"label": "Goodfellas", "icon": "pi pi-video", "data": "Goodfellas Movie"}, {"label": "Untouchables", "icon": "pi pi-video", "data": "Untouchables Movie"}]
            }]
    }
];


  // MENUCARD DROP DOWN

  menuCards = [
    { name: 'Swiggy', value: 'Swiggy' },
    { name: 'Dunzo', value: 'Dunzo' },
    { name: 'Dine In', value: 'Dine In' },
    { name: 'Delievery', value: 'Delievery' }
  ];

  selectedMenuCard: any

   // MENUCARD DROP DOWN

   organizations = [
    { name: 'Taurster pvt ltd', value: 'Taurster pvt ltd' },
    { name: 'Mancester xp org', value: 'Mancester xp org' },
    { name: 'Prestine foods ltd', value: 'Prestine foods ltd' },
    { name: 'Corgent foods chain', value: 'Corgent foods chain' }
  ];

  selectedOrg: any

   // MENUCARD DROP DOWN

   outlets = [
    { name: 'Mc Donalds', value: 'Mc Donalds' },
    { name: 'Burger King', value: 'Burger King' },
    { name: 'KFC', value: 'KFC' },
    { name: 'Dominos', value: 'Dominos' }
  ];

  selectedOutlet: any
  // ADD DISH --- Select Types array

  typesArray: any[] = [
    { name: 'Food', value: 'Food' },
    { name: 'Beverages', value: 'Beverages' },
    { name: 'Liquor', value: 'Liquor' },
  ]
  selectedType = 'Food'

  // ADD DISH --- Select Sales array


  salesArray: any[] = [
    { name: 'For Sale', value: 'For Sale' },
    { name: 'Not For Sale', value: 'Not For Sale' }
  ]
  selectedSale = 'For Sale'



  // MENU ARRAY 

  menuArray = [ 
    { name: 'Paneer', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Palak Paneer', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Mattar Paneer', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Aloo', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Aloo Paratha', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Gobi Paratha', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Muli Paratha', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Egg Curry', type: 'egg', img: 'assets/menu/egg.jpg' },
    { name: 'Egg Bhurji', type: 'egg', img: 'assets/menu/egg.jpg' },
    { name: 'Boiled Egg', type: 'egg', img: 'assets/menu/egg.jpg' },
    { name: 'Fish Fry', type: 'fish', img: 'assets/menu/fish.png' },
    { name: 'Fish Curry', type: 'fish', img: 'assets/menu/fish.png' },
    { name: 'Fish Cutlet', type: 'fish', img: 'assets/menu/fish.png' },
    { name: 'Chicken', type: 'non-veg', img: 'assets/menu/nvegmark.jpg' },
    { name: 'Chicken 65', type: 'non-veg', img: 'assets/menu/nvegmark.jpg' },
    { name: 'Chicken Tandoori', type: 'non-veg', img: 'assets/menu/nvegmark.jpg' },
    { name: 'Fried Chicken Rice', type: 'non-veg', img: 'assets/menu/nvegmark.jpg' },
    { name: 'Fried Paneer Rice', type: 'veg', img: 'assets/menu/vegmark.jpg' },
    { name: 'Fried Egg Rice', type: 'egg', img: 'assets/menu/egg.jpg' }
  ]

  filteredMenu: any[]
  selectedMenu: any


  // Type of Menu

  typeMenuArray = [
    { name: 'Veg', img: 'assets/menu/vegmark.jpg', value: 'Veg' },
    { name: 'Non Veg', img: 'assets/menu/nvegmark.jpg', value: 'Non Veg' },
    { name: 'Egg', img: 'assets/menu/egg.jpg', value: 'Egg' },
    { name: 'Fish', img: 'assets/menu/fish.png', value: 'Fish' },
  ]

  selectedMenuType: any

  // Description

  description: any


  availableFor: any[] = [
    { name: 'Dine In', value: 'Dine In' },
    { name: 'Own Delievery', value: 'Own Delievery' },
    { name: 'Swiggy', value: 'Swiggy' },
    { name: 'Dunzo & Zomato', value: 'Dunzo & Zomato' }
  ]

  selectedAvailableFor: any[]

  // <<<<<<<<<<<<< SIZE GROUPS >>>>>>>>>>>>>>>
  sizePanelToggle: any[] = [
    { name: 'Yes', value: 'Yes' },
    { name: 'No', value: 'No' }
  ]

  selectedSizePanel = 'No'
  toggleSizePanel = false

  // **************************** AUTO COMPLETE SIZE GROUP ************************
  sizeGroupArray = [
    { name: 'Quantity', units: '300 ml/ 400 ml/ 500 ml/ 1 Litre', values: ['300 ml', '400ml', '500 ml', '1 Litre'] },
    { name: 'Sizes', units: 'Small/ Medium/ Large', values: ['Small', 'Medium', 'Large'] },
    { name: 'Portions', units: 'Quarter Plate/ Full Plate', values: ['Quarter Plate', 'Full Plate'] },
    { name: 'Scoops', units: 'Single Scoop/ Double Scoop/ Triple Scoop', values: ['Single Scoop', 'Double Scoop', 'Triple Scoop'] }
  ]


  filteredSize: any[]
  selectedSize: any = {}


  // ************************ Size suggestion sections****************************

  SizeSuggestions = [
    {
      name: 'Previously Used',
      items: [
        { name: 'Quantity', units: '300 ml/ 400 ml/ 500 ml/ 1 Litre', values: ['300 ml', '400ml', '500 ml', '1 Litre'] },
        { name: 'Sizes', units: 'Small/ Medium/ Large', values: ['Small', 'Medium', 'Large'] }
      ]

    },
    {
      name: 'Suggested',
      items: [
        { name: 'Portions', units: 'Quarter Plate/ Full Plate', values: ['Quarter Plate', 'Full Plate'] },
        { name: 'Portions', units: 'Quarter Plate/ Full Plate', values: ['Quarter Plate', 'Full Plate'] },
        { name: 'Portions', units: 'Quarter Plate/ Full Plate', values: ['Quarter Plate', 'Full Plate'] },
        { name: 'Scoops', units: 'Single Scoop/ Double Scoop/ Triple Scoop', values: ['Single Scoop', 'Double Scoop', 'Triple Scoop'] }
      ]
    },
    {
      name: 'Previously Used',
      items: [
        { name: 'Quantity', units: '300 ml/ 400 ml/ 500 ml/ 1 Litre', values: ['300 ml', '400ml', '500 ml', '1 Litre'] },
        { name: 'Sizes', units: 'Small/ Medium/ Large', values: ['Small', 'Medium', 'Large'] },
        { name: 'Sizes', units: 'Small/ Medium/ Large', values: ['Small', 'Medium', 'Large'] }
      ]

    }
  ]

  hideSizeDropdown: boolean = false

  constructor() {

  }
  ngOnInit() {


  }


  // Filtered Menu for Auto Complete
  filterMenu(evt) {
    let filtered: any[] = [];
    let query = evt.query;
    for (let i = 0; i < this.menuArray.length; i++) {
      let menu = this.menuArray[i];
      if (menu.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(menu);
      }
    }

    this.filteredMenu = filtered;
  }


  // on clear value type

  onClearMenu() {
    this.filteredMenu = null
  }


  // size group 
  sizeToggle() {
    console.log(this.selectedSizePanel)

    if (this.selectedSizePanel == 'Yes') {
      this.toggleSizePanel = true
    } else {
      this.toggleSizePanel = false
    }

  }


  // Filter size group

  filterSize(evt) {
    let filtered: any[] = [];
    let query = evt.query;
    for (let i = 0; i < this.sizeGroupArray.length; i++) {
      let size = this.sizeGroupArray[i];
      if (size.name.toLowerCase().indexOf(query.toLowerCase()) == 0 ||
        size.units.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(size);
      }
    }

    this.filteredSize = filtered;
  }

  // clear size group
  onClearSize() {
    this.selectedSize = null
  }


  // SELECTED SIZE GROUP

  selectSizeGroup(value) {
    this.selectedSize = value
    this.hideSizeDropdown = true
  }

  // on Editing size group
  onEditSizeGroup() {
    this.hideSizeDropdown = false

  }

}
