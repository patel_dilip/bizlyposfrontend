import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveInventoryHomeComponent } from './live-inventory-home.component';

describe('LiveInventoryHomeComponent', () => {
  let component: LiveInventoryHomeComponent;
  let fixture: ComponentFixture<LiveInventoryHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveInventoryHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveInventoryHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
