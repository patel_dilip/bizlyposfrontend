import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PopAddMaterialComponent } from '../pop-add-material/pop-add-material.component';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'kt-live-inventory-home',
  templateUrl: './live-inventory-home.component.html',
  styleUrls: ['./live-inventory-home.component.scss'],
  providers: [MessageService]
})
export class LiveInventoryHomeComponent implements OnInit {

  // ngPrime Table
  cols: any = [];
  outletTable: any = [];
  _selectedColumns: any[];
  selected1: any;



  // ALL TABLE #101
  allrows: any = []
  allcols: any = []
  _allselectedColumns: any = []
  allselected1: any
  alliswish: boolean = false;




  // alert options
  alertOptions: any = []
  iswish: boolean = false;
  allWishList: any = [];

  constructor(private matdialog: MatDialog, private messageService: MessageService) { }

  ngOnInit() {

    this.outletTable = [
      {
        "material_code": "101",
        "material_type": "Raw Material",
        "material_name": "Paneer",
        "category": "Dairy",
        "avail_stock":"2.5",
        "unit": "kg",
        "closing_stock": "1.8 kg",
        "avg_stock_unit": "4.5/kg",
        "alert_activity_supplier": { 
          "supplier": "Big Bazaar",
          "area": "kothrud",
          "contact": "7484525291",
          "email": "bigbazar@gmail.com",
          "pan": "BISI2092D",
          "gst": "GST2029EEOO011"
        },
        "wishlist": false
      },
      {
        "material_code": "102",
        "material_type": "Retail Food",
        "material_name": "Balaji Salted Chips",
        "category": "Fried Food",
        "avail_stock":"2.5",
        "unit": "kg",
        "closing_stock": "1.8 kg",
        "avg_stock_unit": "3.5/kg",
        "alert_activity_supplier": { 
          "supplier": "D-mart",
          "area": "Karve Nagar",
          "contact": "7484525291",
          "email": "dmart@gmail.com",
          "pan": "DILI2092D",
          "gst": "GST555DKDO022029"
        },
        "wishlist": false
      },
      {
        "material_code": "103",
        "material_type": "Retail Beverages",
        "material_name": "Coffee Cappacino",
        "category": "Coffee Beans",
        "avail_stock":"5.6",
        "unit": "ltr",
        "closing_stock": "1.2 ltr",
        "avg_stock_unit": "3/ltr",
        "alert_activity_supplier": { 
          "supplier": "Star Bazaar",
          "area": "warje",
          "contact": "9688548557",
          "email": "starbazaar@gmail.com",
          "pan": "STBZI2092D",
          "gst": "GSTBGS29EEOO011"
        },
        "wishlist": false
      },
      {
        "material_code": "104",
        "material_type": "Liquor",
        "material_name": "Whisky",
        "category": "Liqour",
        "avail_stock":"60",
        "unit": "ltr",
        "closing_stock": "41 ltr",
        "avg_stock_unit": "35/ltr",
        "alert_activity_supplier": { 
          "supplier": "Walmart",
          "area": "sinhagad",
          "contact": "84759622155",
          "email": "walmart@gmail.com",
          "pan": "AHY89092D",
          "gst": "GST09876TJG"
        },
        "wishlist": false
      },
    ]
    this.cols = [
      { field: "material_code", header: "Material Code" },
      { field: "material_type", header: "Material Type" },
      { field: "material_name", header: "Material Name" },
      { field: "category", header: "Category" },
      { field: "avail_stock", header: "Available Stock" },
      { field: "unit", header: "Unit" },
      { field: "closing_stock", header: "Closing Stock (31/08/2020)" },
      { field: "avg_stock_unit", header: "Average Cost/Unit" },
      { field: "alert_activity_supplier", header: "Alerts/Activity/Supplier" },
      { field: "wishlist", header: "Wishlist" },
    ]

    this._selectedColumns = this.cols

    // alert options
    this.alertOptions = [
      { label: 'Alerts', value: null },
      { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
      { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
      { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
      { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
      { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } }
    ]


  }

  // ON CHECKBOX CLICK
  onChk() {
    console.log("selected1", this.selected1);
  }

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);
  }


  isheart() {
    this.iswish = !this.iswish
  }


  // Add material pop up dialog

  addMaterial(){

    const dialogRef = this.matdialog.open(PopAddMaterialComponent,{
      width: '950px',
      height: '600px'
    })

   dialogRef.afterClosed().subscribe(res=>{
     console.log('response',res)
   })
  }


// Addwishlist
onAddWishlist(data){
console.log('data',data)



this.outletTable.forEach(element => {
  if(element == data){
element.wishlist = true

this.allWishList.push(element)
this.messageService.add({severity:'info', summary: 'Info', detail: 'Added to wishlist'});
  }
});
}


// Remove WISHLIST

onremoveWishlist(data){
  
   

    this.outletTable.forEach(element => {
      if(element == data){
    element.wishlist = false
    
    this.allWishList.splice(this.allWishList.indexOf(data),1)

    this.messageService.add({severity:'warn', summary: 'Warn', detail: 'Removed from wishlist'});
      }
    });
  
}

// added to wish


}
