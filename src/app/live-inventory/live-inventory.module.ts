import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LiveInventoryHomeComponent } from './live-inventory-home/live-inventory-home.component';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// MATERIAL MODULES
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';
// PRIME NG MODULES
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {TabViewModule} from 'primeng/tabview';
import { MultiSelectModule } from 'primeng/multiselect';
import { PopAddMaterialComponent } from './pop-add-material/pop-add-material.component';
import {SelectButtonModule} from 'primeng/selectbutton';
import {ToolbarModule} from 'primeng/toolbar';
import {OrderListModule} from 'primeng/orderlist';
import {ToastModule} from 'primeng/toast';
import {BreadcrumbModule} from 'primeng/breadcrumb';

// Material modules
import {MatDialogModule} from '@angular/material/dialog';
const routes: Routes = [
  {
    path: 'live-inventory',
    component: LiveInventoryHomeComponent
  }
]

@NgModule({
  declarations: [LiveInventoryHomeComponent, PopAddMaterialComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    TableModule,
    DropdownModule,
    TabViewModule,
    MultiSelectModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    SelectButtonModule,
    ToolbarModule,
    OrderListModule,
    MatChipsModule,
   
    MatMenuModule,
    ToastModule,
    BreadcrumbModule
  ],
  entryComponents: [PopAddMaterialComponent]
})
export class LiveInventoryModule { }
