import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopAddMaterialComponent } from './pop-add-material.component';

describe('PopAddMaterialComponent', () => {
  let component: PopAddMaterialComponent;
  let fixture: ComponentFixture<PopAddMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopAddMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopAddMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
