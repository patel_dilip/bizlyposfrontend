import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'kt-pop-add-material',
  templateUrl: './pop-add-material.component.html',
  styleUrls: ['./pop-add-material.component.scss']
})
export class PopAddMaterialComponent implements OnInit {

  materialForm: FormGroup;

  units: SelectItem[];
  foods: SelectItem[];
  stock_units: SelectItem[];
  materialList: any = []
  rawMaterial: any = []
  selectedcategory: any = {}
  selectedMaterial: any = {}
  isAddMaterial: boolean = true;
  isMatForm: boolean = true;

  // Breadcrums
  directory: MenuItem[];
  constructor(private fb: FormBuilder) {

    // material list
    this.materialList = [
      {
        "material_code": "101",
        "material_type": "Raw Material",
        "material_name": "Paneer",
        "category": "Dairy",
        "avail_stock": "2.5",
        "unit": "kg",
        "closing_stock": "1.8 kg",
        "avg_stock_unit": "4.5/kg",
        "alert_activity_supplier": "",
        "wishlist": false
      },
      {
        "material_code": "102",
        "material_type": "Retail Food",
        "material_name": "Balaji Salted Chips",
        "category": "Fried Food",
        "avail_stock": "2.5",
        "unit": "kg",
        "closing_stock": "1.8 kg",
        "avg_stock_unit": "3.5/kg",
        "alert_activity_supplier": "",
        "wishlist": true
      },
      {
        "material_code": "103",
        "material_type": "Retail Beverages",
        "material_name": "Coffee Cappacino",
        "category": "Coffee Beans",
        "avail_stock": "5.6",
        "unit": "ltr",
        "closing_stock": "1.2 ltr",
        "avg_stock_unit": "3/ltr",
        "alert_activity_supplier": "",
        "wishlist": true
      },
      {
        "material_code": "104",
        "material_type": "Liquor",
        "material_name": "Whisky",
        "category": "Liqour",
        "avail_stock": "60",
        "unit": "ltr",
        "closing_stock": "41 ltr",
        "avg_stock_unit": "35/ltr",
        "alert_activity_supplier": "",
        "wishlist": false
      },
    ]

    // RAW MATERIAL LIST
    this.rawMaterial = [
      {
        root_name: 'Spices',
        category: [
          {
            category_name: 'Dry Spies',
            material: [
              {
                material_name: 'Chilly Powder',
                image: "assets/live-inventory/cummin.jpg",
                other_name: ['Red Chilly Powder', 'Lal Mirchi']
              },
              {
                material_name: 'Cummin Powder',
                image: "assets/live-inventory/red-chilli.jpeg",
                other_name: ['Cummin Spies ', 'Cummin Speed Powder']
              }
            ]
          },
          {
            category_name: 'Wet Spies',
            material: [
              {
                material_name: 'Malabar Masala',
                image: "assets/live-inventory/malabar.jpg",
                other_name: ['Malabar', 'Lal Mirchi']
              },
              {
                material_name: 'Sambhar Masala',
                image: "assets/live-inventory/sambar.jpg",
                other_name: ['Cummin Spies ', 'Cummin Speed Powder']
              }
            ]
          }
        ]
      },
      {
        root_name: 'Fruits',
        category: [
          {
            category_name: 'Apple',
            material: [
              {
                material_name: 'Kashmiri Apple',
                image: "assets/live-inventory/red-apple.jpg",
                other_name: ['Red Apple', 'Apple']
              },
              {
                material_name: 'Green Apple',
                image: "assets/live-inventory/green-apple.jpg",
                other_name: ['Raw Apple ', 'Pear Apple']
              }
            ]
          },
          {
            category_name: 'Banana',
            material: [
              {
                material_name: 'North Banana',
                image: "assets/live-inventory/north-banana.jpg",
                other_name: ['Standard', 'Medium']
              },
              {
                material_name: 'South Banana',
                image: "assets/live-inventory/south-banana.jpg",
                other_name: ['Cherry Banana', 'Sweet Banana']
              }
            ]
          },
          {
            category_name: 'Pineapple',
            material: [
              {
                material_name: 'Pineapple B',
                image: "assets/live-inventory/pineapple.jpg",
                other_name: ['Pineapple spongbob', 'New Pineapple']
              },
              {
                material_name: 'Custard Pineapple',
                image: "assets/live-inventory/cust-pineapple.jpg",
                other_name: ['J Pineapple', 'Green Root Pineapple']
              }
            ]
          },
          {
            category_name: 'Oranges',
            material: [
              {
                material_name: 'Orange Flew',
                image: "assets/live-inventory/orange.jpg",
                other_name: ['BR Orange', 'Reddish Orange']
              },
              {
                material_name: 'Orange Mangroove',
                image: "assets/live-inventory/orange-mangroove.jpg",
                other_name: ['Pulp Orange ', 'California Ora']
              }
            ]
          },
          {
            category_name: 'Mango',
            material: [
              {
                material_name: 'Dasheri Mango',
                image: "assets/live-inventory/mango1.jpg",
                other_name: ['North Mango', 'South Mango']
              },
              {
                material_name: 'Mango Supreme',
                image: "assets/live-inventory/mango2.jpg",
                other_name: ['East Mango', 'West Mango']
              }
            ]
          }
        ]
      },
      {
        root_name: 'Grains',
        category: [
          {
            category_name: 'Wheat',
            material: [
              {
                material_name: 'Wheat Grain',
                image: "assets/live-inventory/wheat1.jpg",
                other_name: ['Wheat 1', 'Wheat 2']
              },
              {
                material_name: 'Grain White',
                image: "assets/live-inventory/wheat2.jpg",
                other_name: ['Grain 1 ', 'Grain 2']
              }
            ]
          },
          {
            category_name: 'Rice',
            material: [
              {
                material_name: 'Basmati Rice',
                image: "assets/live-inventory/basmati.jpg",
                other_name: ['Basmati 1', 'Basmati 2']
              },
              {
                material_name: 'Kollum Rice',
                image: "assets/live-inventory/rice2.jpg",
                other_name: ['Kollum 1', 'Kollum 2']
              }
            ]
          },
          {
            category_name: 'Bajra',
            material: [
              {
                material_name: 'Bajra Grain',
                image: "assets/live-inventory/bajra1.jpg",
                other_name: ['Bajra 1', 'Bajra 2']
              },
              {
                material_name: 'Again Bajra',
                image: "assets/live-inventory/bajra2.jpg",
                other_name: ['Again 1 ', 'Again 2']
              }
            ]
          },
          {
            category_name: 'Moong',
            material: [
              {
                material_name: 'Moong Dal',
                image: "assets/live-inventory/moong1.jpg",
                other_name: ['Moong 1', 'Moong 2']
              },
              {
                material_name: 'Dal Moong',
                image: "assets/live-inventory/moong2.jpg",
                other_name: ['Dal 1 ', 'Dal 2']
              }
            ]
          }
        ]
      },
      {
        root_name: 'Dairy',
        category: [
          {
            category_name: 'Milk',
            material: [
              {
                material_name: 'Soya Milk',
                image: "assets/live-inventory/milk1.jpg",
                other_name: ['Soya 1', 'Soya 2']
              },
              {
                material_name: 'Coconut Milk',
                image: "assets/live-inventory/milk2.jpg",
                other_name: ['coconut 1', 'coconut 2']
              }
            ]
          },
          {
            category_name: 'Curd',
            material: [
              {
                material_name: 'Fresh Curd',
                image: "assets/live-inventory/curd1.jpg",
                other_name: ['Fresh 1', 'Fresh 2']
              },
              {
                material_name: 'Lassi ',
                image: "assets/live-inventory/lassi.jpg",
                other_name: ['Lassi 1 ', 'Lassi 2']
              }
            ]
          },
          {
            category_name: 'Paneer',
            material: [
              {
                material_name: 'Paneer Fresh',
                image: "assets/live-inventory/paneer1.jpg",
                other_name: ['Paneer 1', 'Paneer 2']
              },
              {
                material_name: 'Fresh Paneer',
                image: "assets/live-inventory/paneer2.jpg",
                other_name: ['Fresh 1 ', 'Fresh 2']
              }
            ]
          },
          {
            category_name: 'Cheese',
            material: [
              {
                material_name: 'Cottage Cheese',
                image: "assets/live-inventory/cheese1.jpg",
                other_name: ['Cottage 1', 'Cottage 2']
              },
              {
                material_name: 'Peri Peri Cheese',
                image: "assets/live-inventory/cheese2.jpg",
                other_name: ['Peri 1 ', 'Peri 2']
              }
            ]
          }
        ]
      },
      {
        root_name: 'Poultry',
        category: [
          {
            category_name: 'Eggs',
            material: [
              {
                material_name: 'Brown Egg',
                image: "assets/live-inventory/egg1.jpg",
                other_name: ['omega 1 egg', 'omega 2 egg']
              },
              {
                material_name: 'White Egg',
                image: "assets/live-inventory/egg2.jpg",
                other_name: ['omega 3 egg', 'omega 4 egg']
              }
            ]
          },
          {
            category_name: 'Chicken',
            material: [
              {
                material_name: 'Boiler Egg',
                image: "assets/live-inventory/chicken1.jpg",
                other_name: ['Boiler 1', 'Boiler 2']
              },
              {
                material_name: 'Kadaknath Egg',
                image: "assets/live-inventory/chicken2.jpg",
                other_name: ['Kadaknath 1', 'Kadaknath 2']
              }
            ]
          }
        ]
      },
      {
        root_name: 'Meat',
        category: [
          {
            category_name: 'Mutton',
            material: [
              {
                material_name: 'Lamb',
                image: "assets/live-inventory/lamb.jpg",
                other_name: ['Lamb 1', 'Lamb 2']
              },
              {
                material_name: 'Goat',
                image: "assets/live-inventory/goat.jpg",
                other_name: ['Goat 1', 'Goat 2']
              }
            ]
          },
          {
            category_name: 'Fish',
            material: [
              {
                material_name: 'Tuna',
                image: "assets/live-inventory/tuna.jpg",
                other_name: ['Tuna 1', 'Tuna 2']
              },
              {
                material_name: 'Pamplet',
                image: "assets/live-inventory/pomfret.jpg",
                other_name: ['Pamplet 1 ', 'Pamplet 2']
              }
            ]
          }
        ]
      },
    ]


    this.units = [
      { label: 'KG/Grams', value: 'KG/Grams' },
      { label: 'Ltr./ml', value: 'Ltr./ml' },
      { label: 'Pieces/Dozens', value: 'Pieces/Dozens' }
    ]

    this.foods = [
      { label: 'Veg', value: 'Veg' },
      { label: 'Non-Veg', value: 'Non-Veg' },
      { label: 'Sea Food', value: 'Sea Food' },
      { label: 'Eggs', value: 'Eggs' }
    ]

    this.stock_units = [
      { label: 'KG', value: 'kilogram' },
      { label: 'Grams', value: 'grams' }
    ]

    // directory bread crub

    // example : -{label:'Lionel Messi', url: 'https://en.wikipedia.org/wiki/Lionel_Messi'}

  }

  ngOnInit() {
    this.materialForm = this.fb.group({
      select_unit: [''],
      material_name: [''],
      material_type: [''],
      material_photo: ['assets/live-inventory/imagenf.jpg'],
      available_stock: [''],
      material_code: ['ML-01'],
      low_stock: ['']
    })
  }


  selectUnit(value) {

    console.log(value)
    if (value == 'kilogram') {
      this.stock_units = [
        { label: 'KG', value: 'kilogram' },
        { label: 'Grams', value: 'grams' }
      ]
    }

    if (value == 'litre') {
      this.stock_units = [
        { label: 'Ltr', value: 'litre' },
        { label: 'ml', value: 'mili litre' }
      ]
    }
    if (value == 'pieces') {
      this.stock_units = [
        { label: 'Pieces', value: 'pieces' },
        { label: 'Dozens', value: 'Dozens' }
      ]
    }
  }



  // ON SELECT ROOT FUNCTION
  onSelectRoot(evt) {
    console.log('event', evt)
    console.log('event root name', evt.root_name)

    let label = evt.root_name
    this.directory = [{ label: label }]


    this.selectedcategory = evt
    this.selectedMaterial = []
    this.isAddMaterial = true
    this.isMatForm = true
  }


  // ON SELECT CATEGORY FUNCTION
  onSelectCategory(evt) {
    console.log("event", evt)
    console.log("event category_name", evt.category_name)
    let label = evt.category_name
    console.log('direcctory root', this.directory[0].label)
    let root = this.directory[0].label

    this.directory = [
      { label: root },
      { label: label }
    ]
    this.selectedMaterial = evt
    this.isAddMaterial = false
    this.isMatForm = true
  }


  // on select material
  onselectMaterial(evt) {
    console.log('event material', evt.material_name)

    let root = this.directory[0].label
    let category = this.directory[1].label
    let label = evt.material_name
    this.directory = [
      { label: root },
      { label: category },
      { label: label }
    ]
  }

  // add material form
  addMaterial() {
    // this.isAddMaterial = !this.isAddMaterial
    this.isMatForm = false

    let root = this.directory[0].label
    let category = this.directory[1].label

    this.directory = [
      { label: root },
      { label: category }
    ]

    // Resetting material form   
    this.materialForm.controls.select_unit.reset()
    this.materialForm.controls.material_name.reset()
    this.materialForm.controls.material_type.reset()  
    this.materialForm.controls.available_stock.reset()  
    this.materialForm.controls.low_stock.reset()
  }

  // save material form
  saveMaterial() {


    console.table(this.materialForm.value)

    // ADD MATERIAL TO LIST
    let newMaterial = {
      material_name: this.materialForm.controls.material_name.value,
      image: this.materialForm.controls.material_photo.value,
      other_name: []
    }

    this.rawMaterial.forEach(element => {
      if (element.root_name == this.directory[0].label) {
        element.category.forEach(ele => {
          if (ele.category_name == this.directory[1].label) {
            ele.material.push(
              newMaterial
            )
          }
        });
      }
    });
    this.isMatForm = true

    
  }


}
